#version 150

uniform sampler2D source[];
uniform vec4      sourceSize[];

in Vertex {
  vec2 texCoord;
};

out vec4 fragColor;

void main() {
  vec2 texelSize = 1.0 / sourceSize[0].xy;

  vec2 range;
  range.x = dFdx(texCoord.x) / 2.0 * 0.99;
  range.y = dFdy(texCoord.y) / 2.0 * 0.99;

  float left   = texCoord.x - range.x;
  float top    = texCoord.y + range.y;
  float right  = texCoord.x + range.x;
  float bottom = texCoord.y - range.y;

  vec4 topLeftColor     = texture2D(source[0], (floor(vec2(left, top)     / texelSize) + 0.5) * texelSize);
  vec4 bottomRightColor = texture2D(source[0], (floor(vec2(right, bottom) / texelSize) + 0.5) * texelSize);
  vec4 bottomLeftColor  = texture2D(source[0], (floor(vec2(left, bottom)  / texelSize) + 0.5) * texelSize);
  vec4 topRightColor    = texture2D(source[0], (floor(vec2(right, top)    / texelSize) + 0.5) * texelSize);

  vec2 border = clamp(round(texCoord / texelSize) * texelSize, vec2(left, bottom), vec2(right, top));

  float totalArea = 4.0 * range.x * range.y;

  vec4 averageColor;
  averageColor  = ((border.x - left)  * (top - border.y)    / totalArea) * topLeftColor;
  averageColor += ((right - border.x) * (border.y - bottom) / totalArea) * bottomRightColor;
  averageColor += ((border.x - left)  * (border.y - bottom) / totalArea) * bottomLeftColor;
  averageColor += ((right - border.x) * (top - border.y)    / totalArea) * topRightColor;

  fragColor = averageColor;
}
