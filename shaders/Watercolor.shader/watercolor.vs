#version 150

uniform vec4 targetSize;
uniform vec4 outputSize;

in vec4 position;
in vec2 texCoord;

out Vertex {
  vec2 texCoord[9];
} vertexOut;

void main() {
  float x = (1.0 / targetSize.x);
  float y = (1.0 / targetSize.y);
  vec2 dg1 = vec2( x, y);
  vec2 dg2 = vec2(-x, y);
  vec2 dx = vec2(x, 0.0);
  vec2 dy = vec2(0.0, y);

  gl_Position = gl_ModelViewProjectionMatrix * position;
  vertexOut.texCoord[0] = texCoord;
  vertexOut.texCoord[1] = texCoord - dg1;
  vertexOut.texCoord[2] = texCoord - dy;
  vertexOut.texCoord[3] = texCoord - dg2;
  vertexOut.texCoord[4] = texCoord + dx;
  vertexOut.texCoord[5] = texCoord + dg1;
  vertexOut.texCoord[6] = texCoord + dy;
  vertexOut.texCoord[7] = texCoord + dg2;
  vertexOut.texCoord[8] = texCoord - dx;
}
