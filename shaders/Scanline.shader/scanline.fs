#version 150

uniform bool phase;
uniform sampler2D source[];
uniform vec4 sourceSize[];

in Vertex {
  vec2 texCoord;
};

out vec4 fragColor;

void main() {
  vec3 rgb = texture(source[0], texCoord).xyz;
  vec3 intensity;
  if(sourceSize[0].y <= 240) {
    bool field = fract(texCoord.y * sourceSize[0].y) >= 0.5;
    if(!field) {
      intensity = vec3(0);
    } else {
      intensity = smoothstep(0.2, 0.8, rgb) + normalize(rgb);
    }
  } else {
    bool field = fract(gl_FragCoord.y * 0.25) >= 0.5;
    if((!field && phase) || (field && !phase)) {
      intensity = vec3(0);
    } else {
      intensity = (smoothstep(0.2, 0.8, rgb) + normalize(rgb)) / 2.0;
    }
  }
  fragColor = vec4(intensity * -0.5 + rgb * 1.1, 1.0);
}
