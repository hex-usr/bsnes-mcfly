#version 150

uniform sampler2D source[];

in Vertex {
  vec2 texCoord;
};

out vec4 fragColor;

void main() {
  vec4 rgb = texture2D(source[0], texCoord);
  vec4 intens = smoothstep(0.2,0.8,rgb) + normalize(vec4(rgb.xyz, 1.0));

  if(fract(gl_FragCoord.y * 0.5) > 0.5) intens = rgb * 0.8;
  fragColor = intens;
}
