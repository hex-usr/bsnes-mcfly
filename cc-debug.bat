@echo off

cd bsnes\

del /q out\*.exe
del /q out\*.dll

mingw32-make -j6 profile=accuracy build=debug console=true

if exist out\bsnes-mcfly.exe (
  mingw32-make -j6 profile=performance name=bsnes-performance.exe build=debug console=true

  if not exist out\bsnes-performance.exe (
    set /A ERRORLEVEL=1
  )
) else (
  set /A ERRORLEVEL=1
)

if /I "%ERRORLEVEL%" NEQ "0" (pause)
@echo on