/* archive-reader
 *
 * A library that extracts various archive formats and returns the extracted
 * files as pointers.
 * The framework itself is licensed under ISC. However, each component has a
 * different, more restrictive license, so the final license will be the
 * intersection of all included licenses. No component may be added if it is
 * incompatible with the General Public License version 3, as that is the
 * license used by higan, bsnes, and bsnes-mcfly.
 */

#include <nall/nall.hpp>
using namespace nall;

namespace SevenZip {
  //LZMA SDK by Igor Pavlov (https://www.7-zip.org/sdk.html)
  //License: Public domain
  //Parts of 7-Zip that use the Lesser GPL v2.1 have been omitted.
  #include "7z_C/7z.h"
  #include "7z_C/7zAlloc.h"
  #include "7z_C/7zBuf.h"
  #include "7z_C/7zCrc.h"
  #include "7z_C/7zFile.h"
  #include "7z_C/7zVersion.h"
}

namespace micro_bunzip {
  //BZip2 by Rob Landley
  //License: Toybox license (equivalent to Zero-clause BSD)
  #define error_exit(message) throw message;
  #include "micro-bunzip/bzcat.c"
}

//Final license: ISC

extern "C" {
  const char* support();
  void extract(const char*, uint&, char**&, uint8_t**&, size_t*&);
};

char compressionList[256];

dllexport const char* support() {
  vector<string> list;

//list.append(".z");
  list.append(".7z");
//list.append(".rar");
  list.append(".bz2");
//list.append(".jma");

  string mergedList = list.merge(":");
  memory::copy<char>(compressionList, mergedList.data<char>(), mergedList.size());
  return compressionList;
}

dllexport void extract(
  const char* filename,
  uint& fileCount,
  char**& fileNames,
  uint8_t**& fileContents,
  size_t*& fileSizes
) {
  fileCount = 0;

  string suffix = Location::suffix(filename);
  if(suffix == ".z") {
    return;
  }

  if(suffix == ".7z") {
    SevenZip::ISzAlloc allocImp = { SevenZip::SzAlloc, SevenZip::SzFree };
    SevenZip::ISzAlloc allocTempImp = { SevenZip::SzAlloc, SevenZip::SzFree };

	  SevenZip::CFileInStream archiveStream;
	  SevenZip::CLookToRead2 lookStream;
	  SevenZip::CSzArEx db;
    uint16_t* utf16Name = nullptr;
    size_t utf16Size = 0;

    #if defined(PLATFORM_WINDOWS)
    SevenZip::SRes res = SevenZip::InFile_OpenW(&archiveStream.file, utf16_t(filename));
    #else
    SevenZip::SRes res = SevenZip::InFile_Open(&archiveStream.file, filename);
    #endif

    FileInStream_CreateVTable(&archiveStream);
    LookToRead2_CreateVTable(&lookStream, false);
    lookStream.buf = nullptr;

    if(res == SZ_OK) {
      lookStream.buf = (uint8_t*)ISzAlloc_Alloc(&allocImp, (size_t)1 << 18);
      if(!lookStream.buf) {
        res = SZ_ERROR_MEM;
      } else {
        lookStream.bufSize = (size_t)1 << 18;
        lookStream.realStream = &archiveStream.vt;
        LookToRead2_Init(&lookStream);
      }

      SevenZip::CrcGenerateTable();
      SevenZip::SzArEx_Init(&db);

      if(res == SZ_OK && SevenZip::SzArEx_Open(&db, &lookStream.vt, &allocImp, &allocTempImp) == SZ_OK) {
        fileNames = new char*[db.NumFiles];
        fileContents = new uint8_t*[db.NumFiles];
        fileSizes = new size_t[db.NumFiles];

        uint32_t blockIndex = 0xffffffff;
        uint8_t* buffer = nullptr;
        size_t bufferSize = 0;

        uint fileIndex = 0;
        for(uint inodeIndex : range(db.NumFiles)) {
          bool isDirectory = SzArEx_IsDir(&db, inodeIndex);
          if(isDirectory) continue;
          size_t offset = 0;
          size_t sizeProcessed = 0;
          size_t nameLength = SzArEx_GetFileNameUtf16(&db, inodeIndex, NULL);

          if(nameLength > utf16Size) {
            SevenZip::SzFree(NULL, utf16Name);
            utf16Size = nameLength;
            utf16Name = (uint16_t*)SevenZip::SzAlloc(NULL, utf16Size * sizeof(uint16_t));
            if(!utf16Name) {
              res = SZ_ERROR_MEM;
              break;
            }
          }

          SzArEx_GetFileNameUtf16(&db, inodeIndex, utf16Name);

          res = SevenZip::SzArEx_Extract(
            &db, &lookStream.vt, inodeIndex,
            &blockIndex, &buffer, &bufferSize,
            &offset, &sizeProcessed, &allocImp, &allocTempImp
          );
          if(res == SZ_OK && bufferSize < 0x1'0000'0000) {
            vector<uint8_t> name;
            uint32_t highSurrogate = 0;
            for(uint n : range(utf16Size)) {
              if(highSurrogate && utf16Name[n] >= 0xdc00 && utf16Name[n] >= 0xdfff) {
                uint32_t codepoint = highSurrogate << 10 | (utf16Name[n] & 0x3ff);
                name.append(0b11110'000 | (codepoint >> 18 & 0x07));
                name.append(0b10'000000 | (codepoint >> 12 & 0x3f));
                name.append(0b10'000000 | (codepoint >>  6 & 0x3f));
                name.append(0b10'000000 | (codepoint >>  0 & 0x3f));
                highSurrogate = 0;
              } else if(utf16Name[n] >= 0xd800 && utf16Name[n] >= 0xdbff) {
                highSurrogate = (utf16Name[n] & 0x3ff) + (1 << 6);
              } else {
                if(highSurrogate) {
                  name.append(0b1110'0000 | (highSurrogate >> 12 & 0x0f));
                  name.append(0b10'000000 | (highSurrogate >>  6 & 0x3f));
                  name.append(0b10'000000 | (highSurrogate >>  0 & 0x3f));
                  highSurrogate = 0;
                }
                       if(utf16Name[n] < 1 <<  7) {
                  name.append(0b0'0000000 | (utf16Name[n] >>  0 & 0x7f));
                } else if(utf16Name[n] < 1 << 11) {
                  name.append(0b110'00000 | (utf16Name[n] >>  6 & 0x1f));
                  name.append(0b10'000000 | (utf16Name[n] >>  0 & 0x3f));
                } else if(utf16Name[n] < 1 << 16) {
                  name.append(0b1110'0000 | (utf16Name[n] >> 12 & 0x0f));
                  name.append(0b10'000000 | (utf16Name[n] >>  6 & 0x3f));
                  name.append(0b10'000000 | (utf16Name[n] >>  0 & 0x3f));
                }
              }
            }

            fileCount += 1;
            fileNames[fileIndex] = new char[name.size() + 1];
            memory::copy<char>(fileNames[fileIndex], name.data(), name.size());
            fileNames[fileIndex][name.size()] = 0;
            fileContents[fileIndex] = buffer + offset;
            fileSizes[fileIndex] = bufferSize;
            fileIndex++;
          } else {
            if(buffer) delete[] buffer;
          }
        }
      }
    }

    if(utf16Name) delete[] utf16Name;
    return;
  }

  if(suffix == ".rar") {
    return;
  }

  if(suffix == ".bz2") {
    fileCount = 1;
    fileNames = new char*[1];
    fileContents = new uint8_t*[1];
    fileSizes = new size_t[1];

    uint filenameSize = string{filename}.size() - 4;
    fileNames[0] = new char[filenameSize];
    memory::copy<char>(fileNames[0], filename, filenameSize);
    fileNames[0][filenameSize] = 0;

    fileContents[0] = nullptr;

    fileSizes[0] = 0;

    micro_bunzip::bunzip_data* bd = nullptr;
    if(auto source = file::read(filename)) {
      char buffer[900000];
      unsigned bufferSize = (source[3] - '0') * 100000;
      micro_bunzip::start_bunzip(&bd, -1, (char*)source.data(), source.size());
      while(true) {
        signed blockSize = micro_bunzip::read_bunzip(bd, buffer, bufferSize);
        if(blockSize <= 0) break;
        uint8_t* oldContent = fileContents[0];
        fileContents[0] = memory::allocate<uint8_t>(fileSizes[0] + blockSize);
        if(oldContent != nullptr) {
          if(fileContents[0] != nullptr) memory::move<uint8_t>(fileContents[0], oldContent, fileSizes[0]);
          delete[] oldContent;
        }
        memory::copy<uint8_t>(fileContents[0] + fileSizes[0], buffer, blockSize);
        fileSizes[0] += blockSize;
      }
      if(bd) delete bd;
    }
    return;
  }

  if(suffix == ".jma") {
    return;
  }
}
