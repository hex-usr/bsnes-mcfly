@echo off

cd bsnes\

del /q out\*.exe
del /q out\*.dll

mingw32-make -j6

if not exist out\bsnes-mcfly.exe (
  set /A ERRORLEVEL=1
)

if /I "%ERRORLEVEL%" NEQ "0" (pause)
@echo on