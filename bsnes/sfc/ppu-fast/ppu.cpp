#include <sfc/sfc.hpp>

namespace higan::SuperFamicom {

PPU& ppubase = ppu;
#define PPU PPUfast
#define ppu ppufast

PPU ppu;
#include "io.cpp"
#include <sfc/ppu-fast/line.cpp>
#include "background.cpp"
#include <sfc/ppu-fast/mode7.cpp>
#include "object.cpp"
#include <sfc/ppu-fast/window.cpp>
#include "serialization.cpp"

auto PPU::interlace() const -> bool { return ppubase.display.interlace; }
auto PPU::overscan() const -> bool { return ppubase.display.overscan; }
auto PPU::vdisp() const -> uint { return ppubase.display.vdisp; }

PPU::PPU() {
  output = new uint32[512 * 512] + 16 * 512;  //overscan offset
  #if defined(DOUBLE_VRAM)
  tilecache[TileMode::BPP2] = new uint8[8192 * 8 * 8];
  tilecache[TileMode::BPP4] = new uint8[4096 * 8 * 8];
  tilecache[TileMode::BPP8] = new uint8[2048 * 8 * 8];
  #else
  tilecache[TileMode::BPP2] = new uint8[4096 * 8 * 8];
  tilecache[TileMode::BPP4] = new uint8[2048 * 8 * 8];
  tilecache[TileMode::BPP8] = new uint8[1024 * 8 * 8];
  #endif

  for(uint y : range(lines.size())) {
    lines[y].y = y;
  }

  for(uint layer : range(4)) for(uint priority : range(2)) BGEnabled[layer][priority] = true;
  for(uint priority : range(4)) OBJEnabled[priority] = true;

  FrameSkip = 0;
  FrameCounter = 0;
}

PPU::~PPU() {
  delete[] (output - 16 * 512);  //overscan offset
  delete[] tilecache[TileMode::BPP2];
  delete[] tilecache[TileMode::BPP4];
  delete[] tilecache[TileMode::BPP8];
}

auto PPU::Enter() -> void {
  while(true) scheduler.synchronize(), ppu.main();
}

auto PPU::step(uint clocks) -> void {
  tick(clocks);
  Thread::step(clocks);
  synchronize(cpu);
}

auto PPU::main() -> void {
  if(FrameCounter > 0) {
    if(vcounter() == 0) {
      scheduler.exit(Scheduler::Event::Frame);
      FrameCounter = FrameSkip == 0 ? 0 : (FrameCounter + 1) % FrameSkip;
    }
    step(lineclocks() - hcounter());
    return;
  }
  scanline();
  uint y = vcounter();
  step(512);
  if(y >= 1 && y <= 239) {
    if(io.displayDisable || y >= vdisp()) {
      lines[y].io.displayDisable = true;
    } else {
      memcpy(&lines[y].io, &io, sizeof(io));
      memcpy(&lines[y].cgram, &cgram, sizeof(cgram));
    }
    if(!Line::count) Line::start = y;
    Line::count++;
  }
  if(FrameSkip > 1 && vcounter() == 0) FrameCounter += 1;
  step(lineclocks() - hcounter());
}

auto PPU::scanline() -> void {
  if(vcounter() == 0) {
    ppubase.display.interlace = io.interlace;
    ppubase.display.overscan = io.overscan;
    latch.hires = false;
    io.obj.timeOver = false;
    io.obj.rangeOver = false;
  }

  if(vcounter() > 0 && vcounter() < vdisp()) {
    latch.hires |= io.pseudoHires || io.bgMode == 5 || io.bgMode == 6;
    #undef ppu  //sigh
    latch.hires |= io.bgMode == 7 && option.hack.ppu.hiresMode7();
    #define ppu ppufast
  }

  if(vcounter() == vdisp() && !io.displayDisable) {
    oamAddressReset();
  }

  if(vcounter() == 240) {
    Line::flush();
    scheduler.exit(Scheduler::Event::Frame);
  }
}

auto PPU::refresh() -> void {
  auto output = this->output;
  if(!overscan()) output -= 14 * 512;
  auto pitch  = 512 << !interlace();
  auto width  = 256 << hires();
  auto height = 240 << interlace();
  video.setEffect(Video::Effect::ColorBleed, option.video.colorBleed() && hires());
  video.refresh(output, pitch * sizeof(uint32), width, height);
}

auto PPU::load() -> bool {
  ppu1.version = property.ppu1.version();
  ppu2.version = property.ppu2.version();
  #if defined(DOUBLE_VRAM)
  vram.mask = property.ppu1.vram.size() / sizeof(uint16) - 1;
  if(vram.mask != 0xffff) vram.mask = 0x7fff;
  #endif
  return true;
}

auto PPU::power(bool reset) -> void {
  create(Enter, system.cpuFrequency());
  PPUcounter::reset();
  memory::fill<uint32>(output, 512 * 480);

  function<auto (uint24, uint8) -> uint8> reader{&PPU::readIO, this};
  function<auto (uint24, uint8) -> void> writer{&PPU::writeIO, this};
  bus.map(reader, writer, "00-3f,80-bf:2100-213f");

  if(!reset) {
    for(auto address : range(vram.mask + 1)) {
      vram[address] = 0x0000;
      updateTiledata(address);
    }
    for(auto& color : cgram) color = 0x0000;
    for(auto& object : objects) object = {};
  }

  latch = {};
  io = {};
  updateVideoMode();

  #undef ppu  //sigh
  ItemLimit = !option.hack.ppu.noSpriteLimit() ? 32 : 128;
  TileLimit = !option.hack.ppu.noSpriteLimit() ? 34 : 128;
  #define ppu ppufast

  Line::start = 0;
  Line::count = 0;
}

auto PPU::updateFrameSkip() -> void {
  #undef ppu  //sigh
  FrameSkip = option.hack.ppu.frameSkip();
  #define ppu ppufast
  FrameCounter = 0;
}

auto PPU::updateEnabledLayers() -> void {
  #undef ppu  //sigh
  BGEnabled[0][0] = !option.hack.ppu.bgDisable[0]();
  BGEnabled[0][1] = !option.hack.ppu.bgDisable[1]();
  BGEnabled[1][0] = !option.hack.ppu.bgDisable[2]();
  BGEnabled[1][1] = !option.hack.ppu.bgDisable[3]();
  BGEnabled[2][0] = !option.hack.ppu.bgDisable[4]();
  BGEnabled[2][1] = !option.hack.ppu.bgDisable[5]();
  BGEnabled[3][0] = !option.hack.ppu.bgDisable[6]();
  BGEnabled[3][1] = !option.hack.ppu.bgDisable[7]();
  OBJEnabled[0] = !option.hack.ppu.objDisable[0]();
  OBJEnabled[1] = !option.hack.ppu.objDisable[1]();
  OBJEnabled[2] = !option.hack.ppu.objDisable[2]();
  OBJEnabled[3] = !option.hack.ppu.objDisable[3]();
  #define ppu ppufast
}

}
