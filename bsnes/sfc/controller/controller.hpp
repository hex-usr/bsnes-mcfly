struct Controller : Thread {
  Controller(uint port);
  virtual ~Controller();
  static auto Enter() -> void;

  virtual auto main() -> void;
  auto iobit() -> bool;
  auto iobit(bool data) -> void;
  virtual auto data() -> uint2 { return 0; }
  virtual auto latch(bool data) -> void {}

  const uint port;
};

struct ControllerPort {
  auto connect(uint deviceID) -> void;

  auto power(uint port) -> void;
  auto unload() -> void;
  auto serialize(serializer&) -> void;

  uint port;
  Controller* device = nullptr;
};

extern ControllerPort controllerPort1;
extern ControllerPort controllerPort2;

#include <sfc/controller/gamepad/gamepad.hpp>
#include <sfc/controller/mouse/mouse.hpp>
#include <sfc/controller/super-multitap/super-multitap.hpp>
#include "super-scope/super-scope.hpp"
#include "justifier/justifier.hpp"
