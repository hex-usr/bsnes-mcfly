#ifndef NALL_QT_WINDOW_HPP
#define NALL_QT_WINDOW_HPP

namespace nall {

class Window : public QWidget {
public:
  void setGeometryString(string* geometryString);
  void setCloseOnEscape(bool);
  void show();
  void hide();
  void shrink();

  Window();

protected:
  string* geometryString;
  bool closeOnEscape;
  void keyReleaseEvent(QKeyEvent* event);
  void closeEvent(QCloseEvent* event);
};

inline void Window::setGeometryString(string* geometryString_) {
  geometryString = geometryString_;
  if(geometryString && isVisible() == false) {
    auto data = Decode::Base64(*geometryString);
    QByteArray array((const char*)data.data(), data.size());
    restoreGeometry(array);
  }
}

inline void Window::setCloseOnEscape(bool value) {
  closeOnEscape = value;
}

inline void Window::show() {
  if(geometryString && isVisible() == false) {
    auto data = Decode::Base64(*geometryString);
    QByteArray array((const char*)data.data(), data.size());
    restoreGeometry(array);
  }
  QWidget::show();
  QApplication::processEvents();
  activateWindow();
  raise();
}

inline void Window::hide() {
  if(geometryString && isVisible() == true) {
    QByteArray geometry = saveGeometry();
    *geometryString = Encode::Base64((const uint8_t*)geometry.data(), geometry.length());
  }
  QWidget::hide();
}

inline void Window::shrink() {
  if(isFullScreen()) return;

  for(uint n = 0; n < 2; n++) {
    resize(0, 0);
    usleep(2000);
    QApplication::processEvents();
  }
}

inline void Window::keyReleaseEvent(QKeyEvent* event) {
  if(closeOnEscape && (event->key() == Qt::Key_Escape)) close();
  QWidget::keyReleaseEvent(event);
}

inline void Window::closeEvent(QCloseEvent* event) {
  if(geometryString) {
    QByteArray geometry = saveGeometry();
    *geometryString = Encode::Base64((const uint8_t*)geometry.data(), geometry.length());
  }
  QWidget::closeEvent(event);
}

inline Window::Window() {
  geometryString = nullptr;
  closeOnEscape = true;
}

}

#endif
