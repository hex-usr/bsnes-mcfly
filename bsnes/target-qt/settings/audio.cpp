AudioSettings::AudioSettings() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  boxes = new QHBoxLayout;
  layout->addLayout(boxes);

  deviceLabel = new QLabel("Device:");
  deviceLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  boxes->addWidget(deviceLabel);

  deviceList = new QComboBox;
  for(auto& device : audio.hasDevices()) {
    deviceList->addItem(QString::fromUtf8(device));
  }
  boxes->addWidget(deviceList);

  frequencyLabel = new QLabel("Frequency:");
  frequencyLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  boxes->addWidget(frequencyLabel);

  frequencyList = new QComboBox;
  boxes->addWidget(frequencyList);

  latencyLabel = new QLabel("Latency:");
  latencyLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
  boxes->addWidget(latencyLabel);

  latencyList = new QComboBox;
  boxes->addWidget(latencyList);

  exclusive = new QCheckBox("Exclusive mode");
  layout->addWidget(exclusive);

  sliders = new QGridLayout;
  layout->addLayout(sliders);

  volumeLabel = new QLabel("Volume:");
  volumeLabel->setToolTip("Warning: any volume other than 100% will result in a slight audio quality loss");
  sliders->addWidget(volumeLabel, 0, 0);

  volumeValue = new QLabel;
  volumeValue->setAlignment(Qt::AlignHCenter);
  volumeValue->setMinimumWidth(volumeValue->fontMetrics().width("262144hz"));
  sliders->addWidget(volumeValue, 0, 1);

  volume = new QSlider(Qt::Horizontal);
  volume->setMinimum(0);
  volume->setMaximum(200);
  sliders->addWidget(volume, 0, 2);

  /*
  frequencySkewLabel = new QLabel("Input frequency:");
  frequencySkewLabel->setToolTip(
    "Adjusts audio resampling rate.\n"
    "When both video sync and audio sync are enabled, use this setting to fine-tune the output.\n"
    "Lower the input frequency to clean audio output, eliminating crackling / popping.\n"
    "Raise the input frequency to smooth video output, eliminating duplicated frames."
  );
  sliders->addWidget(frequencySkewLabel, 1, 0);

  frequencySkewValue = new QLabel;
  frequencySkewValue->setAlignment(Qt::AlignHCenter);
  sliders->addWidget(frequencySkewValue, 1, 1);

  frequencySkew = new QSlider(Qt::Horizontal);
  frequencySkew->setMinimum(31500);
  frequencySkew->setMaximum(32500);
  sliders->addWidget(frequencySkew, 1, 2);
  */

  connect(deviceList, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &AudioSettings::deviceChange);
  connect(exclusive, QOverload<int>::of(&QCheckBox::stateChanged), this, &AudioSettings::exclusiveToggle);
  connect(volume, QOverload<int>::of(&QSlider::valueChanged), this, &AudioSettings::volumeAdjust);
//connect(frequencySkew, QOverload<int>::of(&QSlider::valueChanged), this, &AudioSettings::frequencySkewAdjust);

  syncUi();
}

void AudioSettings::synchronizeDriverSettings() {
  if(audio.hasExclusive()) {
    exclusive->show();
  } else {
    exclusive->hide();
  }
}

void AudioSettings::syncUi() {
  if(auto index = audio.hasDevices().find(config->audio.device)) {
    deviceList->setCurrentIndex(index());
  } else {
    deviceList->setCurrentIndex(0);
    deviceChange(0);
  }

  frequencyList->disconnect();
  frequencyList->clear();
  for(auto& frequency : audio.hasFrequencies()) {
    frequencyList->addItem(QString::fromUtf8(string{frequency, "hz"}));
  }
  if(auto index = audio.hasFrequencies().find(config->audio.frequency)) {
    frequencyList->setCurrentIndex(index());
  } else {
    frequencyList->setCurrentIndex(0);
    frequencyChange(0);
  }
  connect(frequencyList, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &AudioSettings::frequencyChange);

  latencyList->disconnect();
  latencyList->clear();
  for(auto& latency : audio.hasLatencies()) {
    latencyList->addItem(QString::fromUtf8(string{latency, "ms"}));
  }
  if(auto index = audio.hasLatencies().find(config->audio.latency)) {
    latencyList->setCurrentIndex(index());
  } else {
    latencyList->setCurrentIndex(0);
    latencyChange(0);
  }
  connect(latencyList, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &AudioSettings::latencyChange);

  exclusive->setChecked(config->audio.exclusive);

  volumeValue->setText(QString::fromUtf8(string{config->audio.volume, "%"}));
  volume->setSliderPosition(config->audio.volume);

  /*
  frequencySkewValue->setText(QString::fromUtf8(string{config->audio.inputFrequency, "hz"}));
  frequencySkew->setSliderPosition(config->audio.inputFrequency);
  */
}

void AudioSettings::deviceChange(int value) {
  config->audio.device = string{deviceList->currentText()};
  program->updateAudioDriver();
  syncUi();
}

void AudioSettings::frequencyChange(int value) {
  config->audio.frequency = toReal(string{frequencyList->currentText()}.trimRight("hz"));
  program->updateAudioDriver();
}

void AudioSettings::latencyChange(int value) {
  config->audio.latency = toNatural(string{latencyList->currentText()}.trimRight("ms"));
  program->updateAudioDriver();
}

void AudioSettings::exclusiveToggle() {
  config->audio.exclusive = exclusive->isChecked();
  program->updateAudioDriver();
}

void AudioSettings::volumeAdjust(int value) {
  config->audio.volume = value;
  program->updateAudioEffects();

  volumeValue->setText(QString::fromUtf8(string{config->audio.volume, "%"}));
  volume->setSliderPosition(config->audio.volume);
}

/*
void AudioSettings::frequencySkewAdjust(int value) {
  config->audio.inputFrequency = value;
  program->updateAudioEffects();

  frequencySkewValue->setText(QString::fromUtf8(string{config->audio.inputFrequency, "hz"}));
  frequencySkew->setSliderPosition(config->audio.inputFrequency);
}
*/
