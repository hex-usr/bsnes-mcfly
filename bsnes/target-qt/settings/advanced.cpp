AdvancedSettings::AdvancedSettings() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(0);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  driverLayout = new QGridLayout;
  driverLayout->setHorizontalSpacing(Style::WidgetSpacing);
  layout->addLayout(driverLayout);
  layout->addSpacing(Style::WidgetSpacing);

  videoLabel = new QLabel("Video driver:");
  driverLayout->addWidget(videoLabel, 0, 0);

  audioLabel = new QLabel("Audio driver:");
  driverLayout->addWidget(audioLabel, 0, 1);

  inputLabel = new QLabel("Input driver:");
  driverLayout->addWidget(inputLabel, 0, 2);

  videoDriver = new QComboBox;
  driverLayout->addWidget(videoDriver, 1, 0);

  audioDriver = new QComboBox;
  driverLayout->addWidget(audioDriver, 1, 1);

  inputDriver = new QComboBox;
  driverLayout->addWidget(inputDriver, 1, 2);

  driverInfo = new QLabel("<small>Note: driver changes require restart to take effect.</small>");
  driverInfo->setStyleSheet("margin-left: -3px; margin-top: 5px;");
  driverLayout->addWidget(driverInfo, 2, 0, 1, 3);

  regionTitle = new QLabel("Hardware region:");
  layout->addWidget(regionTitle);

  regionLayout = new QHBoxLayout;
  regionLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(regionLayout);
  layout->addSpacing(Style::WidgetSpacing);

  regionGroup = new QButtonGroup(this);

  regionAuto = new QRadioButton("Auto-detect");
  regionAuto->setToolTip("Automatically select hardware region on cartridge load");
  regionGroup->addButton(regionAuto);
  regionLayout->addWidget(regionAuto);

  regionNTSC = new QRadioButton("NTSC");
  regionNTSC->setToolTip("Force NTSC region (Japan, Korea, US)");
  regionGroup->addButton(regionNTSC);
  regionLayout->addWidget(regionNTSC);

  regionPAL = new QRadioButton("PAL");
  regionPAL->setToolTip("Force PAL region (Europe, ...)");
  regionGroup->addButton(regionPAL);
  regionLayout->addWidget(regionPAL);

  portTitle = new QLabel("Expansion port device:");
  layout->addWidget(portTitle);

  portLayout = new QHBoxLayout;
  portLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(portLayout);
  layout->addSpacing(Style::WidgetSpacing);

  portGroup = new QButtonGroup(this);

  portSatellaview = new QRadioButton("Satellaview");
  portGroup->addButton(portSatellaview);
  portLayout->addWidget(portSatellaview);

  portNone = new QRadioButton("None");
  portGroup->addButton(portNone);
  portLayout->addWidget(portNone);

  portSpacer = new QWidget;
  portLayout->addWidget(portSpacer);

  focusTitle = new QLabel("When main window does not have focus:");
  layout->addWidget(focusTitle);

  focusLayout = new QHBoxLayout;
  focusLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(focusLayout);
  layout->addSpacing(Style::WidgetSpacing);

  focusButtonGroup = new QButtonGroup(this);

  focusPause = new QRadioButton("Pause emulation");
  focusPause->setToolTip("Ideal for prolonged multi-tasking");
  focusButtonGroup->addButton(focusPause);
  focusLayout->addWidget(focusPause);

  focusIgnore = new QRadioButton("Ignore input");
  focusIgnore->setToolTip("Ideal for light multi-tasking when using keyboard");
  focusButtonGroup->addButton(focusIgnore);
  focusLayout->addWidget(focusIgnore);

  focusAllow = new QRadioButton("Allow input");
  focusAllow->setToolTip("Ideal for light multi-tasking when using joypad(s)");
  focusButtonGroup->addButton(focusAllow);
  focusLayout->addWidget(focusAllow);

  miscTitle = new QLabel("Miscellaneous:");
  layout->addWidget(miscTitle);

  rewindEnable = new QCheckBox("Enable Rewind Support");
  layout->addWidget(rewindEnable);

  useCommonDialogs = new QCheckBox("Use Native OS File Dialogs");
  layout->addWidget(useCommonDialogs);

  hacksTitle = new QLabel("Hacks:");
  layout->addWidget(hacksTitle);

  hacksDoubleVRAM = new QCheckBox("Double VRAM");
  #if defined(DOUBLE_VRAM)
  hacksDoubleVRAM->setToolTip("Set VRAM to 128 KB instead of 64 KB. Some games such as Yoshi's Island can't handle this.");
  #else
  #if defined(PROFILE_PERFORMANCE)
  hacksDoubleVRAM->setToolTip("This option will not work with the Performance profile.");
  #else
  hacksDoubleVRAM->setToolTip("This option will not work unless you recompile bsnes-mcfly with double VRAM support.");
  #endif
  #endif
  #if !defined(DOUBLE_VRAM)
  hacksDoubleVRAM->setEnabled(false);
  #endif
  layout->addWidget(hacksDoubleVRAM);

  hacksNoSpriteLimit = new QCheckBox("No Sprite Limit");
  hacksNoSpriteLimit->setToolTip("Increase the maximum sprites per scanline from 32 to 128.");
  layout->addWidget(hacksNoSpriteLimit);

  hacksHiresMode7 = new QCheckBox("Hires Mode 7");
  hacksHiresMode7->setToolTip("Increase the resolution to 512×224 for crisper Mode 7 rendering.");
  layout->addWidget(hacksHiresMode7);

  updateVideoDriver();
  updateAudioDriver();
  updateInputDriver();
  updateConfiguration();

  connect(regionAuto, &QRadioButton::pressed, [this]() { config->sfc.region = "Auto"; });
  connect(regionNTSC, &QRadioButton::pressed, [this]() { config->sfc.region = "NTSC"; });
  connect(regionPAL , &QRadioButton::pressed, [this]() { config->sfc.region = "PAL";  });

  connect(portSatellaview, &QRadioButton::pressed, [this]() { emulator->connect(2, SFC::ID::Device::Satellaview); });
  connect(portSatellaview, &QRadioButton::pressed, [this]() { emulator->connect(2, SFC::ID::Device::None);        });

  connect(focusPause, &QRadioButton::pressed, [this]() {
    config->input.focusPolicy = ConfigurationSettings::Input::FocusPolicyPauseEmulation;
  });
  connect(focusIgnore, &QRadioButton::pressed, [this]() {
    config->input.focusPolicy = ConfigurationSettings::Input::FocusPolicyIgnoreInput;
  });
  connect(focusAllow, &QRadioButton::pressed, [this]() {
    config->input.focusPolicy = ConfigurationSettings::Input::FocusPolicyAllowInput;
  });

  connect(rewindEnable, &QCheckBox::stateChanged, [this]() {
    config->system.rewindEnabled = rewindEnable->isChecked();
    program->resetRewindHistory();
  });
  connect(useCommonDialogs, &QCheckBox::stateChanged, [this]() {
    config->diskBrowser.useCommonDialogs = useCommonDialogs->isChecked();
  });
  connect(hacksDoubleVRAM, &QCheckBox::stateChanged, [this]() {
    emulator->setProperty("system/ppu1/vram/size", hacksDoubleVRAM->isChecked() ? 131072 : 65536);
  });
  connect(hacksNoSpriteLimit, &QCheckBox::stateChanged, [this]() {
    emulator->setOption("hack/ppu/noSpriteLimit", (bool)hacksNoSpriteLimit->isChecked());
  });
  connect(hacksHiresMode7, &QCheckBox::stateChanged, [this]() {
    emulator->setOption("hack/ppu/hiresMode7", (bool)hacksHiresMode7->isChecked());
  });
}

void AdvancedSettings::updateVideoDriver() {
  videoDriver->disconnect();
  videoDriver->clear();

  for(string& driver : video.hasDrivers()) {
    videoDriver->addItem(QString::fromUtf8(driver));
    if(driver == config->video.driver) videoDriver->setCurrentText(QString::fromUtf8(driver));
  }

  connect(videoDriver, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
    if(index >= 0) config->video.driver = videoDriver->itemText(index).toUtf8().data();
  });
}

void AdvancedSettings::updateAudioDriver() {
  audioDriver->disconnect();
  audioDriver->clear();

  for(string& driver : audio.hasDrivers()) {
    audioDriver->addItem(QString::fromUtf8(driver));
    if(driver == config->audio.driver) audioDriver->setCurrentText(QString::fromUtf8(driver));
  }

  connect(audioDriver, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
    if(index >= 0) config->audio.driver = audioDriver->itemText(index).toUtf8().data();
  });
}

void AdvancedSettings::updateInputDriver() {
  inputDriver->disconnect();
  inputDriver->clear();

  for(string& driver : input.hasDrivers()) {
    inputDriver->addItem(QString::fromUtf8(driver));
    if(driver == config->input.driver) inputDriver->setCurrentText(QString::fromUtf8(driver));
  }

  connect(inputDriver, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
    if(index >= 0) config->input.driver = inputDriver->itemText(index).toUtf8().data();
  });
}

void AdvancedSettings::updateConfiguration() {
  regionAuto->setChecked(config->sfc.region == "Auto");
  regionNTSC->setChecked(config->sfc.region == "NTSC");
  regionPAL->setChecked (config->sfc.region == "PAL");

  portSatellaview->setChecked(emulator->connected(SFC::ID::Port::Expansion) == SFC::ID::Device::Satellaview);
  portNone->setChecked       (emulator->connected(SFC::ID::Port::Expansion) == SFC::ID::Device::None);

  focusPause->setChecked (config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyPauseEmulation);
  focusIgnore->setChecked(config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyIgnoreInput);
  focusAllow->setChecked (config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyAllowInput);

  rewindEnable->setChecked(config->system.rewindEnabled);
  useCommonDialogs->setChecked(config->diskBrowser.useCommonDialogs);

  #if defined(DOUBLE_VRAM)
  hacksDoubleVRAM->setChecked(emulator->getProperty("system/ppu1/vram/size").natural() == 131072);
  #endif
  hacksNoSpriteLimit->setChecked(emulator->getOption("hack/ppu/noSpriteLimit") == "true");
  hacksHiresMode7->setChecked(emulator->getOption("hack/ppu/hiresMode7") == "true");
}
