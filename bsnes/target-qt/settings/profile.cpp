ProfileSettings::ProfileSettings(bool hasAccCom, bool hasPerformance) {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(0);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  profileInfo = new QLabel(
    "Profiles allow you to balance emulation accuracy against system performance.<br>"
    "Note that you must restart bsnes-mcfly to switch from or to the Performance profile!<br>"
    "Switching from Accuracy to Compatibility or back merely requires loading a new game.<br>"
    "Also, while save RAM is compatible between profiles, save states are not cross-compatible."
  );
  layout->addWidget(profileInfo);
  layout->addSpacing(Style::WidgetSpacing);

  if(hasAccCom) {
    profileAccuracy = new QRadioButton("Accuracy");
    profileAccuracy->setStyleSheet("font-weight: bold; font-size: 12pt;");
    connect(profileAccuracy, &QRadioButton::pressed, [this]() {
      config->system.profile = "accuracy";
    });
    layout->addWidget(profileAccuracy);

    profileAccuracyInfo = new QLabel(
    //"<b>System Requirements:</b> A super-computer cooled by LN<sub>2</sub>.<br>"
      "<b>System Requirements:</b> ~3.2 GHz processor (Intel Core i5/AMD FX)<br>"
      "Maximum accuracy, no matter the cost.<br>"
      "Use this mode for development or research purposes."
    );
    profileAccuracyInfo->setStyleSheet("margin-left: 22px;");
    layout->addWidget(profileAccuracyInfo);

    layout->addSpacing(Style::WidgetSpacing);

    profileCompatibility = new QRadioButton("Compatibility");
    profileCompatibility->setStyleSheet("font-weight: bold; font-size: 12pt;");
    connect(profileCompatibility, &QRadioButton::pressed, [this]() {
      config->system.profile = "compatibility";
    });
    layout->addWidget(profileCompatibility);

    profileCompatibilityInfo = new QLabel(
      "<b>System Requirements:</b> ~2.4 GHz processor (Intel Core Duo/AMD Athlon 64 X2)<br>"
      "High accuracy with speed that increases depending on available CPU cores.<br>"
      "Very rarely, the Accuracy profile may be automatically activated in<br>"
      "a small number of games to work around slight graphical or audio glitches."
    );
    profileCompatibilityInfo->setStyleSheet("margin-left: 22px;");
    layout->addWidget(profileCompatibilityInfo);
  }

  if(hasPerformance) {
    if(hasAccCom) layout->addSpacing(Style::WidgetSpacing);

    profilePerformance = new QRadioButton("Performance");
    profilePerformance->setStyleSheet("font-weight: bold; font-size: 12pt;");
    connect(profilePerformance, &QRadioButton::pressed, [this]() {
      config->system.profile = "performance";
    });
    layout->addWidget(profilePerformance);

    profilePerformanceInfo = new QLabel(
      "<b>System Requirements:</b> ~1.5 GHz processor (Intel Pentium IV or AMD Athlon)<br>"
      "Moderate accuracy with reasonable compromises for performance.<br>"
      "Sacrifices a small degree of compatibility to run full-speed on older hardware.<br>"
      "Use this mode for slower systems, or if you are running on battery power."
    );
    profilePerformanceInfo->setStyleSheet("margin-left: 22px;");
    layout->addWidget(profilePerformanceInfo);
  }

  if(config->system.profile == "balanced" || config->system.profile == "fast") {
    config->system.profile = "compatibility";
  }

  if(!config->system.profile
  || config->system.profile == "accuracy" && !hasAccCom
  || config->system.profile == "compatibility" && !hasAccCom
  || config->system.profile == "performance" && !hasPerformance) {
    config->system.profile = hasAccCom ? "compatibility" : "performance";
    if((uint)hasAccCom * 2 + (uint)hasPerformance >= 2) {
      QMessageBox::information(0, "First-Run Notice",
        "<b>Note:</b> bsnes-mcfly contains multiple emulation profiles.<br><br>"
        "If bsnes-mcfly runs too slowly, you can greatly increase the speed by using the "
        "'Performance' profile; or if you want even more accuracy, you can use the "
        "'Accuracy' profile.<br><br>"
        "Feel free to experiment. You can select different profiles via:<br>"
        "Settings -> Configuration -> Profile"
      );
    }
  }
  if(config->system.profile == "accuracy") {
    profileAccuracy->setChecked(true);
  } else if(config->system.profile == "compatibility") {
    profileCompatibility->setChecked(true);
  } else if(config->system.profile == "performance") {
    profilePerformance->setChecked(true);
  }
}
