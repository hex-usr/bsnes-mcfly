#include "../qt.hpp"

#include "profile.cpp"
#include "video.cpp"
#include "audio.cpp"
#include "input.cpp"
#include "paths.cpp"
#include "advanced.cpp"

unique_pointer<SettingsWindow> settingsWindow;

SettingsWindow::SettingsWindow() {
  setObjectName("settings-window");
  setWindowTitle("Configuration Settings");
  resize(600, 360);
  setGeometryString(&config->geometry.settingsWindow);

  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  #if defined(PLATFORM_WINDOWS)
  string suffix = ".dll";
  #else
  string suffix = "";
  #endif

  #if !defined(PROFILE_PERFORMANCE)
  bool hasAccuracy = true;
  #else
  bool hasAccuracy = file::exists({Location::path(Path::program()), "bsnes-accuracy-compatibility", suffix});
  #endif

  #if defined(PROFILE_PERFORMANCE)
  bool hasPerformance = true;
  #else
  bool hasPerformance = file::exists({Location::path(Path::program()), "bsnes-performance", suffix});
  #endif

  bool showProfileTab = (uint)hasAccuracy * 2 + (uint)hasPerformance >= 2;

  if(showProfileTab) {
    profileSettings = new ProfileSettings(hasAccuracy, hasPerformance);
    profileArea = new QScrollArea;
    profileArea->setWidget(profileSettings);
    profileArea->setFrameStyle(0);
    profileArea->setWidgetResizable(true);
  }

  videoSettings = new VideoSettings;
  videoArea = new QScrollArea;
  videoArea->setWidget(videoSettings);
  videoArea->setFrameStyle(0);
  videoArea->setWidgetResizable(true);

  audioSettings = new AudioSettings;
  audioArea = new QScrollArea;
  audioArea->setWidget(audioSettings);
  audioArea->setFrameStyle(0);
  audioArea->setWidgetResizable(true);

  inputSettings = new InputSettings;
  inputArea = new QScrollArea;
  inputArea->setWidget(inputSettings);
  inputArea->setFrameStyle(0);
  inputArea->setWidgetResizable(true);

  pathSettings = new PathSettings;
  pathArea = new QScrollArea;
  pathArea->setWidget(pathSettings);
  pathArea->setFrameStyle(0);
  pathArea->setWidgetResizable(true);

  advancedSettings = new AdvancedSettings;
  advancedArea = new QScrollArea;
  advancedArea->setWidget(advancedSettings);
  advancedArea->setFrameStyle(0);
  advancedArea->setWidgetResizable(true);

  tab = new QTabWidget;
  if(showProfileTab) tab->addTab(profileArea, "Profile");
  tab->addTab(videoArea, "Video");
  tab->addTab(audioArea, "Audio");
  tab->addTab(inputArea, "Input");
  tab->addTab(pathArea, "Paths");
  tab->addTab(advancedArea, "Advanced");
  layout->addWidget(tab);
}

SettingsWindow::~SettingsWindow() {
  tab->clear();
  if(profileSettings) delete profileSettings;
  delete videoSettings;
  delete audioSettings;
  delete inputSettings;
  delete pathSettings;
  delete advancedSettings;
  if(profileArea) delete profileArea;
  delete videoArea;
  delete audioArea;
  delete inputArea;
  delete pathArea;
  delete advancedArea;
  delete tab;
  delete layout;
}
