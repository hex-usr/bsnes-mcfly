struct WindowManager {
  auto updateFullscreenState() -> void;
  auto constrainSize(uint& x, uint& y, uint max) -> void;
  auto resizeMainWindow() -> void;
  auto toggleSynchronizeVideo() -> void;
  auto toggleSynchronizeAudio() -> void;
  auto setNtscMode() -> void;
  auto setPalMode() -> void;
  auto toggleSmoothVideoOutput() -> void;
  auto toggleAspectCorrection() -> void;
  auto setScale(uint) -> void;
  auto toggleFullscreen() -> void;
  auto toggleMenubar() -> void;
  auto toggleStatusbar() -> void;

  bool isFullscreen = false;
};

extern WindowManager windowManager;
