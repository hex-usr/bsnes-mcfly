auto Program::powerOn() -> void {
  resetRewindHistory();  //do not allow rewinding past a destructive system action
  movie.stop();  //movies cannot continue to record after destructive system actions

  video.clear();
  audio.clear();

  if(emulator->loaded() && !power) {
    power = true;
    pause = false;
    emulator->power();

    showMessage("Power on.");
  }

  presentation->syncUi();
  toolsWindow->cheatFinder->synchronize();
}

auto Program::powerOff() -> void {
  resetRewindHistory();  //do not allow rewinding past a destructive system action
  movie.stop();  //movies cannot continue to record after destructive system actions

  video.clear();
  audio.clear();

  if(emulator->loaded() && power) {
    power = false;
    pause = true;
    emulator->power();

    showMessage("Power off.");
  }

  presentation->syncUi();
  toolsWindow->cheatFinder->synchronize();
}

auto Program::powerCycle() -> void {
  resetRewindHistory();  //do not allow rewinding past a destructive system action
  movie.stop();  //movies cannot continue to record after destructive system actions

  video.clear();
  audio.clear();

  if(emulator->loaded()) {
    power = true;
    pause = false;
    emulator->power();

    showMessage("System power was cycled.");
  }

  presentation->syncUi();
  toolsWindow->cheatFinder->synchronize();
}

auto Program::softReset() -> void {
  resetRewindHistory();  //do not allow rewinding past a destructive system action
  movie.stop();  //movies cannot continue to record after destructive system actions

  video.clear();
  audio.clear();

  if(emulator->loaded() && power) {
    pause = false;
    emulator->reset();

    showMessage("System was reset.");
  }

  presentation->syncUi();
  toolsWindow->cheatFinder->synchronize();
}

auto Program::connectDevices() -> void {
  #define connectDevice(device, controller)\
    emulator->connect(SFC::ID::Port::Controller1, SFC::ID::Device::device);\
    inputManager->port1 = controller
  switch(config->input.port1) { default:
  case ControllerPort1::None: connectDevice(None, nullptr); break;
  case ControllerPort1::Gamepad: connectDevice(Gamepad, &Controllers::gamepad1); break;
  case ControllerPort1::Asciipad: connectDevice(Gamepad, &Controllers::asciipad1); break;
  case ControllerPort1::Multitap: connectDevice(SuperMultitap, &Controllers::multitap1); break;
  case ControllerPort1::Mouse: connectDevice(Mouse, &Controllers::mouse1); break;
  }
  #undef connectDevice

  #define connectDevice(device, controller)\
    emulator->connect(SFC::ID::Port::Controller2, SFC::ID::Device::device);\
    inputManager->port2 = controller
  switch(config->input.port2) { default:
  case ControllerPort2::None: connectDevice(None, nullptr); break;
  case ControllerPort2::Gamepad: connectDevice(Gamepad, &Controllers::gamepad2); break;
  case ControllerPort2::Asciipad: connectDevice(Gamepad, &Controllers::asciipad2); break;
  case ControllerPort2::Multitap: connectDevice(SuperMultitap, &Controllers::multitap2); break;
  case ControllerPort2::Mouse: connectDevice(Mouse, &Controllers::mouse2); break;
  case ControllerPort2::SuperScope: connectDevice(SuperScope, &Controllers::superscope); break;
  case ControllerPort2::Justifier: connectDevice(Justifier, &Controllers::justifier1); break;
  case ControllerPort2::Justifiers: connectDevice(Justifiers, &Controllers::justifiers); break;
  }
  #undef connectDevice

  presentation->syncUi();
}

//display message in main window statusbar area for three seconds
auto Program::showMessage(const string& text) -> void {
  presentation->statusBar->showMessage(QString::fromUtf8(text), 3000);
}

//updates system state text at bottom-right of main window statusbar
auto Program::updateStatusText() -> void {
  string text;

  if(!emulator->loaded()) {
    text = "No cartridge loaded";
  } else if(!power) {
    text = "Power off";
  } else if(pause || autopause) {
    text = "Paused";
  } else if(framesUpdated) {
    framesUpdated = false;
    text = {framesExecuted, " fps"};
  } else {
    //nothing to update
    return;
  }

  presentation->systemState->setText(QString::fromUtf8(string{"  ", text, "  "}));
}

auto Program::focused() -> bool {
  //exclusive mode creates its own top-level window: presentation window will not have focus
  if(video.exclusive()) return true;
  if(presentation && presentation->isActive()) return true;
  return false;
}

auto Program::captureScreenshot(uint32_t* data, uint pitch, uint width, uint height) -> void {
  saveScreenshot = false;
  QImage image((const uint8_t*)data, width, height, pitch, QImage::Format_RGB32);
  if(width >  256 && height <= 240) image = image.scaled({(int)width << 0, (int)height << 1});
  if(width <= 256 && height >  240) image = image.scaled({(int)width << 1, (int)height << 0});

  if(image.save(QString::fromUtf8(screenshotPath()))) {
    showMessage("Screenshot saved.");
  } else {
    showMessage("Could not save screenshot.");
  }
}
