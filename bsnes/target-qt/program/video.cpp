auto Program::updateVideoFormat() -> void {
  if(!video.hasFormat(config->video.format)) {
    config->video.format = video.format();
  }
  video.setFormat(config->video.format);
  higan::video.setDepth(config->video.format == "RGB30" ? 30 : 24);
  higan::video.setPalette();
}

auto Program::updateVideoPalette() -> void {
  double saturation = config->video.saturation / 100.0;
  double gamma = config->video.gamma / 100.0;
  double luminance = config->video.luminance / 100.0;
  higan::video.setSaturation(saturation);
  higan::video.setGamma(gamma);
  higan::video.setLuminance(luminance);
  higan::video.setPalette();
}

auto Program::bindVideoFilter() -> void {
  if(filter.open()) filter.close();
  if(config->video.filter == "None") return;
  if(filter.openAbsolute(config->video.filter)) {
    filter.dl_size = filter.sym("filter_size");
    filter.dl_render = filter.sym("filter_render");
    if(!filter.dl_size || !filter.dl_render) filter.close();
  }
}

auto Program::updateVideoShader() -> void {
  if(config->video.shader) {
    video.setShader(config->video.shader);
  } else {
    video.setShader(config->video.context->blur ? "Blur" : "None");
  }
}
