auto Program::updateAudioDriver() -> void {
  if(!audio) return;
  audio.clear();
  audio.setExclusive(config->audio.exclusive);
  audio.setFrequency(config->audio.frequency);
  audio.setLatency(config->audio.latency);

  double scale = config->system.speeds[config->system.speed] / 100.0;
  higan::audio.setFrequency(config->audio.frequency / scale);
}

auto Program::updateAudioEffects() -> void {
  auto volume = config->audio.mute ? 0.0 : config->audio.volume * 0.01;
  higan::audio.setVolume(volume);

  higan::audio.setBalance(0.0);
}
