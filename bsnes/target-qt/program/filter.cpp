Filter filter;
ScanlineFilter scanlineFilter;

Filter::Filter() {
  data = new uint32[2048 * 2048];
  pitch = 2048 * sizeof(uint32);
}

Filter::~Filter() {
  delete[] data;
}

auto Filter::render(const uint32* input, uint inputPitch, uint inputWidth, uint inputHeight) -> void {
  width = inputWidth, height = inputHeight;
  dl_size(width, height);
  dl_render((uint32_t*)data, pitch, (const uint32_t*)input, inputPitch, inputWidth, inputHeight);
}

ScanlineFilter::ScanlineFilter() {
  enabled = false;
  data = new uint32[512 * 480];
  adjust = new unsigned[256];
}

ScanlineFilter::~ScanlineFilter() {
  delete[] data;
  delete[] adjust;
}

auto ScanlineFilter::size(uint& width, uint& height) -> void {
  if(height <= 240) height *= 2;
}

auto ScanlineFilter::render(
  const uint32* input, uint pitch,
  uint width, uint height
) -> void {
  pitch >>= 2;

  for(uint y = 0; y < height; y++) {
    const uint32* sp = input + y * pitch;
    uint32* dp = data + y * (512 << 1);
    memory::copy<uint32_t>(dp, sp, width);
  }

  switch(shortcut) {

  case Shortcut::None: {
    for(unsigned y = 0; y < height; y++) {
      const uint32* sp = input + y * pitch;
      uint32* dp = data + y * (512 << 1) + 512;
      for(unsigned x = 0; x < width; x++) {
        uint32 color = *sp++;
        *dp++ = ((color & 0xff000000)
        | adjust[color >> 16 & 0xff] << 16
        | adjust[color >>  8 & 0xff] <<  8
        | adjust[color >>  0 & 0xff] <<  0
        );
      }
    }
    break;
  }

  case Shortcut::Zero: {
    for(unsigned y = 0; y < height; y++) {
      uint32* dp = data + y * (512 << 1) + 512;
      memory::fill<uint32>(dp, width, 0x00000000);
    }
    break;
  }

  case Shortcut::ShiftRight1: {
    for(unsigned y = 0; y < height; y++) {
      const uint32* sp = input + y * pitch;
      uint32* dp = data + y * (512 << 1) + 512;
      for(unsigned x = 0; x < width; x++) {
        uint32 color = *sp++;
        *dp++ = color >> 1 & 0x7f7f7f;
      }
    }
    break;
  }

  case Shortcut::ShiftRight2: {
    for(unsigned y = 0; y < height; y++) {
      const uint32* sp = input + y * pitch;
      uint32* dp = data + y * (512 << 1) + 512;
      for(unsigned x = 0; x < width; x++) {
        uint32 color = *sp++;
        *dp++ = color >> 2 & 0x3f3f3f;
      }
    }
    break;
  }

  }
}

auto ScanlineFilter::setIntensity(uint intensity) -> void {
  if(intensity >= 100) {
    enabled = false;
  } else {
    enabled = true;

    shortcut = Shortcut::None;
    if(intensity ==  0) shortcut = Shortcut::Zero;
    if(intensity == 25) shortcut = Shortcut::ShiftRight2;
    if(intensity == 50) shortcut = Shortcut::ShiftRight1;

    if(shortcut == Shortcut::None) {
      for(unsigned i = 0; i < 256; i++) {
        adjust[i] = (double)i * (double)intensity / 100.0;
      }
    }
  }
}
