auto Program::path(string type, string location, string extension) -> string {
  auto pathname = Location::path(location);
  auto filename = Location::file(location);
  auto prefix = Location::prefix(filename);
  auto suffix = extension;

  if(type == "Games") {
    if(auto path = config->path.rom) {
      pathname = path;
    }
  }

  if(type == "Patches") {
    if(auto path = config->path.patch) {
      pathname = path;
    }
  }

  if(type == "Saves") {
    if(auto path = config->path.save) {
      pathname = path;
    }
  }

  if(type == "Cheats") {
    if(auto path = config->path.cheat) {
      pathname = path;
    }
  }

  if(type == "States") {
    if(auto path = config->path.state) {
      pathname = path;
    }
  }

  if(type == "Screenshots") {
    if(auto path = config->path.data) {
      pathname = path;
    }
  }

  if(type == "Movies") {
    if(auto path = config->path.data) {
      pathname = path;
    }
  }

  return {pathname, prefix, suffix};
}

auto Program::gamePath() -> string {
  //bsnes v073's scheme:
  //When using the Sufami Turbo or Super Game Boy, the sub-cartridge would be used.
  //This only applied to BS Memory if the base cartridge was the BS-X BIOS.
  if(!emulator->loaded()) return "";
  if(gameBoy.location) return gameBoy.location;
  if(bsMemory.location && SFC::cartridge.has.MCC) return bsMemory.location;
  if(sufamiTurboA.location) return sufamiTurboA.location;
  return superFamicom.location;
}

auto Program::cheatPath(bool gameGenie) -> string {
  if(!emulator->loaded()) return "";
  auto location = gamePath();
  if(location.endsWith("/")) {
    //bsnes-mcfly retains Game Genie cheats, while the official bsnes
    //automatically decrypts them. Therefore, they cannot share cheat files.
    return {location, gameGenie ? "bsnes-mcfly/cheats.bml" : "cheats.bml"};
  } else {
    return path("Cheats", location, gameGenie ? ".gg" : ".cht");
  }
}

//higan stores each save state in raw format.
//bsnes stores each save state RLE-compressed along with a screenshot preview.
//The performance profile is unique to bsnes-mcfly and can do its own thing.

auto Program::statePath() -> string {
  if(!emulator->loaded()) return "";
  auto location = gamePath();
  if(location.endsWith("/")) {
    switch(activeProfile) {
    case Profile::Accuracy:      return {location, "higan/states/"};
    case Profile::Compatibility: return {location, "bsnes/states/"};
    case Profile::Performance:   return {location, "bsnes-mcfly/states/"};
    }
    unreachable;
  } else {
    return path("States", location, ".bsz");
  }
}

auto Program::statePath(uint id, bool managed) -> string {
  if(!emulator->loaded()) return "";
  auto location = gamePath();
  if(location.endsWith("/")) {
    string slot = pad(id, managed ? 2 : 1, '0');
    switch(activeProfile) {
    case Profile::Accuracy:      return {statePath(), managed ? "managed/" : "quick/", "state-", slot, ".bst"};
    case Profile::Compatibility: return {statePath(), managed ? "Managed/" : "Quick/", "Slot ", slot, ".bst"};
    case Profile::Performance:   return {statePath(), managed ? "Managed/" : "Quick/", "Slot ", slot, ".bst"};
    }
    unreachable;
  } else {
    return path("States", location, {"-", pad(id, managed ? 2 : 1, '0'), ".bst"});
    //return path("States", location, ".bsz");
  }
}

auto Program::screenshotPath() -> string {
  if(!emulator->loaded()) return "";
  auto location = gamePath();
  
  time_t systemTime = time(0);
  tm* currentTime = localtime(&systemTime);
  char t[512];
  sprintf(t, "%.4u%.2u%.2u-%.2u%.2u%.2u",
    1900 + currentTime->tm_year, 1 + currentTime->tm_mon, currentTime->tm_mday,
    currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec
  );
  string suffix = {"-", t, ".png"};

  if(location.endsWith("/")) {
    return {location, "screenshot", suffix};
  } else {
    return path("Screenshots", location, suffix);
  }
}

auto Program::moviePath() -> string {
  if(!emulator->loaded()) return "";
  auto location = gamePath();
  
  time_t systemTime = time(0);
  tm* currentTime = localtime(&systemTime);
  char t[512];
  sprintf(t, "%.4u%.2u%.2u-%.2u%.2u%.2u",
    1900 + currentTime->tm_year, 1 + currentTime->tm_mon, currentTime->tm_mday,
    currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec
  );
  string suffix = {"-", t, ".bsv"};

  if(location.endsWith("/")) {
    return {location, "bsnes-mcfly/movie", suffix};
  } else {
    return path("Movies", location, suffix);
  }
}
