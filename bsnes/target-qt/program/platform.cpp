auto Program::open(uint id, string name, vfs::file::mode mode, bool required) -> vfs::shared::file {
  vfs::shared::file result;

  if(id == 0) {  //System
    if(name == "boards.bml" && mode == vfs::file::mode::read) {
      const uint size1 = sizeof(Resource::System::Boards) - 1;
      const uint size2 = sizeof(Resource::System::BoardsUnofficial) - 1;
      vector<char> buffer;
      buffer.resize(size1 + size2);
      memory::copy<char>(buffer.data(),         Resource::System::Boards,           size1);
      memory::copy<char>(buffer.data() + size1, Resource::System::BoardsUnofficial, size2);

      result = vfs::memory::file::open(buffer.data(), buffer.size());
    }

    if(name == "ipl.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(Resource::System::IPLROM, sizeof(Resource::System::IPLROM));
    }
  }

  if(id == 1) {  //Super Famicom
    if(name == "manifest.bml" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(superFamicom.manifest.data<uint8_t>(), superFamicom.manifest.size());
    } else if(name == "program.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(superFamicom.program.data(), superFamicom.program.size());
    } else if(name == "data.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(superFamicom.data.data(), superFamicom.data.size());
    } else if(name == "expansion.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(superFamicom.expansion.data(), superFamicom.expansion.size());
    } else if(superFamicom.location.endsWith("/")) {
      result = openPakSuperFamicom(name, mode);
    } else {
      result = openRomSuperFamicom(name, mode);
    }
  }

  #if defined(CORE_GB)
  if(id == 2) {  //Game Boy
    if(name == "manifest.bml" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(gameBoy.manifest.data<uint8_t>(), gameBoy.manifest.size());
    } else if(name == "program.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(gameBoy.program.data(), gameBoy.program.size());
    } else if(gameBoy.location.endsWith("/")) {
      result = openPakGameBoy(name, mode);
    } else {
      result = openRomGameBoy(name, mode);
    }
  }
  #endif

  if(id == 3) {  //BS Memory
    if(name == "manifest.bml" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(bsMemory.manifest.data<uint8_t>(), bsMemory.manifest.size());
    } else if(name == "program.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(bsMemory.program.data(), bsMemory.program.size());
    } else if(name == "program.flash") {
      result = vfs::memory::file::open(bsMemory.program.data(), bsMemory.program.size());
    } else if(bsMemory.location.endsWith("/")) {
      result = openPakBSMemory(name, mode);
    } else {
      result = openRomBSMemory(name, mode);
    }
  }

  if(id == 4) {  //Sufami Turbo - Slot A
    if(name == "manifest.bml" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(sufamiTurboA.manifest.data<uint8_t>(), sufamiTurboA.manifest.size());
    } else if(name == "program.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(sufamiTurboA.program.data(), sufamiTurboA.program.size());
    } else if(sufamiTurboA.location.endsWith("/")) {
      result = openPakSufamiTurboA(name, mode);
    } else {
      result = openRomSufamiTurboA(name, mode);
    }
  }

  if(id == 5) {  //Sufami Turbo - Slot B
    if(name == "manifest.bml" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(sufamiTurboB.manifest.data<uint8_t>(), sufamiTurboB.manifest.size());
    } else if(name == "program.rom" && mode == vfs::file::mode::read) {
      result = vfs::memory::file::open(sufamiTurboB.program.data(), sufamiTurboB.program.size());
    } else if(sufamiTurboB.location.endsWith("/")) {
      result = openPakSufamiTurboB(name, mode);
    } else {
      result = openRomSufamiTurboB(name, mode);
    }
  }

  if(!result && required) {
    QMessageBox::critical(&*presentation, "bsnes-mcfly", QString::fromUtf8(string{
      "Error: missing required data: ", name
    }));
  }

  return result;
}

auto Program::load(uint id, string name, string type, vector<string> options) -> higan::Platform::Load {
  if(id == 1 && name == "Super Famicom" && type == "sfc") {
    if(gameQueue) {
      superFamicom.location = gameQueue.takeLeft();
    } else {
      /*
      dialog.setTitle("Load Super Famicom");
      dialog.setPath(path("Games", settings["Path/Recent/SuperFamicom"].text()));
      dialog.setFilters({string{"Super Famicom Games|*.sfc:*.smc:*.zip"}});
      superFamicom.location = dialog.openObject();
      */
    }
    if(inode::exists(superFamicom.location)) {
      if(loadSuperFamicom(superFamicom.location)) {
        return {id, config->sfc.region};
      }
    }
  }

  #if defined(CORE_GB)
  if(id == 2 && name == "Game Boy" && type == "gb") {
    if(gameQueue) {
      gameBoy.location = gameQueue.takeLeft();
    } else {
      /*
      dialog.setTitle("Load Game Boy");
      dialog.setPath(path("Games", settings["Path/Recent/GameBoy"].text()));
      dialog.setFilters({string{"Game Boy Games|*.gb:*.gbc:*.zip"}});
      gameBoy.location = dialog.openObject();
      */
    }
    if(inode::exists(gameBoy.location)) {
      if(loadGameBoy(gameBoy.location)) {
        return {id};
      }
    }
  }
  #endif

  if(id == 3 && name == "BS Memory" && type == "bs") {
    if(gameQueue) {
      bsMemory.location = gameQueue.takeLeft();
    } else {
      /*
      dialog.setTitle("Load BS Memory");
      dialog.setPath(path("Games", settings["Path/Recent/BSMemory"].text()));
      dialog.setFilters({string{"BS Memory Games|*.bs:*.zip"}});
      bsMemory.location = dialog.openObject();
      */
    }
    if(inode::exists(bsMemory.location)) {
      if(loadBSMemory(bsMemory.location)) {
        return {id};
      }
    }
  }

  if(id == 4 && name == "Sufami Turbo" && type == "st") {
    if(gameQueue) {
      sufamiTurboA.location = gameQueue.takeLeft();
    } else {
      /*
      dialog.setTitle("Load Sufami Turbo - Slot A");
      dialog.setPath(path("Games", settings["Path/Recent/SufamiTurboA"].text()));
      dialog.setFilters({string{"Sufami Turbo Games|*.st:*.zip"}});
      sufamiTurboA.location = dialog.openObject();
      */
    }
    if(inode::exists(sufamiTurboA.location)) {
      if(loadSufamiTurboA(sufamiTurboA.location)) {
        return {id};
      }
    }
  }

  if(id == 5 && name == "Sufami Turbo" && type == "st") {
    if(gameQueue) {
      sufamiTurboB.location = gameQueue.takeLeft();
    } else {
      /*
      dialog.setTitle("Load Sufami Turbo - Slot B");
      dialog.setPath(path("Games", settings["Path/Recent/SufamiTurboB"].text()));
      dialog.setFilters({string{"Sufami Turbo Games|*.st:*.zip"}});
      sufamiTurboB.location = dialog.openObject();
      */
    }
    if(inode::exists(sufamiTurboB.location)) {
      if(loadSufamiTurboB(sufamiTurboB.location)) {
        return {id};
      }
    }
  }

  gameQueue.reset();
  return {};
}

auto Program::videoFrame(const uint32* data, uint pitch, uint width, uint height) -> void {
  //This hack allows every profile —including Accuracy— to output 240 scanlines
  //instead of 480 when not using interlace mode.
  //However, this hack will shred the Super Scope/Justifier cursors, so it is
  //disabled when either of those controllers are in use.
  if(!SFC::ppu.interlace() && height > 240 && config->input.port2 < ControllerPort2::SuperScope) {
    height >>= 1;
    pitch  <<= 1;
  }

  //this relies on the UI only running between higan::Scheduler::Event::Frame events
  //this will always be the case; so we can avoid an unnecessary copy or one-frame delay here
  //if the core were to exit between a frame event, the next frame might've been only partially rendered
  screenshot.data = data;
  screenshot.pitch = pitch;
  screenshot.width = width;
  screenshot.height = height;

  pitch >>= 2;
  if(config->video.context->region == 0) {
    if(height == 239) data +=  8 * pitch, height -= 15;
    if(height == 240) data +=  8 * pitch, height -= 16;
    if(height == 478) data += 16 * pitch, height -= 30;
    if(height == 480) data += 16 * pitch, height -= 32;
  }

  //scale crop* values from percentage-based (0-100%) to exact pixel sizes (width, height)
  uint maskLeft = (double)cropLeft / 100.0 * width;
  uint maskTop = (double)cropTop / 100.0 * height;
  uint maskRight = (double)cropRight / 100.0 * width;
  uint maskBottom = (double)cropBottom / 100.0 * height;

  data += maskTop * pitch + maskLeft;
  width -= (maskLeft + maskRight);
  height -= (maskTop + maskBottom);

  if(scanlineFilter.enabled && height <= 240) {
    scanlineFilter.render(data, pitch * sizeof(uint32), width, height);
    data = scanlineFilter.data;
    pitch = 512;
    height *= 2;
  }

  if(filter.open()) {
    filter.render(data, pitch * sizeof(uint32), width, height);
    data = filter.data;
    pitch = filter.pitch / sizeof(uint32);
    width = filter.width;
    height = filter.height;
  }

//if(auto [output, length] = video.acquire(filter.open() ? filter.width : width, filter.open() ? filter.height : height); output) {
  if(auto [output, length] = video.acquire(width, height); output) {
    length >>= 2;

    /*
    if(filter.open()) {
      filter.data = reinterpret_cast<uint32*>(output);
      filter.pitch = length * sizeof(uint32);
      filter.render(data, pitch * sizeof(uint32), width, height);
    } else {
    */
    for(uint y = 0; y < height; y++) {
      memory::copy<uint32_t>(output + y * length, data + y * pitch, width);
    }
    /*
    }
    */

    video.release();
    video.output();
    if(saveScreenshot == true) captureScreenshot(output, length * sizeof(uint32), width, height);
  }

  updateRewind();

  //frame counter
  static int frameCount = 0;
  static uint64 previous, current;
  frameCount++;

  current = chrono::timestamp();
  if(current != previous) {
    framesUpdated = true;
    framesExecuted = frameCount;
    frameCount = 0;
    previous = current;
  }
}

auto Program::audioFrame(const double* samples, uint channels) -> void {
  audio.output(samples);
}

auto Program::inputPoll(uint port, uint device, uint input) -> int16 {
  return inputManager->status(port, device, input);
}

auto Program::inputRumble(uint port, uint device, uint input, bool enable) -> void {
}

auto Program::dipSettings(Markup::Node node) -> uint {
  return 0x0000;
}

auto Program::notify(string text) -> void {
  QMessageBox::information(&*presentation, "bsnes-mcfly", QString::fromUtf8(text));
}
