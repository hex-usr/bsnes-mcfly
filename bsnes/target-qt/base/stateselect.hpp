class StateSelectWindow : public Window {
public:
  QGridLayout* layout;
  QPushButton* slot[10];

  void setSlot(uint slot);
  void keyReleaseEvent(QKeyEvent*);
  StateSelectWindow();
};

extern unique_pointer<StateSelectWindow> stateSelectWindow;
