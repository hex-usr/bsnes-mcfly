class HtmlViewerWindow : public Window {
public:
  QVBoxLayout* layout;
  QTextBrowser* document;

  void show(const char* title, const char* htmlData);
  HtmlViewerWindow();
};

extern unique_pointer<HtmlViewerWindow> htmlViewerWindow;
