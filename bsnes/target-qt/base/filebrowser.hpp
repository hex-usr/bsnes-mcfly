class FileBrowser : public FileDialog {
public:
  function<void (const string&)> onChange;
  function<void (const string&)> onActivate;
  function<void (const string&)> onAccept;

  void chooseFile();
  void chooseFolder();
  enum CartridgeMode { LoadDirect, LoadBase, LoadSlot1, LoadSlot2 } cartridgeMode;
  void loadCartridge(CartridgeMode, int = -1);
  void loadShader();

  FileBrowser();

  void change(const string&);
  void activate(const string&);
  void accept(const string&);
  void toggleApplyPatch();

private:
  struct CartridgeInformation {
    string name;
    string region;
    uint romSize;
    uint ramSize;
  } cartinfo;

  QVBoxLayout* previewLayout;
  QLabel* previewInfo;
  QWidget* previewImage;
  QWidget* previewSpacer;
  QCheckBox* previewApplyPatch;
  string folderPath;

  void onChangeCartridge(const string&);
  void onAcceptCartridge(const string&);
  void onAcceptShader(const string&);

  void acceptSuperFamicom(const string& filename);
  void acceptBSMemory(const string& filename);
  void acceptSufamiTurbo(const string& filename);
  void acceptSuperGameBoy(const string& filename);
  void acceptHeuristic(const string& filename);

  bool cartridgeInformation(string filename);
  string decodeShiftJIS(const char* data, uint length);
};

extern unique_pointer<FileBrowser> fileBrowser;
