#include <nall/platform.hpp>
#include <nall/file.hpp>
#include <nall/path.hpp>
#include <nall/stdint.hpp>
#include <nall/string.hpp>
using namespace nall;

auto main(int argc, char** argv) -> int {
  #if defined(PLATFORM_WINDOWS)
  utf8_arguments(argc, argv);
  #endif
  string realPath = Path::program();
  string path = {realPath, "settings-qt.bml"};
  if(!file::exists(path)) path = {Path::userData(), "bsnes-mcfly/settings-qt.bml"};

  string markup = file::read(path);
  auto config = BML::unserialize(markup);
  string profile = config["System/Profile"].text();
  if(profile != "performance") profile = "accuracy-compatibility";

  string binaryName = {"bsnes-", profile};
  #if defined(PLATFORM_WINDOWS)
  binaryName.append(".dll");
  #endif
  string fileName = {realPath, binaryName};

  #if !defined(PLATFORM_WINDOWS)
  char** args = new char*[argc + 1];
  args[0] = strdup(binaryName);
  for(uint i = 1; i < argc; i++) args[i] = strdup(argv[i]);
  args[argc] = 0;
  execvp(args[0], args);
  execv(fileName, args);
  print("[bsnes-mcfly] Error: unable to locate binary file: ", binaryName, "\n");
  #else
  STARTUPINFOW si;
  PROCESS_INFORMATION pi;
  memset(&si, 0, sizeof(STARTUPINFOW));
  if(!CreateProcessW(nall::utf16_t(fileName), GetCommandLineW(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
    MessageBoxA(0, string{"Error: unable to locate binary file: ", binaryName}, "bsnes-mcfly", MB_OK);
  }
  #endif

  return 0;
}
