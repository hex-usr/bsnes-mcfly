struct MessageDialog {
  MessageDialog(const string& text);

  auto error() -> void;

  auto setParent(Window& parent) -> MessageDialog&;

private:
  struct State {
    Window* parent = nullptr;
    string text;
    string title;
  } state;
};
