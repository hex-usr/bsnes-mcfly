class CanvasObject : public QWidget {
public:
  auto dragEnterEvent(QDragEnterEvent*) -> void;
  auto dropEvent(QDropEvent*) -> void;
  auto keyPressEvent(QKeyEvent*) -> void;
  auto keyReleaseEvent(QKeyEvent*) -> void;
};

class CanvasWidget : public CanvasObject {
public:
  auto paintEngine() const -> QPaintEngine*;
  auto mouseReleaseEvent(QMouseEvent*) -> void;
  auto paintEvent(QPaintEvent*) -> void;
};

class AboutWindow : public Window {
public:
  AboutWindow();

  QVBoxLayout* layout;
  struct Logo : public QWidget {
    void paintEvent(QPaintEvent*);
  } *logo;
  QLabel* info;
};

class Presentation : public Window {
public:
  Presentation();
  ~Presentation();

  auto syncUi() -> void;
  auto isActive() -> bool;
  auto resizeEvent(QResizeEvent*) -> void;
  auto closeEvent(QCloseEvent*) -> void;

  auto updateRecentGames() -> void;
  auto addRecentGame(string location) -> void;

  void quit();

  QVBoxLayout* layout;
    QMenuBar* menuBar;
      QMenu* recent;
        QAction* recent_game[10];
        QAction* recent_clear;
      QMenu* system;
        QAction* system_load;
        QMenu* system_loadSpecial;
          QAction* system_loadSpecial_bsxSlotted;
          QAction* system_loadSpecial_bsx;
          QAction* system_loadSpecial_sufamiTurbo;
          QAction* system_loadSpecial_superGameBoy;
        CheckAction* system_power;
        QAction* system_reset;
        QMenu* system_port1;
          RadioAction* system_port1_none;
          RadioAction* system_port1_gamepad;
          RadioAction* system_port1_asciipad;
          RadioAction* system_port1_multitap;
          RadioAction* system_port1_mouse;
        QMenu* system_port2;
          RadioAction* system_port2_none;
          RadioAction* system_port2_gamepad;
          RadioAction* system_port2_asciipad;
          RadioAction* system_port2_multitap;
          RadioAction* system_port2_mouse;
          RadioAction* system_port2_superscope;
          RadioAction* system_port2_justifier;
          RadioAction* system_port2_justifiers;
        QAction* system_exit;
      QMenu* settings;
        QMenu* settings_videoMode;
          RadioAction* settings_videoMode_1x;
          RadioAction* settings_videoMode_2x;
          RadioAction* settings_videoMode_3x;
          RadioAction* settings_videoMode_4x;
          RadioAction* settings_videoMode_5x;
          RadioAction* settings_videoMode_max_normal;
          RadioAction* settings_videoMode_max_wide;
          RadioAction* settings_videoMode_max_wideZoom;
          CheckAction* settings_videoMode_correctAspectRatio;
          RadioAction* settings_videoMode_ntsc;
          RadioAction* settings_videoMode_pal;
        QMenu* settings_videoFilter;
          QAction* settings_videoFilter_configure;
          RadioAction* settings_videoFilter_none;
          vector<RadioAction*> settings_videoFilter_list;
        CheckAction* settings_smoothVideo;
        CheckAction* settings_muteAudio;
        QMenu* settings_emulationSpeed;
          RadioAction* settings_emulationSpeed_slowest;
          RadioAction* settings_emulationSpeed_slow;
          RadioAction* settings_emulationSpeed_normal;
          RadioAction* settings_emulationSpeed_fast;
          RadioAction* settings_emulationSpeed_fastest;
          CheckAction* settings_emulationSpeed_syncVideo;
          CheckAction* settings_emulationSpeed_syncAudio;
        QAction* settings_configuration;
      QMenu* tools;
        QMenu* tools_movies;
          QAction* tools_movies_play;
          QAction* tools_movies_stop;
          QAction* tools_movies_recordFromPowerOn;
          QAction* tools_movies_recordFromHere;
        QAction* tools_captureScreenshot;
        QMenu* tools_loadState;
        QMenu* tools_saveState;
        QAction* tools_cheatEditor;
        QAction* tools_cheatFinder;
        QAction* tools_stateManager;
        QAction* tools_manifestViewer;
        QAction* tools_effectToggle;
      QMenu* help;
        QAction* help_documentation;
        QAction* help_license;
        QAction* help_about;
    CanvasObject* canvasContainer;
      QVBoxLayout* canvasLayout;
        CanvasWidget* canvas;
      QLabel* systemState;
    QStatusBar* statusBar;

private:
  vector<string> videoFilterName;

  auto setupVideoFilters() -> void;
};

extern unique_pointer<AboutWindow> aboutWindow;
extern unique_pointer<Presentation> presentation;
