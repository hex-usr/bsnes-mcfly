#include "../qt.hpp"
#include "about.cpp"
unique_pointer<AboutWindow> aboutWindow;
unique_pointer<Presentation> presentation;

Presentation::Presentation() {
  setObjectName("presentation");
  setWindowTitle(QString::fromUtf8(string{"bsnes-mcfly v", higan::Version}));
  setCloseOnEscape(false);
  setGeometryString(&config->geometry.presentation);

  //menu bar
  #if defined(PLATFORM_MACOS)
  menuBar = new QMenuBar(0);
  #else
  menuBar = new QMenuBar;
  #endif

  recent = menuBar->addMenu("&Recent");

  for(uint slot : range(10)) {
    string prefix = {slot >= 9 ? "1" : "", "&", (slot + 1) % 10};
    recent_game[slot] = recent->addAction(QString::fromUtf8(prefix));
    recent_game[slot]->setData(slot);
    connect(recent_game[slot], &QAction::triggered, [this, slot]() {
      program->gameQueue = config->game.recent[slot].split("|");
      program->load();
    });
  }

  recent->addSeparator();

  recent_clear = recent->addAction("&Clear List");
  connect(recent_clear, &QAction::triggered, this, [&]() {
    for(string& game : config->game.recent) game = "";
    updateRecentGames();
  });

  updateRecentGames();

  system = menuBar->addMenu("&System");

  system_load = system->addAction("Load &Cartridge ...");
  connect(system_load, &QAction::triggered, this, [&]() {
    fileBrowser->setWindowTitle("Load Cartridge");
    fileBrowser->loadCartridge(FileBrowser::LoadDirect);
  });

  system_loadSpecial = system->addMenu("Load &Special");

  system_loadSpecial_bsxSlotted = system_loadSpecial->addAction("Load BS-X &Slotted Cartridge ...");
  connect(system_loadSpecial_bsxSlotted, &QAction::triggered, this, [&]() {
    loaderWindow->loadBsxSlottedCartridge("", "");
  });

  system_loadSpecial_bsx = system_loadSpecial->addAction("Load &BS-X Cartridge ...");
  connect(system_loadSpecial_bsx, &QAction::triggered, this, [&]() {
    loaderWindow->loadBsxCartridge(config->path.bios.bsx, "");
  });

  system_loadSpecial_sufamiTurbo = system_loadSpecial->addAction("Load Sufami &Turbo Cartridge ...");
  connect(system_loadSpecial_sufamiTurbo, &QAction::triggered, this, [&]() {
    loaderWindow->loadSufamiTurboCartridge(config->path.bios.sufamiTurbo, "", "");
  });

  #if defined(CORE_GB)
  system_loadSpecial_superGameBoy = system_loadSpecial->addAction("Load Super &Game Boy Cartridge ...");
  connect(system_loadSpecial_superGameBoy, &QAction::triggered, this, [&]() {
    loaderWindow->loadSuperGameBoyCartridge(config->path.bios.superGameBoy, "");
  });
  #endif

  system->addSeparator();

  system->addAction(system_power = new CheckAction("&Power", 0));
  connect(system_power, &CheckAction::triggered, this, [&]() {
    if(!program->power) {
      program->powerOn();
    } else {
      program->powerOff();
    }
    system_power->setChecked(program->power);
  });

  system_reset = system->addAction("&Reset");
  connect(system_reset, &QAction::triggered, this, [&]() {
    program->softReset();
  });

  system->addSeparator();

  system_port1 = system->addMenu("Controller Port &1");
  system_port1->addAction(system_port1_none = new RadioAction("&None", 0));
  connect(system_port1_none, &RadioAction::triggered, this, [&]() {
    config->input.port1 = ControllerPort1::None; program->connectDevices();
  });
  system_port1->addAction(system_port1_gamepad = new RadioAction("&Gamepad", 0));
  connect(system_port1_gamepad, &RadioAction::triggered, this, [&]() {
    config->input.port1 = ControllerPort1::Gamepad; program->connectDevices();
  });
  system_port1->addAction(system_port1_asciipad = new RadioAction("&asciiPad", 0));
  connect(system_port1_asciipad, &RadioAction::triggered, this, [&]() {
    config->input.port1 = ControllerPort1::Asciipad; program->connectDevices();
  });
  system_port1->addAction(system_port1_multitap = new RadioAction("&Multitap", 0));
  connect(system_port1_multitap, &RadioAction::triggered, this, [&]() {
    config->input.port1 = ControllerPort1::Multitap; program->connectDevices();
  });
  system_port1->addAction(system_port1_mouse = new RadioAction("&Mouse", 0));
  connect(system_port1_mouse, &RadioAction::triggered, this, [&]() {
    config->input.port1 = ControllerPort1::Mouse; program->connectDevices();
  });

  system_port2 = system->addMenu("Controller Port &2");
  system_port2->addAction(system_port2_none = new RadioAction("&None", 0));
  connect(system_port2_none, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::None; program->connectDevices();
  });
  system_port2->addAction(system_port2_gamepad = new RadioAction("&Gamepad", 0));
  connect(system_port2_gamepad, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::Gamepad; program->connectDevices();
  });
  system_port2->addAction(system_port2_asciipad = new RadioAction("&asciiPad", 0));
  connect(system_port2_asciipad, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::Asciipad; program->connectDevices();
  });
  system_port2->addAction(system_port2_multitap = new RadioAction("&Multitap", 0));
  connect(system_port2_multitap, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::Multitap; program->connectDevices();
  });
  system_port2->addAction(system_port2_mouse = new RadioAction("&Mouse", 0));
  connect(system_port2_mouse, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::Mouse; program->connectDevices();
  });
  system_port2->addAction(system_port2_superscope = new RadioAction("&Super Scope", 0));
  connect(system_port2_superscope, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::SuperScope; program->connectDevices();
  });
  system_port2->addAction(system_port2_justifier = new RadioAction("&Justifier", 0));
  connect(system_port2_justifier, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::Justifier; program->connectDevices();
  });
  system_port2->addAction(system_port2_justifiers = new RadioAction("Two &Justifiers", 0));
  connect(system_port2_justifiers, &RadioAction::triggered, this, [&]() {
    config->input.port2 = ControllerPort2::Justifiers; program->connectDevices();
  });

  #if !defined(PLATFORM_MACOS)
  system->addSeparator();
  #endif

  system_exit = system->addAction("E&xit");
  system_exit->setMenuRole(QAction::QuitRole);

  settings = menuBar->addMenu("S&ettings");

  settings_videoMode = settings->addMenu("Video &Mode");

  settings_videoMode->addAction(settings_videoMode_1x = new RadioAction("Scale &1x", 0));
  connect(settings_videoMode_1x, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(1);
  });

  settings_videoMode->addAction(settings_videoMode_2x = new RadioAction("Scale &2x", 0));
  connect(settings_videoMode_2x, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(2);
  });

  settings_videoMode->addAction(settings_videoMode_3x = new RadioAction("Scale &3x", 0));
  connect(settings_videoMode_3x, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(3);
  });

  settings_videoMode->addAction(settings_videoMode_4x = new RadioAction("Scale &4x", 0));
  connect(settings_videoMode_4x, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(4);
  });

  settings_videoMode->addAction(settings_videoMode_5x = new RadioAction("Scale &5x", 0));
  connect(settings_videoMode_5x, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(5);
  });

  settings_videoMode->addAction(settings_videoMode_max_normal = new RadioAction("Scale Max - &Normal", 0));
  connect(settings_videoMode_max_normal, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(6);
  });

  settings_videoMode->addAction(settings_videoMode_max_wide = new RadioAction("Scale Max - &Wide", 0));
  connect(settings_videoMode_max_wide, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(7);
  });

  settings_videoMode->addAction(settings_videoMode_max_wideZoom = new RadioAction("Scale Max - Wide &Zoom", 0));
  connect(settings_videoMode_max_wideZoom, &RadioAction::triggered, this, [&]() {
    windowManager.setScale(8);
  });

  settings_videoMode->addSeparator();

  settings_videoMode->addAction(settings_videoMode_correctAspectRatio = new CheckAction("Correct &Aspect Ratio", 0));
  connect(settings_videoMode_correctAspectRatio, &CheckAction::triggered, this, [&]() {
    windowManager.toggleAspectCorrection();
  });

  settings_videoMode->addSeparator();

  settings_videoMode->addAction(settings_videoMode_ntsc = new RadioAction("&NTSC", 0));
  connect(settings_videoMode_ntsc, &RadioAction::triggered, this, [&]() {
    windowManager.setNtscMode();
  });

  settings_videoMode->addAction(settings_videoMode_pal = new RadioAction("&PAL", 0));
  connect(settings_videoMode_pal, &RadioAction::triggered, this, [&]() {
    windowManager.setPalMode();
  });

  setupVideoFilters();

  settings->addAction(settings_smoothVideo = new CheckAction("&Smooth Video Output", 0));
  connect(settings_smoothVideo, &CheckAction::triggered, this, [&]() {
    windowManager.toggleSmoothVideoOutput();
  });

  settings->addSeparator();

  settings->addAction(settings_muteAudio = new CheckAction("&Mute Audio Output", 0));
  connect(settings_muteAudio, &CheckAction::triggered, this, [&]() {
    settings_muteAudio->toggleChecked();
    config->audio.mute = settings_muteAudio->isChecked();
    program->updateAudioEffects();
  });

  settings->addSeparator();

  settings_emulationSpeed = settings->addMenu("Emulation &Speed");

  settings_emulationSpeed->addAction(settings_emulationSpeed_slowest = new RadioAction("Slowest", 0));
  connect(settings_emulationSpeed_slowest, &RadioAction::triggered, this, [&]() {
    config->system.speed = 0; program->updateAudioDriver(); syncUi();
  });

  settings_emulationSpeed->addAction(settings_emulationSpeed_slow = new RadioAction("Slow", 0));
  connect(settings_emulationSpeed_slow, &RadioAction::triggered, this, [&]() {
    config->system.speed = 1; program->updateAudioDriver(); syncUi();
  });

  settings_emulationSpeed->addAction(settings_emulationSpeed_normal = new RadioAction("Normal", 0));
  connect(settings_emulationSpeed_normal, &RadioAction::triggered, this, [&]() {
    config->system.speed = 2; program->updateAudioDriver(); syncUi();
  });

  settings_emulationSpeed->addAction(settings_emulationSpeed_fast = new RadioAction("Fast", 0));
  connect(settings_emulationSpeed_fast, &RadioAction::triggered, this, [&]() {
    config->system.speed = 3; program->updateAudioDriver(); syncUi();
  });

  settings_emulationSpeed->addAction(settings_emulationSpeed_fastest = new RadioAction("Fastest", 0));
  connect(settings_emulationSpeed_fastest, &RadioAction::triggered, this, [&]() {
    config->system.speed = 4; program->updateAudioDriver(); syncUi();
  });

  settings_emulationSpeed->addSeparator();

  settings_emulationSpeed->addAction(settings_emulationSpeed_syncVideo = new CheckAction("Sync &Video", 0));
  connect(settings_emulationSpeed_syncVideo, &CheckAction::triggered, this, [&]() {
    windowManager.toggleSynchronizeVideo();
  });

  settings_emulationSpeed->addAction(settings_emulationSpeed_syncAudio = new CheckAction("Sync &Audio", 0));
  connect(settings_emulationSpeed_syncAudio, &CheckAction::triggered, this, [&]() {
    windowManager.toggleSynchronizeAudio();
  });

  settings_configuration = settings->addAction("&Configuration ...");
  settings_configuration->setMenuRole(QAction::PreferencesRole);
  connect(settings_configuration, &QAction::triggered, this, [&]() {
    settingsWindow->show();
  });

  tools = menuBar->addMenu("&Tools");

  tools_movies = tools->addMenu("&Movies");

  tools_movies_play = tools_movies->addAction("Play Movie ...");
  connect(tools_movies_play, &QAction::triggered, this, [&]() {
    movie.chooseFile();
    syncUi();
  });

  tools_movies_stop = tools_movies->addAction("Stop");
  connect(tools_movies_stop, &QAction::triggered, this, [&]() {
    movie.stop();
    syncUi();
  });

  tools_movies_recordFromPowerOn = tools_movies->addAction("Record Movie (and restart system)");
  connect(tools_movies_recordFromPowerOn, &QAction::triggered, this, [&]() {
    program->powerCycle();
    movie.record();
    syncUi();
  });

  tools_movies_recordFromHere = tools_movies->addAction("Record Movie (starting from here)");
  connect(tools_movies_recordFromHere, &QAction::triggered, this, [&]() {
    movie.record();
    syncUi();
  });

  tools_captureScreenshot = tools->addAction("&Capture Screenshot");
  connect(tools_captureScreenshot, &QAction::triggered, this, [&]() {
    //tell SFC::Interface to save a screenshot at the next video_refresh() event
    program->saveScreenshot = true;
  });

  tools->addSeparator();

  tools_loadState = tools->addMenu("&Load Quick State");
  for(uint slot = 1; slot <= 10; slot++) {
    QAction* loadAction = new QAction(QString::fromUtf8(string{"Slot ", slot}), 0);
    //loadAction->setData(slot);
    connect(loadAction, &QAction::triggered, this, [=]() {
      program->loadState(slot, false);
    });
    tools_loadState->addAction(loadAction);
  }

  tools_saveState = tools->addMenu("&Save Quick State");
  for(uint slot = 1; slot <= 10; slot++) {
    QAction* saveAction = new QAction(QString::fromUtf8(string{"Slot ", slot}), 0);
    //saveAction->setData(slot);
    connect(saveAction, &QAction::triggered, this, [=]() {
      program->saveState(slot, false);
    });
    tools_saveState->addAction(saveAction);
  }

  tools->addSeparator();

  tools_cheatEditor = tools->addAction("Cheat &Editor ...");
  connect(tools_cheatEditor, &QAction::triggered, this, [&]() {
    toolsWindow->tab->setCurrentIndex(0); toolsWindow->show();
  });

  tools_cheatFinder = tools->addAction("Cheat &Finder ...");
  connect(tools_cheatFinder, &QAction::triggered, this, [&]() {
    toolsWindow->tab->setCurrentIndex(1); toolsWindow->show();
  });

  tools_stateManager = tools->addAction("&State Manager ...");
  connect(tools_stateManager, &QAction::triggered, this, [&]() {
    toolsWindow->tab->setCurrentIndex(2); toolsWindow->show();
  });

  tools_manifestViewer = tools->addAction("Manifest &Viewer ...");
  connect(tools_manifestViewer, &QAction::triggered, this, [&]() {
    toolsWindow->tab->setCurrentIndex(3); toolsWindow->show();
  });

  tools_effectToggle = tools->addAction("Effect &Toggle ...");
  connect(tools_effectToggle, &QAction::triggered, this, [&]() {
    toolsWindow->tab->setCurrentIndex(4); toolsWindow->show();
  });

  help = menuBar->addMenu("&Help");

  help_documentation = help->addAction("&Documentation ...");
  connect(help_documentation, &QAction::triggered, this, [&]() {
    string dml = DML{}.parse(Resource::Help::Documentation, "");
    htmlViewerWindow->show("Usage Documentation", dml);
  });

  help_license = help->addAction("&License ...");
  connect(help_license, &QAction::triggered, this, [&]() {
    htmlViewerWindow->show("License Agreement", Resource::Help::License);
  });

  #if !defined(PLATFORM_MACOS)
  help->addSeparator();
  #endif

  help_about = help->addAction("&About ...");
  help_about->setMenuRole(QAction::AboutRole);
  connect(help_about, &QAction::triggered, this, [&]() {
    aboutWindow->show();
  });

  //canvas
  canvasContainer = new CanvasObject;
  canvasContainer->setAcceptDrops(true); {
    canvasContainer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    canvasContainer->setObjectName("backdrop");

    canvasLayout = new QVBoxLayout; {
      canvasLayout->setMargin(0);
      canvasLayout->setAlignment(Qt::AlignCenter);

      canvas = new CanvasWidget;
      canvas->setAcceptDrops(true);
      canvas->setFocusPolicy(Qt::StrongFocus);
      canvas->setAttribute(Qt::WA_PaintOnScreen, true);  //disable Qt painting on focus / resize

      QPalette palette;
      palette.setColor(QPalette::Window, QColor(0, 0, 0));

      canvas->setPalette(palette);
      canvas->setAutoFillBackground(true);
    }
    canvasLayout->addWidget(canvas);
  }
  canvasContainer->setLayout(canvasLayout);

  //status bar
  statusBar = new QStatusBar;
  statusBar->setSizeGripEnabled(false);
  statusBar->showMessage("");
  systemState = new QLabel;
  statusBar->addPermanentWidget(systemState);

  //layout
  layout = new QVBoxLayout;
  layout->setMargin(0);
  layout->setSpacing(0);
  #if !defined(PLATFORM_MACOS)
  layout->addWidget(menuBar);
  #endif
  layout->addWidget(canvasContainer);
  layout->addWidget(statusBar);
  setLayout(layout);

  //slots
  connect(system_exit, &QAction::triggered, this, &Presentation::quit);

  syncUi();
}

Presentation::~Presentation() {
  for(uint n : range(10)) delete recent_game[n];
  delete recent_clear;
  delete recent;
  delete system_loadSpecial_bsxSlotted;
  delete system_loadSpecial_bsx;
  delete system_loadSpecial_sufamiTurbo;
  delete system_loadSpecial_superGameBoy;
  delete system_loadSpecial;
  delete system_load;
  delete system_power;
  delete system_reset;
  delete system_port1_none;
  delete system_port1_gamepad;
  delete system_port1_asciipad;
  delete system_port1_multitap;
  delete system_port1_mouse;
  delete system_port2_none;
  delete system_port2_gamepad;
  delete system_port2_asciipad;
  delete system_port2_multitap;
  delete system_port2_mouse;
  delete system_port2_superscope;
  delete system_port2_justifier;
  delete system_port2_justifiers;
  delete system_port1;
  delete system_port2;
  delete system_exit;
  delete system;
  delete settings_videoMode_1x;
  delete settings_videoMode_2x;
  delete settings_videoMode_3x;
  delete settings_videoMode_4x;
  delete settings_videoMode_5x;
  delete settings_videoMode_max_normal;
  delete settings_videoMode_max_wide;
  delete settings_videoMode_max_wideZoom;
  delete settings_videoMode_correctAspectRatio;
  delete settings_videoMode_ntsc;
  delete settings_videoMode_pal;
  delete settings_videoMode;
//delete settings_videoFilter_configure;
//delete settings_videoFilter_none;
  for(RadioAction* filter : settings_videoFilter_list) delete filter;
  settings_videoFilter_list.reset();
  delete settings_videoFilter;
  delete settings_smoothVideo;
  delete settings_muteAudio;
  delete settings_emulationSpeed_slowest;
  delete settings_emulationSpeed_slow;
  delete settings_emulationSpeed_normal;
  delete settings_emulationSpeed_fast;
  delete settings_emulationSpeed_fastest;
  delete settings_emulationSpeed_syncVideo;
  delete settings_emulationSpeed_syncAudio;
  delete settings_emulationSpeed;
  delete settings_configuration;
  delete settings;
  delete tools_movies_play;
  delete tools_movies_stop;
  delete tools_movies_recordFromPowerOn;
  delete tools_movies_recordFromHere;
  delete tools_movies;
  delete tools_captureScreenshot;
  delete tools_loadState;
  delete tools_saveState;
  delete tools_cheatEditor;
  delete tools_cheatFinder;
  delete tools_stateManager;
  delete tools_manifestViewer;
  delete tools_effectToggle;
  delete tools;
  delete help_documentation;
  delete help_license;
  delete help_about;
  delete help;
  delete menuBar;
  delete canvas;
  delete canvasLayout;
  delete canvasContainer;
  delete systemState;
  delete statusBar;
  delete layout;
}

auto Presentation::syncUi() -> void {
  system_power->setEnabled(::emulator->loaded());
  system_power->setChecked(program->power);
  system_power->setEnabled(::emulator->loaded());
  system_reset->setEnabled(::emulator->loaded() && program->power);

  system_port1_none->setChecked      (config->input.port1 == ControllerPort1::None);
  system_port1_gamepad->setChecked   (config->input.port1 == ControllerPort1::Gamepad);
  system_port1_asciipad->setChecked  (config->input.port1 == ControllerPort1::Asciipad);
  system_port1_multitap->setChecked  (config->input.port1 == ControllerPort1::Multitap);
  system_port1_mouse->setChecked     (config->input.port1 == ControllerPort1::Mouse);

  system_port2_none->setChecked      (config->input.port2 == ControllerPort2::None);
  system_port2_gamepad->setChecked   (config->input.port2 == ControllerPort2::Gamepad);
  system_port2_asciipad->setChecked  (config->input.port2 == ControllerPort2::Asciipad);
  system_port2_multitap->setChecked  (config->input.port2 == ControllerPort2::Multitap);
  system_port2_mouse->setChecked     (config->input.port2 == ControllerPort2::Mouse);
  system_port2_superscope->setChecked(config->input.port2 == ControllerPort2::SuperScope);
  system_port2_justifier->setChecked (config->input.port2 == ControllerPort2::Justifier);
  system_port2_justifiers->setChecked(config->input.port2 == ControllerPort2::Justifiers);

  settings_videoMode_1x->setChecked        (config->video.context->multiplier == 1);
  settings_videoMode_2x->setChecked        (config->video.context->multiplier == 2);
  settings_videoMode_3x->setChecked        (config->video.context->multiplier == 3);
  settings_videoMode_4x->setChecked        (config->video.context->multiplier == 4);
  settings_videoMode_5x->setChecked        (config->video.context->multiplier == 5);
  settings_videoMode_max_normal->setChecked(config->video.context->multiplier == 6);
  settings_videoMode_max_wide->setChecked  (config->video.context->multiplier == 7);
  settings_videoMode_max_wideZoom->setChecked (config->video.context->multiplier == 8);

  settings_videoMode_max_normal->setVisible  (windowManager.isFullscreen == true);
  settings_videoMode_max_wide->setVisible    (windowManager.isFullscreen == true);
  settings_videoMode_max_wideZoom->setVisible(windowManager.isFullscreen == true);

  settings_videoMode_correctAspectRatio->setChecked(config->video.context->correctAspectRatio);
  settings_videoMode_ntsc->setChecked(config->video.context->region == 0);
  settings_videoMode_pal->setChecked (config->video.context->region == 1);

  //only enable configuration option if the active filter supports it ...
//settings_videoFilter_configure->setEnabled(filter.settings());

  for(uint i = 0; i < settings_videoFilter_list.size(); i++) {
    settings_videoFilter_list[i]->setChecked(config->video.filter == (i == 0 ? "None" : videoFilterName[i - 1]));
  }

  settings_smoothVideo->setChecked(config->video.context->blur);
  settings_muteAudio->setChecked(config->audio.mute);

  settings_emulationSpeed_slowest->setChecked(config->system.speed == 0);
  settings_emulationSpeed_slow->setChecked   (config->system.speed == 1);
  settings_emulationSpeed_normal->setChecked (config->system.speed == 2);
  settings_emulationSpeed_fast->setChecked   (config->system.speed == 3);
  settings_emulationSpeed_fastest->setChecked(config->system.speed == 4);

  settings_emulationSpeed_syncVideo->setChecked(config->video.synchronize);
  settings_emulationSpeed_syncAudio->setChecked(config->audio.synchronize);

  //movies contain save states to synchronize playback to recorded input
  tools_movies->setEnabled(::emulator->loaded());
  if(tools_movies->isEnabled()) {
    tools_movies_play->setEnabled(movie.state == Movie::Inactive);
    tools_movies_stop->setEnabled(movie.state != Movie::Inactive);
    tools_movies_recordFromPowerOn->setEnabled(movie.state == Movie::Inactive);
    tools_movies_recordFromHere->setEnabled(movie.state == Movie::Inactive);
  }
}

auto Presentation::isActive() -> bool {
  return isActiveWindow() && !isMinimized();
}

auto Presentation::resizeEvent(QResizeEvent* event) -> void {
  Window::resizeEvent(event);
  QApplication::processEvents();
}

auto Presentation::closeEvent(QCloseEvent* event) -> void {
  Window::closeEvent(event);
  quit();
}

auto Presentation::updateRecentGames() -> void {
  for(uint index : range(10)) {
    QAction& item = *recent_game[index];
    if(string game = config->game.recent[index]) {
      string displayName;
      auto games = game.split("|");
      for(auto& part : games) {
        displayName.append(Location::prefix(part), " + ");
      }
      displayName.trimRight(" + ", 1L);
      displayName.replace("&", "&&");
      item.setText(QString::fromUtf8(string{index == 9 ? "1" : "", "&", (index + 1) % 10, ": ", displayName}));
      item.setEnabled(true);
    } else {
      item.setText("<empty>");
      item.setEnabled(false);
    }
  }
}

auto Presentation::addRecentGame(string location) -> void {
  vector<string> games;
  games.resize(10);
  for(uint n : range(10)) games[n] = config->game.recent[n];

  if(auto index = games.find(location)) {
    games.remove(index());
  } else {
    games.resize(9);
  }
  games.prepend(location);

  for(uint n : range(10)) config->game.recent[n] = games[n];
  updateRecentGames();
}

void Presentation::quit() {
  hide();
  program->terminate = true;
}

auto Presentation::setupVideoFilters() -> void {
  videoFilterName.reset();

  auto files = directory::files(locate("filters/"), "*.filter");

  if(files.size() > 0) {
    settings_videoFilter = settings->addMenu("Video &Filter");

    /*
    settings_videoFilter_configure = settings_videoFilter->addAction("&Configure Active Filter ...");
    connect(settings_videoFilter_configure, &QAction::triggered, [this]() {
      QWidget *widget = filter.settings();
      if(widget) {
        widget->show();
        widget->activateWindow();
        widget->raise();
      }
    });

    settings_videoFilter->addSeparator();
    */

    settings_videoFilter->addAction(settings_videoFilter_none = new RadioAction("&None", 0));
    connect(settings_videoFilter_none, &RadioAction::triggered, [this]() {
      config->video.filter = "None";
      program->bindVideoFilter();
      syncUi();
    });
    settings_videoFilter_list.append(settings_videoFilter_none);

    for(const string& name : files) {
      string path = {locate("filters/"), name};
      videoFilterName.append(path);

      RadioAction* action = new RadioAction(QString::fromUtf8(Location::prefix(name)), 0);
      settings_videoFilter->addAction(action);
      connect(action, &RadioAction::triggered, [this, path]() {
        config->video.filter = path;
        program->bindVideoFilter();
        syncUi();
      });
      settings_videoFilter_list.append(action);
    }
  }
}

//============
//CanvasObject
//============

//implement drag-and-drop support:
//drag cartridge image onto main window canvas area to load

auto CanvasObject::dragEnterEvent(QDragEnterEvent* event) -> void {
  if(event->mimeData()->hasUrls()) {
    //do not accept multiple files at once
    if(event->mimeData()->urls().count() == 1) event->acceptProposedAction();
  }
}

auto CanvasObject::dropEvent(QDropEvent* event) -> void {
  if(event->mimeData()->hasUrls()) {
    QList<QUrl> list = event->mimeData()->urls();
    if(list.count() == 1) {
      program->gameQueue = {list.at(0).toLocalFile().toUtf8().constData()};
      program->load();
    }
  }
}

//accept all key events for this widget to prevent system chime from playing on OS X
//key events are actually handled by Input class

auto CanvasObject::keyPressEvent(QKeyEvent* event) -> void {
}

auto CanvasObject::keyReleaseEvent(QKeyEvent* event) -> void {
}

//============
//CanvasWidget
//============

//custom video render and mouse capture functionality

auto CanvasWidget::paintEngine() const -> QPaintEngine* {
  if(::emulator->loaded()) {
    video.output();
    return nullptr;
  }
  return QWidget::paintEngine();
}

auto CanvasWidget::mouseReleaseEvent(QMouseEvent* event) -> void {
  //acquire exclusive mode access to mouse when video output widget is clicked
  //(will only acquire if cart is loaded, and mouse / lightgun is in use.)
  if(emulator->loaded()) {
    if(config->input.port1 == ControllerPort1::Mouse
    || config->input.port2 == ControllerPort2::Mouse
    || config->input.port2 == ControllerPort2::SuperScope
    || config->input.port2 == ControllerPort2::Justifier
    || config->input.port2 == ControllerPort2::Justifiers
    ) input.acquire();
  }
}

auto CanvasWidget::paintEvent(QPaintEvent* event) -> void {
  event->ignore();
}
