struct ControllerPort1 { enum { None, Gamepad, Asciipad, Multitap, Mouse }; };
struct ControllerPort2 { enum { None, Gamepad, Asciipad, Multitap, Mouse, SuperScope, Justifier, Justifiers }; };

namespace Controllers {

struct TurboInput : DigitalInput {
  uint holdHi;
  uint holdLo;
  uint counter;
  void cache();
  TurboInput(const char*, const char* = "None");
};

struct Gamepad : InputGroup {
  DigitalInput up, down, left, right, b, a, y, x, l, r, select, start;
  TurboInput turboB, turboA, turboY, turboX, turboL, turboR;
  int16_t status(uint) const;
  Gamepad(uint, const char*);
};

struct Multitap : InputGroup {
  Gamepad& port1;
  Gamepad& port2;
  Gamepad& port3;
  Gamepad& port4;
  int16_t status(uint) const;
  Multitap(Gamepad&, Gamepad&, Gamepad&, Gamepad&);
};

struct AsciiSwitch : DigitalInput {
  enum Mode { Off, Turbo, Auto } mode;
  void poll();
  AsciiSwitch(const char*, const char* = "None");
};

struct AsciiSlowMotion : DigitalInput {
  bool enabled;
  uint holdHi;
  uint holdLo;
  uint counter;
  void poll();
  void cache();
  AsciiSlowMotion(const char*, const char* = "None");
};

struct AsciiInput : DigitalInput {
  AsciiSwitch* asciiSwitch;
  uint holdHi;
  uint holdLo;
  uint counter;
  void cache();
  AsciiInput(const char*, const char* = "None");
};

struct Asciipad : InputGroup {
  DigitalInput up, down, left, right;
  AsciiInput b, a, y, x, l, r;
  DigitalInput select, start;
  AsciiSwitch switchB, switchA, switchY, switchX, switchL, switchR;
  AsciiSlowMotion slowMotion;
  int16_t status(uint) const;
  Asciipad(uint, const char*);
};

struct Mouse : InputGroup {
  RelativeInput x, y;
  DigitalInput left, right;
  int16_t status(uint) const;
  Mouse(uint, const char*);
};

struct SuperScope : InputGroup {
  RelativeInput x, y;
  DigitalInput trigger, cursor, turbo, pause;
  int16_t status(uint) const;
  SuperScope(uint, const char*);
};

struct Justifier : InputGroup {
  RelativeInput x, y;
  DigitalInput trigger, start;
  int16_t status(uint) const;
  Justifier(uint, const char*);
};

struct Justifiers : InputGroup {
  Justifier& port1;
  Justifier& port2;
  int16_t status(uint) const;
  Justifiers(Justifier&, Justifier&);
};

extern Gamepad gamepad1;
extern Asciipad asciipad1;
extern Gamepad multitap1a;
extern Gamepad multitap1b;
extern Gamepad multitap1c;
extern Gamepad multitap1d;
extern Multitap multitap1;
extern Mouse mouse1;

extern Gamepad gamepad2;
extern Asciipad asciipad2;
extern Gamepad multitap2a;
extern Gamepad multitap2b;
extern Gamepad multitap2c;
extern Gamepad multitap2d;
extern Multitap multitap2;
extern Mouse mouse2;
extern SuperScope superscope;
extern Justifier justifier1;
extern Justifier justifier2;
extern Justifiers justifiers;

}
