#include "../qt.hpp"
#include "controller.cpp"
#include "userinterface-general.cpp"
#include "userinterface-system.cpp"
#include "userinterface-emulationspeed.cpp"
#include "userinterface-states.cpp"
#include "userinterface-videosettings.cpp"
InputManager* inputManager;
HID::Null hidNull;

void AbstractInput::bind() {
  auto list = mapping.split("+");

  modifier = InputModifier::None;
  for(uint i = 0; i < list.size(); i++) {
    if(list[i] == "Shift") modifier |= InputModifier::Shift;
    if(list[i] == "Control") modifier |= InputModifier::Control;
    if(list[i] == "Alt") modifier |= InputModifier::Alt;
    if(list[i] == "Super") modifier |= InputModifier::Super;
  }

  auto values = list[list.size() - 1].split("/");
  if(values.size() == 1) return;  //skip "None" mapping

  uint64_t id = toHex(values[0]);
  string group = values(1, "");
  string input = values(2, "");
  string qualifier = values(3, "");

  for(auto device : inputManager->devices) {
    if(id != device->id()) continue;
    if(auto groupID = device->find(group)) {
      if(auto inputID = device->group(groupID()).find(input)) {
        item.device = device;
        item.id = id;
        item.group = groupID();
        item.input = inputID();
        item.qualifier = Input::Qualifier::None;
        if(qualifier == "Lo") item.qualifier = Input::Qualifier::Lo;
        if(qualifier == "Hi") item.qualifier = Input::Qualifier::Hi;
        break;
      }
    }
  }
}

bool AbstractInput::append(string encode) {
  auto mappings = mapping.split(",");
  if(mappings.find(encode)) return true;  //mapping already bound
  if(!mapping || mapping == "None") mapping = encode;  //remove "None"
  else mapping = encode;  //mapping.append(",", encode);  //add to existing mapping list
  bind();
  return true;
}

void AbstractInput::bindDefault() {
  mapping = defaultMapping;
  bind();
}

void AbstractInput::unbind() {
  item.device.reset();
  item.id = 0;
  item.group = 0;
  item.input = 0;
  item.qualifier = Input::Qualifier::None;
  mapping = "None";
}

void AbstractInput::cache() {
  cachedState = state;
}

AbstractInput::AbstractInput(const char* name_, const char* mapping_) :
parent(nullptr),
name(name_),
defaultMapping(mapping_) {
  mapping = defaultMapping;
  state = 0;
  previousState = 0;
  cachedState = 0;
}

//

bool DigitalInput::bind(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue) {
  if(device->isNull() || (device->isKeyboard() && device->group(group).input(input).name() == "Escape")) {
    return unbind(), true;
  }

  string encode = {hex(device->id()), "/", device->group(group).name(), "/", device->group(group).input(input).name()};

  if((device->isKeyboard() && group == HID::Keyboard::GroupID::Button)
  || (device->isMouse() && group == HID::Mouse::GroupID::Button)
  || (device->isJoypad() && group == HID::Joypad::GroupID::Button)
  ) {
    if(newValue != 0) return append({inputManager->modifierString(), encode});
  }

  if((device->isJoypad() && group == HID::Joypad::GroupID::Axis)
  || (device->isJoypad() && group == HID::Joypad::GroupID::Hat)
  || (device->isJoypad() && group == HID::Joypad::GroupID::Trigger)) {
    if(newValue < -16384 && group != HID::Joypad::GroupID::Trigger) {  //triggers are always hi
      return append({inputManager->modifierString(), encode, "/Lo"});
    }

    if(newValue > +16384) {
      return append({inputManager->modifierString(), encode, "/Hi"});
    }
  }

  return false;
}

void DigitalInput::poll() {
  const bool logic = 1;  //AND

  previousState = state;

  if(modifier != inputManager->modifier && ::config->input.modifierEnable) { state = 0; return; }
  if(::config->input.focusPolicy != ConfigurationSettings::Input::FocusPolicyAllowInput && !program->focused()) {
    state = 0;
    return;
  }
  if(!item.device) { state = 0; return; }
  bool result = logic;

  HID::Device& device = *(item.device);
  int16_t value = device.group(item.group).input(item.input).value();
  bool output = logic;
  if((device.isKeyboard() && item.group == HID::Keyboard::GroupID::Button)
  || (device.isMouse() && item.group == HID::Mouse::GroupID::Button)
  || (device.isJoypad() && item.group == HID::Joypad::GroupID::Button)
  ) {
    output = value;
  }
  if((device.isJoypad() && item.group == HID::Joypad::GroupID::Axis)
  || (device.isJoypad() && item.group == HID::Joypad::GroupID::Hat)
  || (device.isJoypad() && item.group == HID::Joypad::GroupID::Trigger)) {
    if(item.qualifier == Input::Qualifier::Lo) output = value < -16384;
    if(item.qualifier == Input::Qualifier::Hi) output = value > +16384;
  }
  if(logic == 0) result |= output;
  if(logic == 1) result &= output;

  state = result;
}

bool DigitalInput::isPressed() const { return state; }
bool DigitalInput::wasPressed() const { return previousState; }

DigitalInput::DigitalInput(const char* name, const char* mapping) : AbstractInput(name, mapping) {
}

//

bool RelativeInput::bind(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue) {
  if(device->isNull() || (device->isKeyboard() && device->group(group).input(input).name() == "Escape")) {
    return unbind(), true;
  }

  string encode = {hex(device->id()), "/", device->group(group).name(), "/", device->group(group).input(input).name()};

  if((device->isMouse() && group == HID::Mouse::GroupID::Axis)
  || (device->isJoypad() && group == HID::Joypad::GroupID::Axis)
  || (device->isJoypad() && group == HID::Joypad::GroupID::Hat)
  ) {
    if(newValue < -16384) return append({inputManager->modifierString(), encode});
    if(newValue > +16384) return append({inputManager->modifierString(), encode});
  }

  return false;
}

void RelativeInput::poll() {
  if(::config->input.focusPolicy != ConfigurationSettings::Input::FocusPolicyAllowInput && !program->focused()) {
    state = 0;
    return;
  }
  if(!item.device) { state = 0; return; }
  int16_t result = 0;

  HID::Device& device = *(item.device);
  int16_t value = device.group(item.group).input(item.input).value();
  if(device.isJoypad() && item.group == HID::Joypad::GroupID::Axis) value >>= 8;
  if(device.isJoypad() && item.group == HID::Joypad::GroupID::Hat) value = (value < 0 ? -1 : value > 0 ? + 1 : 0);
  if(device.isMouse() && input.acquired() == false) value = 0;
  result += value;

  state = result;
}

RelativeInput::RelativeInput(const char* name, const char* mapping) : AbstractInput(name, mapping) {
}

//

void HotkeyInput::poll() {
  DigitalInput::poll();
  if(program->focused() && state != previousState) {
    state ? pressed() : released();
  }
}

HotkeyInput::HotkeyInput(const char* name, const char* mapping) : DigitalInput(name, mapping) {
}

//

void InputGroup::attach(AbstractInput* input) {
  input->parent = this;
  append(input);
}

void InputGroup::bind() {
  for(uint i = 0; i < size(); i++) {
    (*this)[i]->bind();
  }
}

void InputGroup::poll() {
  for(uint i = 0; i < size(); i++) {
    (*this)[i]->poll();
  }
}

void InputGroup::cache() {
  for(uint i = 0; i < size(); i++) {
    (*this)[i]->cache();
  }
}

void InputGroup::flushCache() {
  for(uint i = 0; i < size(); i++) {
    AbstractInput& input = *((*this)[i]);
    input.cachedState = 0;
  }
}

InputGroup::InputGroup(uint category_, const char *name_) : category(category_), name(name_) {
//inputManager->append(this);
}

//

//convert an input mapping string to a more human-readable form for the UI
string InputManager::sanitize(string mapping, string concatenate) const {
  auto values = mapping.split("+");
  auto part = values[values.size() - 1].split("/");
  if(part.size() >= 2) {  //skip "None" mapping
    if(part[0] == "1") part[0] = "Keyboard";
    else if(part[0] == "2") part[0] = "Mouse";
    else part[0] = {"Joypad(", slice(part[0], 0, 3), ")"};
    values[values.size() - 1] = part.merge(".");
  }
  return values.merge(concatenate);
}

void InputManager::onChange(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue) {
  if(settingsWindow->inputSettings->isActiveWindow()) {
    settingsWindow->inputSettings->inputEvent(device, group, input, oldValue, newValue);
  }
}

void InputManager::bind() {
  for(uint i = 0; i < inputList.size(); i++) {
    inputList[i]->bind();
  }
}

void InputManager::poll() {
  auto devices = input.poll();
  bool changed = devices.size() != this->devices.size();
  if(changed == false) {
    for(uint n = 0; n < devices.size(); n++) {
      changed = devices[n] != this->devices[n];
      if(changed) break;
    }
  }
  if(changed == true) {
    cache.keyboard = nullptr;
    cache.mouse = nullptr;
    this->devices = devices;
    for(auto device : devices) {
      if(device->id() == 1) {
        cache.keyboard = &device->group(HID::Keyboard::GroupID::Button);
        cache.escape = cache.keyboard->find("Escape")();
        cache.f4 = cache.keyboard->find("F4")();
        cache.disambiguateLR = (bool)cache.keyboard->find("LeftShift");
        if(cache.disambiguateLR) {
          const string directions[] = {"Left", "Right"};
          for(uint n = 0; n < 2; n++) {
            cache.shift[n]   = cache.keyboard->find({directions[n], "Shift"})();
            cache.control[n] = cache.keyboard->find({directions[n], "Control"})();
            cache.alt[n]     = cache.keyboard->find({directions[n], "Alt"})();
            cache.super[n]   = cache.keyboard->find({directions[n], "Super"})();
          }
        } else {
          cache.shift[0]   = cache.keyboard->find("Shift")();
          cache.control[0] = cache.keyboard->find("Control")();
          cache.alt[0]     = cache.keyboard->find("Alt")();
          cache.super[0]   = cache.keyboard->find("Super")();
        }
      }
      if(device->id() == 2) cache.mouse = device;
    }
    bind();
  }

  modifier = 0;
  if(cache.keyboard != nullptr) {
    for(uint n = 0; n <= cache.disambiguateLR; n++) {
      if(cache.keyboard->input(cache.shift[n]).value()) modifier |= InputModifier::Shift;
      if(cache.keyboard->input(cache.control[n]).value()) modifier |= InputModifier::Control;
      if(cache.keyboard->input(cache.alt[n]).value()) modifier |= InputModifier::Alt;
      if(cache.keyboard->input(cache.super[n]).value()) modifier |= InputModifier::Super;
    }
  }

  for(uint i = 0; i < inputList.size(); i++) {
    inputList[i]->poll();
  }

  //release mouse capture if escape key is pressed, but only when not using fullscreen exclusive mode
  if(!windowManager.isFullscreen || !video.exclusive()) {
    if(cache.keyboard && cache.keyboard->input(cache.escape).value()) {
      input.release();
    }
  }

  //ensure that Alt+F4 will work in fullscreen (don't worry about windowed mode)
  if(windowManager.isFullscreen) {
    if(cache.keyboard && modifier == InputModifier::Alt && cache.keyboard->input(cache.f4).value()) {
      presentation->quit();
    }
  }

  if(::config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyIgnoreInput && !program->focused()) {
    for(uint i = 0; i < inputList.size(); i++) {
      InputGroup& group = *(inputList[i]);
      group.flushCache();
    }
  } else {
    for(uint i = 0; i < inputList.size(); i++) {
      InputGroup& group = *(inputList[i]);
      if(group.category == InputCategory::Port1 || group.category == InputCategory::Port2) {
        group.cache();
      }
    }
  }
}

int16_t InputManager::status(uint port, uint device, uint id) {
  int16_t result = 0;

  if(port == SFC::ID::Port::Controller1 && port1) result = port1->status(id);
  if(port == SFC::ID::Port::Controller2 && port2) result = port2->status(id);

  if(movie.state == Movie::Playback) {
    result = movie.read();
  } else if(movie.state == Movie::Record) {
    movie.write(result);
  }

  return result;
}

string InputManager::modifierString() const {
  string mapping;
  if(modifier & InputModifier::Shift) mapping.append("Shift+");
  if(modifier & InputModifier::Control) mapping.append("Control+");
  if(modifier & InputModifier::Alt) mapping.append("Alt+");
  if(modifier & InputModifier::Super) mapping.append("Super+");
  return mapping;
}

InputManager::InputManager() {
  inputManager = this;

  cache.keyboard = nullptr;
  cache.mouse = nullptr;

  inputList.append(&Controllers::gamepad1);
  inputList.append(&Controllers::asciipad1);
  inputList.append(&Controllers::multitap1a);
  inputList.append(&Controllers::multitap1b);
  inputList.append(&Controllers::multitap1c);
  inputList.append(&Controllers::multitap1d);
  inputList.append(&Controllers::multitap1);
  inputList.append(&Controllers::mouse1);

  inputList.append(&Controllers::gamepad2);
  inputList.append(&Controllers::asciipad2);
  inputList.append(&Controllers::multitap2a);
  inputList.append(&Controllers::multitap2b);
  inputList.append(&Controllers::multitap2c);
  inputList.append(&Controllers::multitap2d);
  inputList.append(&Controllers::multitap2);
  inputList.append(&Controllers::mouse2);
  inputList.append(&Controllers::superscope);
  inputList.append(&Controllers::justifier1);
  inputList.append(&Controllers::justifier2);
  inputList.append(&Controllers::justifiers);

  inputList.append(&userInterfaceGeneral);
  inputList.append(&userInterfaceSystem);
  inputList.append(&userInterfaceEmulationSpeed);
  inputList.append(&userInterfaceStates);
  inputList.append(&userInterfaceVideoSettings);

  for(auto input : inputList) {
    if(input->category == InputCategory::Hidden) continue;
    auto& portConfig = (
    input->category == InputCategory::Port1 ? config.port1 :
    input->category == InputCategory::Port2 ? config.port2 :
    config.userInterface
    );
    for(auto abstract : *input) {
      if(!abstract->mapping) abstract->mapping = "None";
      input->deviceConfig.append(abstract->mapping, string{abstract->name}.replace(" ", ""));
    }
    portConfig.append(input->deviceConfig, string{input->name}.replace(" ", ""));
  }
  config.append(config.port1, "Port1");
  config.append(config.port2, "Port2");
  config.append(config.userInterface, "UserInterface");

  config.load(locate("input-qt.bml"));
  config.save(locate("input-qt.bml"));

  bind();
}

InputManager::~InputManager() {
  config.save(locate("input-qt.bml"));
}
