InputGroup userInterfaceSystem(InputCategory::UserInterface, "System");

namespace UserInterfaceSystem {

struct LoadCartridge : HotkeyInput {
  void pressed() {
    fileBrowser->setWindowTitle("Load Cartridge");
    fileBrowser->loadCartridge(FileBrowser::LoadDirect);
  }

  LoadCartridge() : HotkeyInput("Load Cartridge", "Shift+1/Button/L") {
    userInterfaceSystem.attach(this);
  }
} loadCartridge;

struct LoadBsxSlottedCartridge : HotkeyInput {
  void pressed() {
    loaderWindow->loadBsxSlottedCartridge("", "");
  }

  LoadBsxSlottedCartridge() : HotkeyInput("Load BS-X Slotted Cartridge") {
    userInterfaceSystem.attach(this);
  }
} loadBsxSlottedCartridge;

struct LoadBsxCartridge : HotkeyInput {
  void pressed() {
    loaderWindow->loadBsxCartridge(config->path.bios.bsx, "");
  }

  LoadBsxCartridge() : HotkeyInput("Load BS-X Cartridge") {
    userInterfaceSystem.attach(this);
  }
} loadBsxCartridge;

struct LoadSufamiTurboCartridge : HotkeyInput {
  void pressed() {
    loaderWindow->loadSufamiTurboCartridge(config->path.bios.sufamiTurbo, "", "");
  }

  LoadSufamiTurboCartridge() : HotkeyInput("Load Sufami Turbo Cartridge") {
    userInterfaceSystem.attach(this);
  }
} loadSufamiTurboCartridge;

struct LoadSuperGameBoyCartridge : HotkeyInput {
  void pressed() {
    #if defined(CORE_GB)
    loaderWindow->loadSuperGameBoyCartridge(config->path.bios.superGameBoy, "");
    #endif
  }

  LoadSuperGameBoyCartridge() : HotkeyInput("Load Super Game Boy Cartridge") {
    userInterfaceSystem.attach(this);
  }
} loadSuperGameBoyCartridge;

struct PowerCycle : HotkeyInput {
  void pressed() {
    program->powerCycle();
  }

  PowerCycle() : HotkeyInput("Power Cycle") {
    userInterfaceSystem.attach(this);
  }
} powerCycle;

struct Reset : HotkeyInput {
  void pressed() {
    program->softReset();
  }

  Reset() : HotkeyInput("Reset") {
    userInterfaceSystem.attach(this);
  }
} reset;

struct Pause : HotkeyInput {
  void pressed() {
    program->pause = !program->pause;
    if(program->pause) audio.clear();
  }

  Pause() : HotkeyInput("Pause",
    #if defined(PLATFORM_WINDOWS)
    "Control+1/Button/P"  //can't use Pause because of a conflict with NumLock
    #else
    "1/Button/Pause"
    #endif
  ) {
    userInterfaceSystem.attach(this);
  }
} pause;

}
