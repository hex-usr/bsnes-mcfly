InputGroup userInterfaceEmulationSpeed(InputCategory::UserInterface, "Emulation Speed");

namespace UserInterfaceEmulationSpeed {

//slowdown and speedup do not work well with Vsync enabled, as it locks the
//speed to the monitor refresh rate. thus, when one is pressed, it is disabled
//until the key is released.

struct Slowdown : HotkeyInput {
  void pressed() {
    config->system.speed = 0;
    program->updateAudioDriver();
    video.setBlocking(false);
    audio.setBlocking(true);
    presentation->syncUi();
  }

  void released() {
    config->system.speed = 2;
    program->updateAudioDriver();
    video.setBlocking(config->video.synchronize);
    audio.setBlocking(config->audio.synchronize);
    presentation->syncUi();
  }

  Slowdown() : HotkeyInput("Slowdown", "Shift+1/Button/Tilde") {
    userInterfaceEmulationSpeed.attach(this);
  }
} slowdown;

struct Speedup : HotkeyInput {
  void pressed() {
    emulator->setOption("hack/ppu/frameSkip", 9);
    config->system.speed = 4;
    if(audio.driver() != "XAudio 2.1") program->updateAudioDriver();
    video.setBlocking(false);
    audio.setBlocking(false);
    presentation->syncUi();
  }

  void released() {
    emulator->setOption("hack/ppu/frameSkip", 0);
    config->system.speed = 2;
    if(audio.driver() != "XAudio 2.1") program->updateAudioDriver();
    video.setBlocking(config->video.synchronize);
    audio.setBlocking(config->audio.synchronize);
    presentation->syncUi();
  }

  Speedup() : HotkeyInput("Speedup", "1/Button/Tilde") {
    userInterfaceEmulationSpeed.attach(this);
  }
} speedup;

struct Decrease : HotkeyInput {
  void pressed() {
    if(config->system.speed > 0) config->system.speed--;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  Decrease() : HotkeyInput("Decrease", "Control+1/Button/Divide") {
    userInterfaceEmulationSpeed.attach(this);
  }
} decrease;

struct Increase : HotkeyInput {
  void pressed() {
    if(config->system.speed < 4) config->system.speed++;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  Increase() : HotkeyInput("Increase", "Control+1/Button/Multiply") {
    userInterfaceEmulationSpeed.attach(this);
  }
} increase;

struct SetSlowestSpeed : HotkeyInput {
  void pressed() {
    config->system.speed = 0;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  SetSlowestSpeed() : HotkeyInput("Set Slowest Speed", "Control+1/Button/Num1") {
    userInterfaceEmulationSpeed.attach(this);
  }
} setSlowestSpeed;

struct SetSlowSpeed : HotkeyInput {
  void pressed() {
    config->system.speed = 1;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  SetSlowSpeed() : HotkeyInput("Set Slow Speed", "Control+1/Button/Num2") {
    userInterfaceEmulationSpeed.attach(this);
  }
} setSlowSpeed;

struct SetNormalSpeed : HotkeyInput {
  void pressed() {
    config->system.speed = 2;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  SetNormalSpeed() : HotkeyInput("Set Normal Speed", "Control+1/Button/Num3") {
    userInterfaceEmulationSpeed.attach(this);
  }
} setNormalSpeed;

struct SetFastSpeed : HotkeyInput {
  void pressed() {
    config->system.speed = 3;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  SetFastSpeed() : HotkeyInput("Set Fast Speed", "Control+1/Button/Num4") {
    userInterfaceEmulationSpeed.attach(this);
  }
} setFastSpeed;

struct SetFastestSpeed : HotkeyInput {
  void pressed() {
    config->system.speed = 4;
    program->updateAudioDriver();
    presentation->syncUi();
  }

  SetFastestSpeed() : HotkeyInput("Set Fastest Speed", "Control+1/Button/Num5") {
    userInterfaceEmulationSpeed.attach(this);
  }
} setFastestSpeed;

struct SynchronizeVideo : HotkeyInput {
  void pressed() {
    windowManager.toggleSynchronizeVideo();
  }

  SynchronizeVideo() : HotkeyInput("Synchronize Video", "Control+1/Button/V") {
    userInterfaceEmulationSpeed.attach(this);
  }
} synchronizeVideo;

struct SynchronizeAudio : HotkeyInput {
  void pressed() {
    windowManager.toggleSynchronizeAudio();
  }

  SynchronizeAudio() : HotkeyInput("Synchronize Audio", "Control+1/Button/A") {
    userInterfaceEmulationSpeed.attach(this);
  }
} synchronizeAudio;

}
