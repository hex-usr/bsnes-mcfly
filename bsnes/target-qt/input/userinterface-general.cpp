InputGroup userInterfaceGeneral(InputCategory::UserInterface, "General");

namespace UserInterfaceGeneral {

struct ToggleMenubar : HotkeyInput {
  void pressed() {
    windowManager.toggleMenubar();
  }

  ToggleMenubar() : HotkeyInput("Toggle Menubar", "1/Button/Tab") {
    userInterfaceGeneral.attach(this);
  }
} toggleMenubar;

struct ToggleStatusbar : HotkeyInput {
  void pressed() {
    windowManager.toggleStatusbar();
  }

  ToggleStatusbar() : HotkeyInput("Toggle Statusbar", "1/Button/Tab") {
    userInterfaceGeneral.attach(this);
  }
} toggleStatusbar;

struct ToggleCheatSystem : HotkeyInput {
  void pressed() {
    //there is a signal attached to cheatEnableBox that will update SFC::cheat.enable(bool);
    if(!toolsWindow->cheatEditor->cheatEnableBox->isChecked()) {
      toolsWindow->cheatEditor->cheatEnableBox->setChecked(true);
      program->showMessage("Cheat system enabled.");
    } else {
      toolsWindow->cheatEditor->cheatEnableBox->setChecked(false);
      program->showMessage("Cheat system disabled.");
    }
  }

  ToggleCheatSystem() : HotkeyInput("Toggle Cheat System") {
    userInterfaceGeneral.attach(this);
  }
} toggleCheatSystem;

struct CaptureScreenshot : HotkeyInput {
  void pressed() {
    //tell SFC::Interface to save a screenshot at the next video_refresh() event
    program->saveScreenshot = true;
  }

  CaptureScreenshot() : HotkeyInput("Capture Screenshot") {
    userInterfaceGeneral.attach(this);
  }
} captureScreenshot;

//put here instead of in a separate "Audio Settings" group,
//because there is only one audio option at present
struct MuteAudioOutput : HotkeyInput {
  void pressed() {
    presentation->settings_muteAudio->toggleChecked();
    config->audio.mute = presentation->settings_muteAudio->isChecked();
    program->updateAudioEffects();
  }

  MuteAudioOutput() : HotkeyInput("Mute Audio Output", "Shift+1/Button/M") {
    userInterfaceGeneral.attach(this);
  }
} muteAudioOutput;

struct Exit : HotkeyInput {
  void pressed() {
    presentation->quit();
  }

  Exit() : HotkeyInput("Exit") {
    userInterfaceGeneral.attach(this);
  }
} exit;

}
