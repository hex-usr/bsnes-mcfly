namespace Controllers {

void TurboInput::cache() {
  if(state) {
    cachedState = (counter < holdHi ? state : 0);
    if(++counter >= holdHi + holdLo) counter = 0;
  } else {
    cachedState = 0;
    counter = 0;
  }
}

TurboInput::TurboInput(const char* name, const char* mapping) : DigitalInput(name, mapping) {
  holdHi = 2;
  holdLo = 2;
  counter = 0;
}

int16_t Gamepad::status(uint id) const {
  if(config->input.allowInvalidInput == false) {
    //block up+down and left+right combinations:
    //a real gamepad has a pivot in the D-pad that makes this impossible;
    //some software titles will crash if up+down or left+right are detected
    if(id == SFC::Gamepad::Down && up.cachedState) return 0;
    if(id == SFC::Gamepad::Right && left.cachedState) return 0;
  }

  switch(id) {
  case SFC::Gamepad::Up: return up.cachedState;
  case SFC::Gamepad::Down: return down.cachedState;
  case SFC::Gamepad::Left: return left.cachedState;
  case SFC::Gamepad::Right: return right.cachedState;
  case SFC::Gamepad::B: return b.cachedState | turboB.cachedState;
  case SFC::Gamepad::A: return a.cachedState | turboA.cachedState;
  case SFC::Gamepad::Y: return y.cachedState | turboY.cachedState;
  case SFC::Gamepad::X: return x.cachedState | turboX.cachedState;
  case SFC::Gamepad::L: return l.cachedState | turboL.cachedState;
  case SFC::Gamepad::R: return r.cachedState | turboR.cachedState;
  case SFC::Gamepad::Select: return select.cachedState;
  case SFC::Gamepad::Start: return start.cachedState;
  }
  return 0;
}

Gamepad::Gamepad(uint category, const char* name) :
InputGroup(category, name),
up("Up"),
down("Down"),
left("Left"),
right("Right"),
b("B"),
a("A"),
y("Y"),
x("X"),
l("L"),
r("R"),
select("Select"),
start("Start"),
turboB("Turbo B"),
turboA("Turbo A"),
turboY("Turbo Y"),
turboX("Turbo X"),
turboL("Turbo L"),
turboR("Turbo R") {
  attach(&up); attach(&down); attach(&left); attach(&right);
  attach(&b); attach(&a); attach(&y); attach(&x);
  attach(&l); attach(&r); attach(&select); attach(&start);
  attach(&turboB); attach(&turboA); attach(&turboY); attach(&turboX);
  attach(&turboL); attach(&turboR);

  if(this == &gamepad1) {
    up.defaultMapping = up.mapping = "1/Button/Up";
    down.defaultMapping = down.mapping = "1/Button/Down";
    left.defaultMapping = left.mapping = "1/Button/Left";
    right.defaultMapping = right.mapping = "1/Button/Right";
    b.defaultMapping = b.mapping = "1/Button/Z";
    a.defaultMapping = a.mapping = "1/Button/X";
    y.defaultMapping = y.mapping = "1/Button/A";
    x.defaultMapping = x.mapping = "1/Button/S";
    l.defaultMapping = l.mapping = "1/Button/D";
    r.defaultMapping = r.mapping = "1/Button/C";
    select.defaultMapping = select.mapping = "1/Button/Apostrophe";
    start.defaultMapping = start.mapping = "1/Button/Return";
  }
}

//

int16_t Multitap::status(uint id) const {
  uint index = id / 12;
  id %= 12;
  switch(index & 3) { default:
    case 0: return port1.status(id);
    case 1: return port2.status(id);
    case 2: return port3.status(id);
    case 3: return port4.status(id);
  }
}

Multitap::Multitap(Gamepad& port1_, Gamepad& port2_, Gamepad& port3_, Gamepad& port4_) :
InputGroup(InputCategory::Hidden, "Multitap"),
port1(port1_), port2(port2_), port3(port3_), port4(port4_) {
}

//

void AsciiSwitch::poll() {
  DigitalInput::poll();

  //only change state when controller is active
  if(!parent) return;
  if(parent->category == InputCategory::Port1 && inputManager->port1 != parent) return;
  if(parent->category == InputCategory::Port2 && inputManager->port2 != parent) return;

  if(previousState != state && state) {
    switch(mode) {
      case Off: mode = Turbo; program->showMessage({name, " set to turbo."}); break;
      case Turbo: mode = Auto; program->showMessage({name, " set to auto."}); break;
      case Auto: mode = Off; program->showMessage({name, " set to off."}); break;
    }
  }
}

AsciiSwitch::AsciiSwitch(const char* name, const char* mapping) : DigitalInput(name, mapping) {
  mode = Off;
}

void AsciiInput::cache() {
  if(asciiSwitch->mode == AsciiSwitch::Off) {
    cachedState = state;
  } else if(asciiSwitch->mode == AsciiSwitch::Turbo) {
    if(state) {
      cachedState = (counter < holdHi ? state : 0);
      if(++counter >= holdHi + holdLo) counter = 0;
    } else {
      cachedState = 0;
      counter = 0;
    }
  } else if(asciiSwitch->mode == AsciiSwitch::Auto) {
    cachedState = (counter < holdHi);
    if(++counter >= holdHi + holdLo) counter = 0;
  }
}

AsciiInput::AsciiInput(const char* name, const char* mapping) : DigitalInput(name, mapping) {
  holdHi = 2;
  holdLo = 2;
  counter = 0;
}

void AsciiSlowMotion::poll() {
  DigitalInput::poll();

  //only change state when controller is active
  if(!parent) return;
  if(parent->category == InputCategory::Port1 && inputManager->port1 != parent) return;
  if(parent->category == InputCategory::Port2 && inputManager->port2 != parent) return;

  if(previousState != state && state) {
    if(enabled == false) {
      enabled = true;
      program->showMessage({name, " enabled."});
    } else {
      enabled = false;
      program->showMessage({name, " disabled."});
    }
  }
}

void AsciiSlowMotion::cache() {
  if(enabled == false) {
    cachedState = 0;
  } else {
    cachedState = counter < holdHi;
    if(++counter >= holdHi + holdLo) counter = 0;
  }
}

AsciiSlowMotion::AsciiSlowMotion(const char* name, const char* mapping) :
DigitalInput(name, mapping) {
  enabled = false;
  holdHi = 2;
  holdLo = 2;
}

int16_t Asciipad::status(uint id) const {
  if(config->input.allowInvalidInput == false) {
    if(id == SFC::Gamepad::Down && up.cachedState) return 0;
    if(id == SFC::Gamepad::Right && left.cachedState) return 0;
  }

  switch(id) {
  case SFC::Gamepad::Up: return up.cachedState;
  case SFC::Gamepad::Down: return down.cachedState;
  case SFC::Gamepad::Left: return left.cachedState;
  case SFC::Gamepad::Right: return right.cachedState;
  case SFC::Gamepad::B: return b.cachedState;
  case SFC::Gamepad::A: return a.cachedState;
  case SFC::Gamepad::Y: return y.cachedState;
  case SFC::Gamepad::X: return x.cachedState;
  case SFC::Gamepad::L: return l.cachedState;
  case SFC::Gamepad::R: return r.cachedState;
  case SFC::Gamepad::Select: return select.cachedState;
  case SFC::Gamepad::Start: return start.cachedState | slowMotion.cachedState;
  }
  return 0;
}

Asciipad::Asciipad(uint category, const char* name) :
InputGroup(category, name),
up("Up"),
down("Down"),
left("Left"),
right("Right"),
b("B"),
a("A"),
y("Y"),
x("X"),
l("L"),
r("R"),
select("Select"),
start("Start"),
switchB("B Switch"),
switchA("A Switch"),
switchY("Y Switch"),
switchX("X Switch"),
switchL("L Switch"),
switchR("R Switch"),
slowMotion("Slow Motion") {
  b.asciiSwitch = &switchB;
  a.asciiSwitch = &switchA;
  y.asciiSwitch = &switchY;
  x.asciiSwitch = &switchX;
  l.asciiSwitch = &switchL;
  r.asciiSwitch = &switchR;

  attach(&up); attach(&down); attach(&left); attach(&right);
  attach(&b); attach(&a); attach(&y); attach(&x);
  attach(&l); attach(&r); attach(&select); attach(&start);
  attach(&switchB); attach(&switchA); attach(&switchY); attach(&switchX);
  attach(&switchL); attach(&switchR); attach(&slowMotion);

  if(this == &asciipad1) {
    up.defaultMapping = up.mapping = "1/Button/Up";
    down.defaultMapping = down.mapping = "1/Button/Down";
    left.defaultMapping = left.mapping = "1/Button/Left";
    right.defaultMapping = right.mapping = "1/Button/Right";
    b.defaultMapping = b.mapping = "1/Button/Z";
    a.defaultMapping = a.mapping = "1/Button/X";
    y.defaultMapping = y.mapping = "1/Button/A";
    x.defaultMapping = x.mapping = "1/Button/S";
    l.defaultMapping = l.mapping = "1/Button/D";
    r.defaultMapping = r.mapping = "1/Button/C";
    select.defaultMapping = select.mapping = "1/Button/Apostrophe";
    start.defaultMapping = start.mapping = "1/Button/Return";
  }
}

//

int16_t Mouse::status(uint id) const {
  switch(id) {
  case SFC::Mouse::X: return x.cachedState;
  case SFC::Mouse::Y: return y.cachedState;
  case SFC::Mouse::Left: return left.cachedState;
  case SFC::Mouse::Right: return right.cachedState;
  }
  return 0;
}

Mouse::Mouse(uint category, const char* name) :
InputGroup(category, name),
x("X-axis", "2/Axis/X"),
y("Y-axis", "2/Axis/Y"),
left("Left Button", "2/Button/Left"),
right("Right Button", "2/Button/Right") {
  attach(&x); attach(&y); attach(&left); attach(&right);
}

//

int16_t SuperScope::status(uint id) const {
  switch(id) {
  case SFC::SuperScope::X: return x.cachedState;
  case SFC::SuperScope::Y: return y.cachedState;
  case SFC::SuperScope::Trigger: return trigger.cachedState;
  case SFC::SuperScope::Cursor: return cursor.cachedState;
  case SFC::SuperScope::Turbo: return turbo.cachedState;
  case SFC::SuperScope::Pause: return pause.cachedState;
  }
  return 0;
}

SuperScope::SuperScope(uint category, const char* name) :
InputGroup(category, name),
x("X-axis", "2/Axis/X"),
y("Y-axis", "2/Axis/Y"),
trigger("Trigger", "2/Button/Left"),
cursor("Cursor", "2/Button/Right"),
turbo("Turbo", "1/Button/T"),
pause("Pause", "1/Button/P") {
  attach(&x); attach(&y); attach(&trigger); attach(&cursor);
  attach(&turbo); attach(&pause);
}

//

int16_t Justifier::status(uint id) const {
  switch(id) {
  case SFC::Justifier::X: return x.cachedState;
  case SFC::Justifier::Y: return y.cachedState;
  case SFC::Justifier::Trigger: return trigger.cachedState;
  case SFC::Justifier::Start: return start.cachedState;
  }
  return 0;
}

Justifier::Justifier(uint category, const char* name) :
InputGroup(category, name),
x("X-axis"),
y("Y-axis"),
trigger("Trigger"),
start("Start") {
  attach(&x); attach(&y); attach(&trigger); attach(&start);

  if(this == &justifier1) {
    x.defaultMapping = x.mapping = "2/Axis/X";
    y.defaultMapping = y.mapping = "2/Axis/Y";
    trigger.defaultMapping = trigger.mapping = "2/Button/Left";
    start.defaultMapping = start.mapping = "2/Button/Right";
  }
}

//

int16_t Justifiers::status(uint id) const {
  uint index = id / 4;
  id %= 4;
  switch(index) { default:
    case 0: return port1.status(id);
    case 1: return port2.status(id);
  }
}

Justifiers::Justifiers(Justifier& port1_, Justifier& port2_) :
InputGroup(InputCategory::Hidden, "Justifiers"),
port1(port1_), port2(port2_) {
}

//

Gamepad gamepad1(InputCategory::Port1, "Gamepad");
Asciipad asciipad1(InputCategory::Port1, "asciiPad");
Gamepad multitap1a(InputCategory::Port1, "Multitap - Port 1");
Gamepad multitap1b(InputCategory::Port1, "Multitap - Port 6");
Gamepad multitap1c(InputCategory::Port1, "Multitap - Port 7");
Gamepad multitap1d(InputCategory::Port1, "Multitap - Port 8");
Multitap multitap1(multitap1a, multitap1b, multitap1c, multitap1d);
Mouse mouse1(InputCategory::Port1, "Mouse");

Gamepad gamepad2(InputCategory::Port2, "Gamepad");
Asciipad asciipad2(InputCategory::Port2, "asciiPad");
Gamepad multitap2a(InputCategory::Port2, "Multitap - Port 2");
Gamepad multitap2b(InputCategory::Port2, "Multitap - Port 3");
Gamepad multitap2c(InputCategory::Port2, "Multitap - Port 4");
Gamepad multitap2d(InputCategory::Port2, "Multitap - Port 5");
Multitap multitap2(multitap2a, multitap2b, multitap2c, multitap2d);
Mouse mouse2(InputCategory::Port2, "Mouse");
SuperScope superscope(InputCategory::Port2, "Super Scope");
Justifier justifier1(InputCategory::Port2, "Justifier 1");
Justifier justifier2(InputCategory::Port2, "Justifier 2");
Justifiers justifiers(justifier1, justifier2);

}
