struct Movie {
  enum State { Inactive, Playback, Record } state;

  void chooseFile();
  void play(const string& filename);
  void record();
  void stop();

  int16_t read();
  void write(int16_t value);

  Movie();

private:
  file_buffer fp;
};

extern Movie movie;
