#include "../qt.hpp"
ConfigurationSettings* config = nullptr;

ConfigurationSettings::ConfigurationSettings() {
  //external
  sfc.append(sfc.region = "Auto", "Region", "Valid: Auto, NTSC, PAL");

//sfc.cpu.append(SFC::config.cpu.ntsc_frequency  = 315.0 / 88.0 * 6000000.0, "NTSCFrequency");
//sfc.cpu.append(SFC::config.cpu.pal_frequency   = 21281370, "PALFrequency");
//sfc.append(sfc.cpu, "CPU");

//sfc.smp.append(SFC::config.smp.ntsc_frequency = 32040.5 * 768, "NTSCFrequency");
//sfc.smp.append(SFC::config.smp.pal_frequency  = 32040.5 * 768, "PALFrequency");
//sfc.append(sfc.smp, "SMP");

  sfc.superfx.append(sfc.superfx.speed = 100, "Speed", "Valid range: 100 to 800");
  sfc.append(sfc.superfx, "SuperFX");

  append(sfc, "SFC");

  //internal
  for(uint n : range(10)) game.recent_.append(game.recent[n], string{n + 1});
  game.append(game.recent_, "Recent");
  append(game, "Game");

  system.append(system.profile = "", "Profile", "Valid: accuracy, compatibility, performance");
  system.append(system.crashedOnLastRun = false, "CrashedOnLastRun");
  system.append(system.speed = 2, "Speed");
  system.append(system.speeds[0] =  50, "SpeedSlowest");
  system.append(system.speeds[1] =  75, "SpeedSlow");
  system.append(system.speeds[2] = 100, "SpeedNormal");
  system.append(system.speeds[3] = 150, "SpeedFast");
  system.append(system.speeds[4] = 200, "SpeedFastest");
  system.append(system.autoSaveMemory = false, "AutoSaveMemory", "Automatically save cartridge back-up RAM once every minute");
  system.append(system.rewindEnabled = false, "RewindEnabled", "Automatically save states periodically to allow auto-rewind support");
  system.append(system.cheatEnabled = true, "CheatEnabled");
  system.append(system.stateManagerDoubleClick = false, "StateManagerDoubleClick");
  append(system, "System");

  diskBrowser.append(diskBrowser.useCommonDialogs = false, "UseCommonDialogs");
  diskBrowser.append(diskBrowser.showPanel = true, "ShowPanel");
  append(diskBrowser, "DiskBrowser");

  file.append(file.applyPatches = true, "ApplyPatches");
  append(file, "File");

  path.bios.append(path.bios.bsx = "", "BSX");
  path.bios.append(path.bios.sufamiTurbo = "", "SufamiTurbo");
  path.bios.append(path.bios.superGameBoy = "", "SuperGameBoy");
  path.append(path.bios, "BIOS");
  path.append(path.rom   = "", "ROM");
  path.append(path.save  = "", "Save");
  path.append(path.state = "", "State");
  path.append(path.patch = "", "Patch");
  path.append(path.cheat = "", "Cheat");
  path.append(path.data  = "", "Data");
  path.current.append(path.current.folder    = "", "Folder");
  path.current.append(path.current.shader    = "", "Shader");
  path.current.append(path.current.cartridge = "", "Cartridge");
  path.current.append(path.current.filter    = 0,  "Filter");
  path.append(path.current, "Current");
  append(path, "Path");

  video.context = &video.windowed;
  video.append(video.driver = ruby::Video::safestDriver(), "Driver");
  video.append(video.synchronize = false, "Synchronize");
  video.append(video.format = "RGB24", "Format");
  video.append(video.filter = "None", "Filter");
  video.append(video.shader = "", "Shader");
  video.append(video.ntscAspectRatio =       8.0 /       7.0, "NTSCAspectRatio", "NTSC aspect ratio (x / y)");
  video.append(video.palAspectRatio  = 2950000.0 / 2128137.0, "PALAspectRatio",  "PAL aspect ratio (x / y)");
  video.append(video.cropLeft   = 0, "CropLeft");
  video.append(video.cropTop    = 0, "CropTop");
  video.append(video.cropRight  = 0, "CropRight");
  video.append(video.cropBottom = 0, "CropBottom");
  video.append(video.saturation     =   100, "Saturation");
  video.append(video.gamma          =   100, "Gamma");
  video.append(video.luminance      =   100, "Luminance");
  video.append(video.scanline       =   100, "Scanline");
  video.append(video.autoHideFullscreenMenu = false, "AutoHideFullscreenMenu");
  video.append(video.fullscreenExclusive    = false, "FullscreenExclusive");
  video.windowed.append(video.windowed.correctAspectRatio = true, "CorrectAspectRatio");
  video.windowed.append(video.windowed.multiplier         =    2, "Multiplier");
  video.windowed.append(video.windowed.region             =    0, "Region");
  video.windowed.append(video.windowed.blur               =    1, "Blur");
  video.append(video.windowed, "Windowed");
  video.fullscreen.append(video.fullscreen.correctAspectRatio = true, "CorrectAspectRatio");
  video.fullscreen.append(video.fullscreen.multiplier         =    9, "Multiplier");
  video.fullscreen.append(video.fullscreen.region             =    0, "Region");
  video.fullscreen.append(video.fullscreen.blur               =    1, "Blur");
  video.append(video.fullscreen, "Fullscreen");
  append(video, "Video");

  audio.append(audio.driver = ruby::Audio::safestDriver(), "Driver");
  audio.append(audio.device = "", "Device");
  audio.append(audio.frequency = 48000, "Frequency");
  audio.append(audio.latency = 60, "Latency");
  audio.append(audio.exclusive = false, "Exclusive");
  audio.append(audio.synchronize = true, "Synchronize");
  audio.append(audio.mute = false, "Mute");
  audio.append(audio.volume = 100, "Volume");
  append(audio, "Audio");

  input.append(input.driver = ruby::Input::safestDriver(), "Driver");
  input.append(input.focusPolicy = Input::FocusPolicyIgnoreInput, "FocusPolicy");
  input.append(input.port1 = ControllerPort1::Gamepad, "Port1");
  input.append(input.port2 = ControllerPort2::Gamepad, "Port2");
  input.append(input.allowInvalidInput = false, "AllowInvalidInput", "Allow up+down / left+right combinations; may trigger bugs in some games");
  input.append(input.modifierEnable = true, "ModifierEnable");
  append(input, "Input");

  geometry.append(geometry.presentation      = "", "Presentation");
  geometry.append(geometry.loaderWindow      = "", "LoaderWindow");
  geometry.append(geometry.stateSelectWindow = "", "StateSelectWindow");
  geometry.append(geometry.htmlViewerWindow  = "", "HTMLViewerWindow");
  geometry.append(geometry.aboutWindow       = "", "AboutWindow");
  geometry.append(geometry.fileBrowser       = "", "FileBrowser");
  geometry.append(geometry.folderCreator     = "", "FolderCreator");
  geometry.append(geometry.settingsWindow    = "", "SettingsWindow");
  geometry.append(geometry.toolsWindow       = "", "ToolsWindow");
  geometry.append(geometry.cheatDatabase     = "", "CheatDatabase");
  append(geometry, "Geometry");

  load();
}

void ConfigurationSettings::load() {
  Configuration::Document::load(locate("settings-qt.bml"));
//SFC::config.superfx.speed = max(0, min(2, SFC::config.superfx.speed));
  video.context = !windowManager.isFullscreen ? &video.windowed : &video.fullscreen;
  save();  //creates file if it does not exist
}

void ConfigurationSettings::save() {
  Configuration::Document::save(locate("settings-qt.bml"));
}
