EffectToggle::EffectToggle() {
  layout = new QGridLayout;
  layout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  ppuLabel = new QLabel("<b>S-PPU (Video)</b>");
  layout->addWidget(ppuLabel, 0, 0, 1, 4);

  ppuInfo = new QLabel("These toggles will not work with the accuracy profile.");
  layout->addWidget(ppuInfo, 1, 0, 1, 4);

  for(auto bgID : range(4)) {
    for(auto priority : range(2)) {
      bgEnabled[bgID][priority] = new QCheckBox(QString::fromUtf8(string{"BG", bgID + 1, " Priority ", priority}));
      bgEnabled[bgID][priority]->setChecked(true);
      connect(bgEnabled[bgID][priority], &QCheckBox::stateChanged, this, [=]() {
        string name = {"hack/ppu/bgDisable", bgID, priority == 0 ? "lo" : "hi"};
        emulator->setOption(name, !bgEnabled[bgID][priority]->isChecked());
      });
      layout->addWidget(bgEnabled[bgID][priority], 2 + bgID, priority);
    }
  }

  for(auto priority : range(4)) {
    objEnabled[priority] = new QCheckBox(QString::fromUtf8(string{"OBJ Priority ", priority}));
    objEnabled[priority]->setChecked(true);
    connect(objEnabled[priority], &QCheckBox::stateChanged, this, [=]() {
      string name = {"hack/ppu/objDisable", priority};
      emulator->setOption(name, !objEnabled[priority]->isChecked());
    });
    layout->addWidget(objEnabled[priority], 6, priority);
  }

  dspLabel = new QLabel("<b>S-DSP (Audio)</b>");
  layout->addWidget(dspLabel, 7, 0, 1, 4);

  for(auto channel : range(8)) {
    channelEnabled[channel] = new QCheckBox(QString::fromUtf8(string{"Channel ", channel}));
    channelEnabled[channel]->setChecked(true);
    connect(channelEnabled[channel], &QCheckBox::stateChanged, this, [=]() {
      string name = {"hack/dsp/channelMute", channel};
      emulator->setOption(name, !channelEnabled[channel]->isChecked());
    });
    layout->addWidget(channelEnabled[channel], 8 + channel / 4, channel % 4);
  }
}
