class CheatDatabase : public Window {
public:
  QVBoxLayout* layout;
    QLabel* title;
    QTreeWidget* cheatList;
    QHBoxLayout* controlLayout;
      QPushButton* selectAllButton;
      QPushButton* clearAllButton;
      QWidget* spacer;
      QPushButton* okButton;
      QPushButton* cancelButton;

  void findCodes();
  CheatDatabase();

  void selectAllCodes();
  void clearAllCodes();
  void addCodes();

private:
  vector<string> cheatCode;
};

class CheatEditor : public QWidget {
public:
  QVBoxLayout* layout;
    QTreeWidget* cheatList;
    QGridLayout* gridLayout;
      QLabel* codeLabel;
      QLineEdit* codeEdit;
      QLabel* descLabel;
      QLineEdit* descEdit;
    QHBoxLayout* controlLayout;
      QCheckBox* cheatEnableBox;
      QWidget* spacer;
      QPushButton* findButton;
      QPushButton* clearButton;

  auto reset() -> void;
  auto loadCheats() -> void;
  auto saveCheats() -> void;
  auto addCode(const string& code, const string& description) -> bool;

  CheatEditor();

  void synchronize();
  void updateUI();
  void updateInterface();
  void updateCode();
  void updateDesc();
  void findCheatCodes();
  void clearSelected();

private:
  enum : uint { Code, Desc };
  string cheatText[128][2];
  bool lock;
  friend class CheatDatabase;

  maybe<string> decode(string code);
  maybe<string> decodeGB(string code);
};

class CheatFinder : public QWidget {
public:
  QVBoxLayout* layout;
    QTreeWidget* list;
    QGridLayout* controlLayout;
      QLabel* sizeLabel;
      QButtonGroup* sizeGroup;
      QRadioButton* size8bit;
      QRadioButton* size16bit;
      QRadioButton* size24bit;
      QRadioButton* size32bit;
      QLabel* compareLabel;
      QButtonGroup* compareGroup;
      QRadioButton* compareEqual;
      QRadioButton* compareNotEqual;
      QRadioButton* compareLessThan;
      QRadioButton* compareGreaterThan;
      QLabel* valueLabel;
      QHBoxLayout* actionLayout;
        QLineEdit* valueEdit;
        QPushButton* searchButton;
        QPushButton* resetButton;

  void synchronize();
  void refreshList();
  CheatFinder();

  void searchMemory();
  void resetSearch();

private:
  vector<uint> addrList;
  vector<uint> dataList;

  uint read(uint addr, uint size);
};

class StateManager : public QWidget {
public:
  QVBoxLayout* layout;
    QTreeWidget* list;
    QHBoxLayout* infoLayout;
      QLabel* descriptionLabel;
      QLineEdit* descriptionText;
    QHBoxLayout* controlLayout;
      QCheckBox* doubleClick;
      QWidget* spacer;
      QPushButton* loadButton;
      QPushButton* saveButton;
      QPushButton* eraseButton;

  auto reload() -> void;
  auto update() -> void;

  StateManager();

  void doRefresh();
  void doChangeDescription();
  void doLoad();
  void doSave();
  void doErase();

private:
  enum : uint { Slots = 32 };
};

class ManifestViewer : public QWidget {
public:
	ManifestViewer();
	
	void doRefresh();

private:
	QVBoxLayout* layout;
	  QPlainTextEdit* manifestView;
};

class EffectToggle : public QWidget {
public:
  QGridLayout* layout;
    QLabel* ppuLabel;
    QLabel* ppuInfo;
    QCheckBox* bgEnabled[4][2];
    QCheckBox* objEnabled[4];
    QLabel* dspLabel;
    QCheckBox* channelEnabled[8];

  EffectToggle();
};

class ToolsWindow : public Window {
public:
  ToolsWindow();
  ~ToolsWindow();

  QVBoxLayout* layout;
    QTabWidget* tab;
      QScrollArea* cheatEditorArea;
        CheatEditor* cheatEditor;
      QScrollArea* cheatFinderArea;
        CheatFinder* cheatFinder;
      QScrollArea* stateManagerArea;
        StateManager* stateManager;
      QScrollArea* manifestViewerArea;
        ManifestViewer* manifestViewer;
      QScrollArea* effectToggleArea;
        EffectToggle* effectToggle;
};

extern unique_pointer<CheatDatabase> cheatDatabase;
extern unique_pointer<ToolsWindow> toolsWindow;
