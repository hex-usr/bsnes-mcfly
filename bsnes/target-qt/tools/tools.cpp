#include "../qt.hpp"

unique_pointer<ToolsWindow> toolsWindow;

#include "cheat-editor.cpp"
#include "cheat-finder.cpp"
#include "state-manager.cpp"
#include "manifest-viewer.cpp"
#include "effect-toggle.cpp"

ToolsWindow::ToolsWindow() {
  setObjectName("tools-window");
  setWindowTitle("Tools");
  resize(600, 360);
  setGeometryString(&config->geometry.toolsWindow);

  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  cheatEditor = new CheatEditor;
  cheatEditorArea = new QScrollArea;
  cheatEditorArea->setWidget(cheatEditor);
  cheatEditorArea->setFrameStyle(0);
  cheatEditorArea->setWidgetResizable(true);

  cheatFinder = new CheatFinder;
  cheatFinderArea = new QScrollArea;
  cheatFinderArea->setWidget(cheatFinder);
  cheatFinderArea->setFrameStyle(0);
  cheatFinderArea->setWidgetResizable(true);

  stateManager = new StateManager;
  stateManagerArea = new QScrollArea;
  stateManagerArea->setWidget(stateManager);
  stateManagerArea->setFrameStyle(0);
  stateManagerArea->setWidgetResizable(true);

  manifestViewer = new ManifestViewer;
  manifestViewerArea = new QScrollArea;
  manifestViewerArea->setWidget(manifestViewer);
  manifestViewerArea->setFrameStyle(0);
  manifestViewerArea->setWidgetResizable(true);

  effectToggle = new EffectToggle;
  effectToggleArea = new QScrollArea;
  effectToggleArea->setWidget(effectToggle);
  effectToggleArea->setFrameStyle(0);
  effectToggleArea->setWidgetResizable(true);

  tab = new QTabWidget;
  tab->addTab(cheatEditorArea, "Cheat Editor");
  tab->addTab(cheatFinderArea, "Cheat Finder");
  tab->addTab(stateManagerArea, "State Manager");
  tab->addTab(manifestViewerArea, "Manifest Viewer");
  tab->addTab(effectToggleArea, "Effect Toggle");
  layout->addWidget(tab);
}

ToolsWindow::~ToolsWindow() {
  tab->clear();
  delete cheatEditor;
  delete cheatFinder;
  delete stateManager;
  delete manifestViewer;
  delete effectToggle;
  delete cheatEditorArea;
  delete cheatFinderArea;
  delete stateManagerArea;
  delete manifestViewerArea;
  delete effectToggleArea;
  delete tab;
  delete layout;
}
