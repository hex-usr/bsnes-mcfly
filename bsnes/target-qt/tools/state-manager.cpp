StateManager::StateManager() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  list = new QTreeWidget;
  list->setColumnCount(2);
  list->setHeaderLabels(QStringList() << "Slot" << "Description");
  list->setAllColumnsShowFocus(true);
  list->sortByColumn(0, Qt::AscendingOrder);
  list->setRootIsDecorated(false);
  list->resizeColumnToContents(0);
  connect(list, &QTreeWidget::itemSelectionChanged, [this]() { doRefresh(); });
  connect(list, &QTreeWidget::itemActivated, [this](QTreeWidgetItem*, int) {
    if(config->system.stateManagerDoubleClick) doLoad();
  });
  layout->addWidget(list);

  infoLayout = new QHBoxLayout;
  layout->addLayout(infoLayout);

  descriptionLabel = new QLabel("Description:");
  infoLayout->addWidget(descriptionLabel);

  descriptionText = new QLineEdit;
  connect(descriptionText, &QLineEdit::textEdited, [this](const QString&) { doChangeDescription(); });
  infoLayout->addWidget(descriptionText);

  controlLayout = new QHBoxLayout;
  layout->addLayout(controlLayout);

  doubleClick = new QCheckBox("Double-click to load");
  connect(doubleClick, &QCheckBox::stateChanged, [this](int) {
    config->system.stateManagerDoubleClick = doubleClick->isChecked();
  });
  controlLayout->addWidget(doubleClick);

  spacer = new QWidget;
  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  controlLayout->addWidget(spacer);

  loadButton = new QPushButton("Load");
  connect(loadButton, &QPushButton::released, [this]() { doLoad(); });
  controlLayout->addWidget(loadButton);

  saveButton = new QPushButton("Save");
  connect(saveButton, &QPushButton::released, [this]() { doSave(); });
  controlLayout->addWidget(saveButton);

  eraseButton = new QPushButton("Erase");
  connect(eraseButton, &QPushButton::released, [this]() { doErase(); });
  controlLayout->addWidget(eraseButton);

  doRefresh();
}

auto StateManager::reload() -> void {
  list->clear();
  list->setSortingEnabled(false);

  if(emulator->loaded()) {
    for(uint n = 0; n < 32; n++) {
      QTreeWidgetItem* item = new QTreeWidgetItem(list);
      item->setData(0, Qt::UserRole, QVariant(n));
      char slot[16];
      sprintf(slot, "%2u", n + 1);
      item->setText(0, slot);
    }
    update();
  }

  list->setSortingEnabled(true);
  list->header()->setSortIndicatorShown(false);
  doRefresh();
}

auto StateManager::update() -> void {
  //iterate all list items
  QList<QTreeWidgetItem*> items = list->findItems("", Qt::MatchContains);
  for(uint i = 0; i < items.count(); i++) {
    QTreeWidgetItem* item = items[i];
    uint offset = item->data(0, Qt::UserRole).toUInt();
    auto buffer = file::read(program->statePath(1 + offset, true));
    if(buffer.size() >= 4 + 16 + 64 + 512) {
      string description;
      description.reserve(512);
      memory::copy<char>(description.get(), buffer.data() + 4 + 16 + 64, 512);
      description.resize(description.length());
      item->setForeground(0, QBrush(QColor(0, 0, 0)));
      item->setForeground(1, QBrush(QColor(0, 0, 0)));
      item->setText(1, QString::fromUtf8(description));
    } else {
      item->setForeground(0, QBrush(QColor(128, 128, 128)));
      item->setForeground(1, QBrush(QColor(128, 128, 128)));
      item->setText(1, "Empty");
    }
  }

  for(uint n = 0; n <= 1; n++) list->resizeColumnToContents(n);
}

void StateManager::doRefresh() {
  vector<uint8_t> buffer;
  QList<QTreeWidgetItem*> items = list->selectedItems();
  if(items.count() > 0) {
    QTreeWidgetItem* item = items[0];
    uint offset = item->data(0, Qt::UserRole).toUInt();
    buffer = file::read(program->statePath(1 + offset, true));
    if(buffer.size() >= 4 + 16 + 64 + 512) {
      string description;
      description.reserve(512);
      memory::copy<char>(description.get(), buffer.data() + 4 + 16 + 64, 512);
      description.resize(description.length());
      descriptionText->setText(QString::fromUtf8(description));
      descriptionText->setEnabled(true);
      loadButton->setEnabled(true);
      eraseButton->setEnabled(true);
    } else {
      descriptionText->setText("");
      descriptionText->setEnabled(false);
      loadButton->setEnabled(false);
      eraseButton->setEnabled(false);
    }
    saveButton->setEnabled(true);
  } else {
    descriptionText->setText("");
    descriptionText->setEnabled(false);
    loadButton->setEnabled(false);
    saveButton->setEnabled(false);
    eraseButton->setEnabled(false);
  }
}

void StateManager::doChangeDescription() {
  QList<QTreeWidgetItem*> items = list->selectedItems();
  if(items.count() > 0) {
    QTreeWidgetItem* item = items[0];
    uint offset = item->data(0, Qt::UserRole).toUInt();
    auto buffer = file::read(program->statePath(1 + offset, true));
    string description = descriptionText->text().toUtf8().constData();
    description.reserve(512);
    memory::copy<uint8_t>(buffer.data() + 4 + 16 + 64, description.data(), 512);
    file::write(program->statePath(1 + offset, true), buffer);
    update();
  }
}

void StateManager::doLoad() {
  QList<QTreeWidgetItem*> items = list->selectedItems();
  if(items.count() > 0) {
    QTreeWidgetItem* item = items[0];
    uint slot = item->data(0, Qt::UserRole).toUInt();
    program->loadState(slot + 1, true);
  }
}

void StateManager::doSave() {
  QList<QTreeWidgetItem*> items = list->selectedItems();
  if(items.count() > 0) {
    QTreeWidgetItem* item = items[0];
    uint slot = item->data(0, Qt::UserRole).toUInt();
    program->saveState(slot + 1, true);
    doChangeDescription();
    doRefresh();
    descriptionText->setFocus();
  }
}

void StateManager::doErase() {
  QList<QTreeWidgetItem*> items = list->selectedItems();
  if(items.count() > 0) {
    QTreeWidgetItem* item = items[0];
    uint slot = item->data(0, Qt::UserRole).toUInt();
    file::remove(program->statePath(slot + 1, true));
    update();
    doRefresh();
  }
}
