CheatFinder::CheatFinder() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  list = new QTreeWidget;
  list->setColumnCount(3);
  list->setHeaderLabels(QStringList() << "Address" << "Current Value" << "Previous Value");
  list->setAllColumnsShowFocus(true);
  list->sortByColumn(0, Qt::AscendingOrder);
  list->setRootIsDecorated(false);
  layout->addWidget(list);

  controlLayout = new QGridLayout;
  controlLayout->setVerticalSpacing(0);
  layout->addLayout(controlLayout);

  sizeLabel = new QLabel("Data size:");
  controlLayout->addWidget(sizeLabel, 0, 0);

  sizeGroup = new QButtonGroup(this);

  size8bit = new QRadioButton("8-bit");
  size8bit->setChecked(true);
  sizeGroup->addButton(size8bit);
  controlLayout->addWidget(size8bit, 0, 1);

  size16bit = new QRadioButton("16-bit");
  sizeGroup->addButton(size16bit);
  controlLayout->addWidget(size16bit, 0, 2);

  size24bit = new QRadioButton("24-bit");
  sizeGroup->addButton(size24bit);
  controlLayout->addWidget(size24bit, 0, 3);

  size32bit = new QRadioButton("32-bit");
  sizeGroup->addButton(size32bit);
  controlLayout->addWidget(size32bit, 0, 4);

  compareLabel = new QLabel("Compare mode:");
  controlLayout->addWidget(compareLabel, 1, 0);

  compareGroup = new QButtonGroup(this);

  compareEqual = new QRadioButton("Equal to");
  compareEqual->setChecked(true);
  compareGroup->addButton(compareEqual);
  controlLayout->addWidget(compareEqual, 1, 1);

  compareNotEqual = new QRadioButton("Not equal to");
  compareGroup->addButton(compareNotEqual);
  controlLayout->addWidget(compareNotEqual, 1, 2);

  compareLessThan = new QRadioButton("Less than");
  compareGroup->addButton(compareLessThan);
  controlLayout->addWidget(compareLessThan, 1, 3);

  compareGreaterThan = new QRadioButton("Greater than");
  compareGroup->addButton(compareGreaterThan);
  controlLayout->addWidget(compareGreaterThan, 1, 4);

  valueLabel = new QLabel("Search value:");
  controlLayout->addWidget(valueLabel, 2, 0);

  actionLayout = new QHBoxLayout;
  actionLayout->setSpacing(Style::WidgetSpacing);
  controlLayout->addLayout(actionLayout, 2, 1, 1, 4);

  valueEdit = new QLineEdit;
  actionLayout->addWidget(valueEdit);

  searchButton = new QPushButton("Search");
  actionLayout->addWidget(searchButton);

  resetButton = new QPushButton("Reset");
  actionLayout->addWidget(resetButton);

  connect(valueEdit, &QLineEdit::returnPressed, this, &CheatFinder::searchMemory);
  connect(searchButton, &QPushButton::released, this, &CheatFinder::searchMemory);
  connect(resetButton, &QPushButton::released, this, &CheatFinder::resetSearch);
  synchronize();
}

void CheatFinder::synchronize() {
  if(!emulator->loaded() || !program->power) {
    list->clear();
    for(uint n = 0; n < 3; n++) list->resizeColumnToContents(n);
    valueEdit->setEnabled(false);
    valueEdit->setText("");
    searchButton->setEnabled(false);
    resetButton->setEnabled(false);
  } else {
    valueEdit->setEnabled(true);
    searchButton->setEnabled(true);
    resetButton->setEnabled(true);
  }
}

void CheatFinder::refreshList() {
  list->clear();
  list->setSortingEnabled(false);

  uint size = 0;
  if(size16bit->isChecked()) size = 1;
  if(size24bit->isChecked()) size = 2;
  if(size32bit->isChecked()) size = 3;

  for(uint i = 0; i < addrList.size() && i < 256; i++) {
    QTreeWidgetItem *item = new QTreeWidgetItem(list);

    uint addr = addrList[i];
    uint data = read(addr, size);
    uint prev = dataList[i];

    char temp[256];

    sprintf(temp, "%.6x", 0x7e0000 + addr);
    item->setText(0, temp);

    sprintf(temp, "%u (0x%x)", data, data);
    item->setText(1, temp);

    sprintf(temp, "%u (0x%x)", prev, prev);
    item->setText(2, temp);
  }

  list->setSortingEnabled(true);
  list->header()->setSortIndicatorShown(false);
  for(uint n = 0; n < 3; n++) list->resizeColumnToContents(n);
}

void CheatFinder::searchMemory() {
  uint size = 0;
  if(size16bit->isChecked()) size = 1;
  if(size24bit->isChecked()) size = 2;
  if(size32bit->isChecked()) size = 3;

  uint data;
  string text = valueEdit->text().toUtf8().constData();

  //auto-detect input data type
  if(text.beginsWith("0x")) data = toHex((const char*)text + 2);
  else if(text.beginsWith("-")) data = toInteger(text);
  else data = toNatural(text);

  if(addrList.size() == 0) {
    //search for the first time: enqueue all possible values so they are all searched
    for(uint addr = 0; addr < 128 * 1024; addr++) {
      addrList.append(addr);
      dataList.append(read(addr, size));
    }
  }

  vector<uint> newAddrList, newDataList, oldDataList;

  for(uint i = 0; i < addrList.size(); i++) {
    uint thisAddr = addrList[i];
    uint thisData = read(thisAddr, size);

    if((compareEqual->isChecked()       && thisData == data)
    || (compareNotEqual->isChecked()    && thisData != data)
    || (compareLessThan->isChecked()    && thisData <  data)
    || (compareGreaterThan->isChecked() && thisData >  data)
    ) {
      newAddrList.append(thisAddr);
      newDataList.append(thisData);
      oldDataList.append(dataList[i]);
    }
  }

  //first refresh the list with the old data values (for the previous value column)
  addrList = newAddrList;
  dataList = oldDataList;
  refreshList();

  //and now update the list with the new data values (for the next search)
  dataList = newDataList;
}

void CheatFinder::resetSearch() {
  addrList.reset();
  dataList.reset();
  refreshList();
}

//size = 0 (8-bit), 1 (16-bit), 2 (24-bit), 3 (32-bit)
uint CheatFinder::read(uint addr, uint size) {
  uint data = 0;

  for(uint n = 0; n <= size; n++) {
    if(addr + n >= 128 * 1024) break;
    data |= SFC::cpu.wram[addr + n] << (n << 3);
  }

  return data;
}
