struct ArchiveReader : public library {
  struct ArchiveFile {
    string name;
    vector<uint8_t> contents;
  };

  ArchiveReader();
  ~ArchiveReader();
  auto support() -> vector<string>;
  auto extract(const string&) -> vector<ArchiveFile>;
  auto reset() -> void;

  function<const char* ()> dl_support;
  function<void (const char*, uint&, char**&, uint8_t**&, size_t*&)> dl_extract;

  uint fileCount = 0;
  char** fileNames = nullptr;
  uint8_t** fileContents = nullptr;
  size_t* fileSizes = nullptr;
};

struct Reader {
  Reader();

  auto load(const string& location, string& type) -> vector<uint8_t>;

  vector<string> sfcExtensionList;
  vector<string> gbExtensionList;
  vector<string> bsExtensionList;
  vector<string> stExtensionList;

  set<string> sharedExtensions;
  vector<string> compressionList;
};

extern unique_pointer<ArchiveReader> archiveReader;
extern Reader reader;
