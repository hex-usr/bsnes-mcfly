struct CPU : Thread, PPUcounter {
  #define WDC65816 CPU
  #include "core/core.hpp"
  #undef WDC65816

  inline auto interruptPending() const -> bool { return false; }
  inline auto pio() const -> uint8 { return io.pio; }
  inline auto synchronizing() const -> bool { return scheduler.synchronizing(); }

  //cpu.cpp
  CPU();
  ~CPU();

  static auto Enter() -> void;
  auto main() -> void;
  auto interrupt() -> void;

  auto load() -> bool;
  auto power(bool reset) -> void;

  //memory.cpp
  auto idle() -> void;
  auto read(uint24 addr) -> uint8;
  auto write(uint24 addr, uint8 data) -> void;

  //io.cpp
  auto readRAM(uint24 addr, uint8 data) -> uint8;
  auto readAPU(uint24 addr, uint8 data) -> uint8;
  auto readCPU(uint24 addr, uint8 data) -> uint8;
  auto readDMA(uint24 addr, uint8 data) -> uint8;
  auto writeRAM(uint24 addr, uint8 data) -> void;
  auto writeAPU(uint24 addr, uint8 data) -> void;
  auto writeCPU(uint24 addr, uint8 data) -> void;
  auto writeDMA(uint24 addr, uint8 data) -> void;

  auto serialize(serializer&) -> void;

  uint8* wram;
  uint8* wramUpperBound;
  vector<Thread*> coprocessors;

private:
  //timing
  auto queue_event(uint id) -> void;
  auto lastCycle() -> void;
  auto step(uint clocks) -> void;
  auto scanline() -> void;
  alwaysinline auto run_auto_joypad_poll() -> void;

  struct QueueEvent {
    enum : uint {
      DramRefresh,
      HdmaRun,
    };
  };
  priority_queue<uint> queue;

  //memory
  auto speed(uint addr) const -> uint;

  //dma.cpp
  auto dma_transfer_valid(uint8 bbus, uint abus) -> bool;
  auto dma_addr_valid(uint abus) -> bool;
  auto dma_read(uint abus) -> uint8;
  auto dma_write(bool valid, uint addr, uint8 data) -> void;
  auto dma_transfer(bool direction, uint8 bbus, uint abus) -> void;

  inline auto dma_bbus(uint i, uint index) -> uint8;
  inline auto dma_addr(uint i) -> uint;
  inline auto hdma_addr(uint i) -> uint;
  inline auto hdma_iaddr(uint i) -> uint;

  auto dma_run() -> void;
  auto hdma_active_after(uint i) -> bool;
  auto hdma_update(uint i) -> void;
  auto hdma_run() -> void;
  auto hdma_init() -> void;
  auto dma_reset() -> void;

  struct Channel {
    bool dma_enabled;
    bool hdma_enabled;

    bool direction;
    bool indirect;
    bool unused;
    bool reverse_transfer;
    bool fixed_transfer;
    uint8 transfer_mode;

    uint8 dest_addr;
    uint16 source_addr;
    uint8 source_bank;

    union {
      uint16_t transfer_size;
      uint16_t indirect_addr;
    };

    uint8 indirect_bank;
    uint16 hdma_addr;
    uint8 line_counter;
    uint8 unknown;

    bool hdma_completed;
    bool hdma_do_transfer;
  } channel[8];

  struct Status {
    bool nmi_valid;
    bool nmi_line;
    bool nmi_transition;
    bool nmi_pending;

    bool irq_valid;
    bool irq_line;
    bool irq_transition;
    bool irq_pending;

    bool irq_lock;
    bool hdma_pending;
  } status;

  struct IO {
    //$2181-$2183
    union {
      uint8_t* wramAddress;
      uintptr_t wramAddressRaw;
      struct {
        #if defined(ENDIAN_LSB)
        uint8_t wramAddressLo;
        uint8_t wramAddressHi;
        uint8_t wramAddressDb;
        #endif
        uint8_t wramAddressPadding1[sizeof(uint8_t*) - 2];
        #if defined(ENDIAN_MSB)
        uint8_t wramAddressDb;
        uint8_t wramAddressHi;
        uint8_t wramAddressLo;
        #endif
      };
      struct {
        #if defined(ENDIAN_LSB)
        uint16_t wramAddressWord;
        #endif
        uint8_t wramAddressPadding2[sizeof(uint8_t*) - 2];
        #if defined(ENDIAN_MSB)
        uint16_t wramAddressWord;
        #endif
      };
    };

    //$4200
    boolean hirqEnable;
    boolean virqEnable;
    boolean nmiEnable;
    boolean autoJoypadPoll;

    //$4201
    uint8 pio = 0xff;

    //$4202-$4203
    uint8 wrmpya = 0xff;
    uint8 wrmpyb = 0xff;

    //$4204-$4206
    uint16 wrdiva = 0xffff;
    uint8 wrdivb = 0xff;

    //$4207-$420a
    uint16 htime = 0x0000;
    uint16 vtime = 0x0000;

    //$420d
    uint romSpeed = 8;

    //$4214-$4217
    uint16 rddiv;
    uint16 rdmpy;
  } io;
};

extern CPU cpu;
