auto CPU::serialize(serializer& s) -> void {
  serializeCore(s);
  Thread::serialize(s);
  PPUcounter::serialize(s);

  s.array(wram, 131072);

  queue.serialize(s);

  for(uint i = 0; i < 8; i++) {
    s.integer(channel[i].dma_enabled);
    s.integer(channel[i].hdma_enabled);

    s.integer(channel[i].direction);
    s.integer(channel[i].indirect);
    s.integer(channel[i].unused);
    s.integer(channel[i].reverse_transfer);
    s.integer(channel[i].fixed_transfer);
    s.integer(channel[i].transfer_mode);

    s.integer(channel[i].dest_addr);
    s.integer(channel[i].source_addr);
    s.integer(channel[i].source_bank);

    s.integer(channel[i].transfer_size);

    s.integer(channel[i].indirect_bank);
    s.integer(channel[i].hdma_addr);
    s.integer(channel[i].line_counter);
    s.integer(channel[i].unknown);

    s.integer(channel[i].hdma_completed);
    s.integer(channel[i].hdma_do_transfer);
  }

  s.integer(status.nmi_valid);
  s.integer(status.nmi_line);
  s.integer(status.nmi_transition);
  s.integer(status.nmi_pending);

  s.integer(status.irq_valid);
  s.integer(status.irq_line);
  s.integer(status.irq_transition);
  s.integer(status.irq_pending);

  s.integer(status.irq_lock);
  s.integer(status.hdma_pending);

  uint wramAddress = io.wramAddressRaw & 0x1ffff;
  s.integer(wramAddress);
  io.wramAddressRaw = (io.wramAddressRaw & ~0x1ffff) | wramAddress;

  s.boolean(io.hirqEnable);
  s.boolean(io.virqEnable);
  s.boolean(io.nmiEnable);
  s.boolean(io.autoJoypadPoll);

  s.integer(io.pio);

  s.integer(io.wrmpya);
  s.integer(io.wrmpyb);
  s.integer(io.wrdiva);
  s.integer(io.wrdivb);

  s.integer(io.htime);
  s.integer(io.vtime);

  s.integer(io.romSpeed);

  s.integer(io.rddiv);
  s.integer(io.rdmpy);
}
