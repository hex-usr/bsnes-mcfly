auto CPU::queue_event(uint id) -> void {
  switch(id) {
    case QueueEvent::DramRefresh: return step(40);
    case QueueEvent::HdmaRun: return hdma_run();
  }
}

auto CPU::lastCycle() -> void {
  if(status.irq_lock) {
    status.irq_lock = false;
    return;
  }

  if(status.nmi_transition) {
    r.wai = false;
    status.nmi_transition = false;
    status.nmi_pending = true;
  }

  if(status.irq_transition || r.irq) {
    r.wai = false;
    status.irq_transition = false;
    status.irq_pending = !r.p.i;
  }
}

auto CPU::step(uint clocks) -> void {
  if(io.hirqEnable) {
    if(io.virqEnable) {
      uint cpu_time = vcounter() * 1364 + hcounter();
      uint irq_time = io.vtime * 1364 + io.htime * 4;
      uint framelines = (system.region() == System::Region::NTSC ? 262 : 312) + (ppu.interlace() && !field());
      if(cpu_time > irq_time) irq_time += framelines * 1364;
      bool irq_valid = status.irq_valid;
      status.irq_valid = cpu_time <= irq_time && cpu_time + clocks > irq_time;
      if(!irq_valid && status.irq_valid) status.irq_line = true;
    } else {
      uint irq_time = io.htime * 4;
      if(hcounter() > irq_time) irq_time += 1364;
      bool irq_valid = status.irq_valid;
      status.irq_valid = hcounter() <= irq_time && hcounter() + clocks > irq_time;
      if(!irq_valid && status.irq_valid) status.irq_line = true;
    }
    if(status.irq_line) status.irq_transition = true;
  } else if(io.virqEnable) {
    bool irq_valid = status.irq_valid;
    status.irq_valid = vcounter() == io.vtime;
    if(!irq_valid && status.irq_valid) status.irq_line = true;
    if(status.irq_line) status.irq_transition = true;
  } else {
    status.irq_valid = false;
  }

  tick(clocks);
  queue.tick(clocks);

  smp.clock -= clocks * (uint64)smp.frequency;
  Thread::step(clocks);
  if(controllerPort2.lightgun) {
    synchronize(*controllerPort2.lightgun);
  }
}

auto CPU::scanline() -> void {
  while(smp.clock < 0) smp.main();
  synchronize(ppu);
  for(auto coprocessor : coprocessors) synchronize(*coprocessor);

  if(vcounter() == 0) hdma_init();

  queue.enqueue(534, QueueEvent::DramRefresh);

  if(vcounter() < ppu.vdisp()) {
    queue.enqueue(1104 + 8, QueueEvent::HdmaRun);
  }

  bool nmi_valid = status.nmi_valid;
  status.nmi_valid = vcounter() >= ppu.vdisp();
  if(!nmi_valid && status.nmi_valid) {
    status.nmi_line = true;
    if(io.nmiEnable) status.nmi_transition = true;
  } else if(nmi_valid && !status.nmi_valid) {
    status.nmi_line = false;
  }

  if(io.autoJoypadPoll && vcounter() == ppu.vdisp() + 2) {
    run_auto_joypad_poll();
  }
}

auto CPU::run_auto_joypad_poll() -> void {
  controllerPort1.latch(1);
  controllerPort2.latch(1);
  controllerPort1.latch(0);
  controllerPort2.latch(0);

  if(controllerPort2.deviceID == ID::Device::SuperScope) {
    controllerPort2.joy0 = 0x00ff;
    for(uint n : range(8)) {
      controllerPort2.joy0h <<= 1;
      controllerPort2.joy0h  |= controllerPort2.data();
    }
  } else if(controllerPort2.lightgun) {
    controllerPort2.counter1 = 16;
  }
}
