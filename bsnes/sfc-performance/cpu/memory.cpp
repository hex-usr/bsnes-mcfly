auto CPU::idle() -> void {
  step(6);
}

auto CPU::read(uint24 addr) -> uint8 {
  r.mdr = bus.read(addr, r.mdr);
  step(speed(addr));
  return r.mdr;
}

auto CPU::write(uint24 addr, uint8 data) -> void {
  step(speed(addr));
  bus.write(addr, r.mdr = data);
}

auto CPU::speed(uint addr) const -> uint {
  if(addr & 0x408000) {
    if(addr & 0x800000) return io.romSpeed;
    return 8;
  }
  if((addr + 0x6000) & 0x4000) return 8;
  if((addr - 0x4000) & 0x7e00) return 6;
  return 12;
}
