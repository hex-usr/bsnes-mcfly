#include <math.h>
#define Tan(a) (CosTable[a] ? ((((int32_t)SinTable[a]) << 16) / CosTable[a]) : 0x80000000)
#define sar(b, n) ((b) >> (n))

//Wireframe Helpers
void HitachiDSP::C4TransfWireFrame() {
  double c4x = (double)C4WFXVal;
  double c4y = (double)C4WFYVal;
  double c4z = (double)C4WFZVal - 0x95;
  double tanval, c4x2, c4y2, c4z2;

  //Rotate X
  tanval = -(double)C4WFX2Val * Math::Pi * 2 / 128;
  c4y2   = c4y * ::cos(tanval) - c4z * ::sin(tanval);
  c4z2   = c4y * ::sin(tanval) + c4z * ::cos(tanval);

  //Rotate Y
  tanval = -(double)C4WFY2Val * Math::Pi * 2 / 128;
  c4x2   = c4x * ::cos(tanval)  + c4z2 * ::sin(tanval);
  c4z    = c4x * -::sin(tanval) + c4z2 * ::cos(tanval);

  //Rotate Z
  tanval = -(double)C4WFDist * Math::Pi * 2 / 128;
  c4x    = c4x2 * ::cos(tanval) - c4y2 * ::sin(tanval);
  c4y    = c4x2 * ::sin(tanval) + c4y2 * ::cos(tanval);

  //Scale
  C4WFXVal = (int16_t)(c4x * C4WFScale / (0x90 * (c4z + 0x95)) * 0x95);
  C4WFYVal = (int16_t)(c4y * C4WFScale / (0x90 * (c4z + 0x95)) * 0x95);
}

void HitachiDSP::C4CalcWireFrame() {
  C4WFXVal = C4WFX2Val - C4WFXVal;
  C4WFYVal = C4WFY2Val - C4WFYVal;

  if(abs(C4WFXVal) > abs(C4WFYVal)) {
    C4WFDist = abs(C4WFXVal) + 1;
    C4WFYVal = (256 * (long)C4WFYVal) / abs(C4WFXVal);
    C4WFXVal = (C4WFXVal < 0) ? -256 : 256;
  } else if(C4WFYVal != 0) {
    C4WFDist = abs(C4WFYVal) + 1;
    C4WFXVal = (256 * (long)C4WFXVal) / abs(C4WFYVal);
    C4WFYVal = (C4WFYVal < 0) ? -256 : 256;
  } else {
    C4WFDist = 0;
  }
}

void HitachiDSP::C4TransfWireFrame2() {
  double c4x = (double)C4WFXVal;
  double c4y = (double)C4WFYVal;
  double c4z = (double)C4WFZVal;
  double tanval, c4x2, c4y2, c4z2;

  //Rotate X
  tanval = -(double)C4WFX2Val * Math::Pi * 2 / 128;
  c4y2   = c4y * ::cos(tanval) - c4z * ::sin(tanval);
  c4z2   = c4y * ::sin(tanval) + c4z * ::cos(tanval);

  //Rotate Y
  tanval = -(double)C4WFY2Val * Math::Pi * 2 / 128;
  c4x2   = c4x * ::cos(tanval)  + c4z2 * ::sin(tanval);
  c4z    = c4x * -::sin(tanval) + c4z2 * ::cos(tanval);

  //Rotate Z
  tanval = -(double)C4WFDist * Math::Pi * 2 / 128;
  c4x    = c4x2 * ::cos(tanval) - c4y2 * ::sin(tanval);
  c4y    = c4x2 * ::sin(tanval) + c4y2 * ::cos(tanval);

  //Scale
  C4WFXVal = (int16_t)(c4x * C4WFScale / 0x100);
  C4WFYVal = (int16_t)(c4y * C4WFScale / 0x100);
}

void HitachiDSP::C4DrawWireFrame() {
  uint32_t line = readl(0x1f80);
  uint32_t point1, point2;
  int16_t X1, Y1, Z1;
  int16_t X2, Y2, Z2;
  uint8_t Color;

  uint8_t mdr = cpu.r.mdr;
  for(int32_t i = dataRAM[0x0295]; i > 0; i--, line += 5) {
    if(bus.read(line, mdr) == 0xff && bus.read(line + 1, mdr) == 0xff) {
      int32_t tmp = line - 5;
      while(bus.read(tmp + 2, mdr) == 0xff && bus.read(tmp + 3, mdr) == 0xff && (tmp + 2) >= 0) { tmp -= 5; }
      point1 = (read(0x1f82) << 16) | (bus.read(tmp + 2, mdr) << 8) | bus.read(tmp + 3, mdr);
    } else {
      point1 = (read(0x1f82) << 16) | (bus.read(line, mdr) << 8) | bus.read(line + 1, mdr);
    }
    point2 = (read(0x1f82) << 16) | (bus.read(line + 2, mdr) << 8) | bus.read(line + 3, mdr);

    X1=(bus.read(point1 + 0, mdr) << 8) | bus.read(point1 + 1, mdr);
    Y1=(bus.read(point1 + 2, mdr) << 8) | bus.read(point1 + 3, mdr);
    Z1=(bus.read(point1 + 4, mdr) << 8) | bus.read(point1 + 5, mdr);
    X2=(bus.read(point2 + 0, mdr) << 8) | bus.read(point2 + 1, mdr);
    Y2=(bus.read(point2 + 2, mdr) << 8) | bus.read(point2 + 3, mdr);
    Z2=(bus.read(point2 + 4, mdr) << 8) | bus.read(point2 + 5, mdr);
    Color = bus.read(line + 4, mdr);
    C4DrawLine(X1, Y1, Z1, X2, Y2, Z2, Color);
  }
}

void HitachiDSP::C4DrawLine(int32_t X1, int32_t Y1, int16_t Z1, int32_t X2, int32_t Y2, int16_t Z2, uint8_t Color) {
  //Transform coordinates
  C4WFXVal  = (int16_t)X1;
  C4WFYVal  = (int16_t)Y1;
  C4WFZVal  = Z1;
  C4WFScale = read(0x1f90);
  C4WFX2Val = read(0x1f86);
  C4WFY2Val = read(0x1f87);
  C4WFDist  = read(0x1f88);
  C4TransfWireFrame2();
  X1 = (C4WFXVal + 48) << 8;
  Y1 = (C4WFYVal + 48) << 8;

  C4WFXVal = (int16_t)X2;
  C4WFYVal = (int16_t)Y2;
  C4WFZVal = Z2;
  C4TransfWireFrame2();
  X2 = (C4WFXVal + 48) << 8;
  Y2 = (C4WFYVal + 48) << 8;

  //Get line info
  C4WFXVal  = (int16_t)(X1 >> 8);
  C4WFYVal  = (int16_t)(Y1 >> 8);
  C4WFX2Val = (int16_t)(X2 >> 8);
  C4WFY2Val = (int16_t)(Y2 >> 8);
  C4CalcWireFrame();
  X2 = (int16_t)C4WFXVal;
  Y2 = (int16_t)C4WFYVal;

  //Render line
  for(int32_t i = C4WFDist ? C4WFDist : 1; i > 0; i--) {
    if(X1 > 0xff && Y1 > 0xff && X1 < 0x6000 && Y1 < 0x6000) {
      uint16 addr = (((Y1 >> 8) >> 3) << 8) - (((Y1 >> 8) >> 3) << 6) + (((X1 >> 8) >> 3) << 4) + ((Y1 >> 8) & 7) * 2;
      uint8_t bit = 0x80 >> ((X1 >> 8) & 7);
      dataRAM[addr + 0x300] &= ~bit;
      dataRAM[addr + 0x301] &= ~bit;
      if(Color & 1) dataRAM[addr + 0x300] |= bit;
      if(Color & 2) dataRAM[addr + 0x301] |= bit;
    }
    X1 += X2;
    Y1 += Y2;
  }
}

void HitachiDSP::C4DoScaleRotate(int row_padding) {
  int16_t A, B, C, D;

  //Calculate matrix
  int32_t XScale = readw(0x1f8f);
  int32_t YScale = readw(0x1f92);

  if(XScale & 0x8000)XScale = 0x7fff;
  if(YScale & 0x8000)YScale = 0x7fff;

  if(readw(0x1f80) == 0) {  //no rotation
    A = (int16_t)XScale;
    B = 0;
    C = 0;
    D = (int16_t)YScale;
  } else if(readw(0x1f80) == 128) {  //90 degree rotation
    A = 0;
    B = (int16_t)(-YScale);
    C = (int16_t)XScale;
    D = 0;
  } else if(readw(0x1f80) == 256) {  //180 degree rotation
    A = (int16_t)(-XScale);
    B = 0;
    C = 0;
    D = (int16_t)(-YScale);
  } else if(readw(0x1f80) == 384) {  //270 degree rotation
    A = 0;
    B = (int16_t)YScale;
    C = (int16_t)(-XScale);
    D = 0;
  } else {
    A = (int16_t)  sar(CosTable[readw(0x1f80) & 0x1ff] * XScale, 15);
    B = (int16_t)(-sar(SinTable[readw(0x1f80) & 0x1ff] * YScale, 15));
    C = (int16_t)  sar(SinTable[readw(0x1f80) & 0x1ff] * XScale, 15);
    D = (int16_t)  sar(CosTable[readw(0x1f80) & 0x1ff] * YScale, 15);
  }

  //Calculate Pixel Resolution
  uint8_t w = read(0x1f89) & ~7;
  uint8_t h = read(0x1f8c) & ~7;

  //Clear the output RAM
  memset(dataRAM, 0, (w + row_padding / 4) * h / 2);

  int32_t Cx = (int16_t)readw(0x1f83);
  int32_t Cy = (int16_t)readw(0x1f86);

  //Calculate start position (i.e. (Ox, Oy) = (0, 0))
  //The low 12 bits are fractional, so (Cx<<12) gives us the Cx we want in
  //the function. We do Cx*A etc normally because the matrix parameters
  //already have the fractional parts.
  int32_t LineX = (Cx << 12) - Cx * A - Cx * B;
  int32_t LineY = (Cy << 12) - Cy * C - Cy * D;

  //Start loop
  uint32_t X, Y;
  uint8_t byte;
  int32_t outidx = 0;
  uint8_t bit    = 0x80;

  for(int32_t y = 0; y < h; y++) {
    X = LineX;
    Y = LineY;
    for(int32_t x = 0; x < w; x++) {
      if((X >> 12) >= w || (Y >> 12) >= h) {
        byte = 0;
      } else {
        uint32_t addr = (Y >> 12) * w + (X >> 12);
        byte = read(0x600 + (addr >> 1));
        if(addr & 1) { byte >>= 4; }
      }

      //De-bitplanify
      if(byte & 1) dataRAM[outidx     ] |= bit;
      if(byte & 2) dataRAM[outidx +  1] |= bit;
      if(byte & 4) dataRAM[outidx + 16] |= bit;
      if(byte & 8) dataRAM[outidx + 17] |= bit;

      bit >>= 1;
      if(!bit) {
        bit     = 0x80;
        outidx += 32;
      }

      X += A;  //Add 1 to output x => add an A and a C
      Y += C;
    }
    outidx += 2 + row_padding;
    if(outidx & 0x10) {
      outidx &= ~0x10;
    } else {
      outidx -= w * 4 + row_padding;
    }
    LineX += B;  //Add 1 to output y => add a B and a D
    LineY += D;
  }
}
