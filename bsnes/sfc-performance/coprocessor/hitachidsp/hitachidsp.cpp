//=============
//Cx4 emulation
//=============
//Used in Rockman X2/X3 (Mega Man X2/X3)
//Portions (c) anomie, Overload, zsKnight, Nach, byuu

HitachiDSP hitachidsp;
#include "data.cpp"
#include "functions.cpp"
#include "opcodes.cpp"
#include "serialization.cpp"

auto HitachiDSP::Enter() -> void {
}

void HitachiDSP::unload() {
  rom.reset();
  ram.reset();
}

void HitachiDSP::power() {
  memory::fill<uint8>(dataRAM, 3072, 0x00);
  memory::fill(reg, 0x0100, 0);
}

uint32_t HitachiDSP::ldr(uint8_t r) {
  uint16 addr = 0x0080 + (r * 3);
  return (reg[addr + 0] <<  0)
       | (reg[addr + 1] <<  8)
       | (reg[addr + 2] << 16);
}

void HitachiDSP::str(uint8_t r, uint32_t data) {
  uint16 addr = 0x0080 + (r * 3);
  reg[addr + 0] = (data >>  0);
  reg[addr + 1] = (data >>  8);
  reg[addr + 2] = (data >> 16);
}

void HitachiDSP::mul(uint32_t x, uint32_t y, uint32_t &rl, uint32_t &rh) {
  int64 rx = x & 0xffffff;
  int64 ry = y & 0xffffff;
  if(rx & 0x800000)rx |= ~0x7fffff;
  if(ry & 0x800000)ry |= ~0x7fffff;

  rx *= ry;

  rl = (rx)       & 0xffffff;
  rh = (rx >> 24) & 0xffffff;
}

uint32_t HitachiDSP::sin(uint32_t rx) {
  r0 = rx & 0x1ff;
  if(r0 & 0x100)r0 ^= 0x1ff;
  if(r0 & 0x080)r0 ^= 0x0ff;
  if(rx & 0x100) {
    return sin_table[r0 + 0x80];
  } else {
    return sin_table[r0];
  }
}

uint32_t HitachiDSP::cos(uint32_t rx) {
  return sin(rx + 0x080);
}

void HitachiDSP::immediate_reg(uint32_t start) {
  r0 = ldr(0);
  for(uint32_t i = start; i < 48; i++) {
    if((r0 & 0x0fff) < 0x0c00) {
      dataRAM[r0 & 0x0fff] = immediate_data[i];
    }
    r0++;
  }
  str(0, r0);
}

void HitachiDSP::transfer_data() {
  uint32_t src;
  uint16 dest, count;

  src   = (reg[0x40]) | (reg[0x41] << 8) | (reg[0x42] << 16);
  count = (reg[0x43]) | (reg[0x44] << 8);
  dest  = (reg[0x45]) | (reg[0x46] << 8);

  for(uint32_t i = 0; i < count; i++) {
    write(dest++, bus.read(src++, cpu.r.mdr));
  }
}

uint8 HitachiDSP::read(unsigned addr) {
  addr &= 0x1fff;

  if(addr < 0x0c00) {
    return dataRAM[addr];
  }

  if(addr >= 0x1f00) {
    return reg[addr & 0xff];
  }

  return cpu.r.mdr;
}

void HitachiDSP::write(unsigned addr, uint8 data) {
  addr &= 0x1fff;

  if(addr < 0x0c00) {
    dataRAM[addr] = data;
    return;
  }

  if(addr >= 0x1f00) {
    writeIO(addr, data);
    return;
  }
}

auto HitachiDSP::readROM(uint24 addr, uint8 data) -> uint8 {
  return rom.read(addr, data);
}

auto HitachiDSP::writeROM(uint24 addr, uint8 data) -> void {
}

auto HitachiDSP::readRAM(uint24 addr, uint8 data) -> uint8 {
  if(ram.size() == 0) return 0x00;  //not open bus
  return ram.read(Bus::mirror(addr, ram.size()), data);
}

auto HitachiDSP::writeRAM(uint24 addr, uint8 data) -> void {
  if(ram.size() == 0) return;
  return ram.write(Bus::mirror(addr, ram.size()), data);
}

auto HitachiDSP::readDRAM(uint24 addr, uint8 data) -> uint8 {
  addr &= 0xfff;
  if(addr >= 0xc00) return data;
  return dataRAM[addr];
}

auto HitachiDSP::writeDRAM(uint24 addr, uint8 data) -> void {
  addr &= 0xfff;
  if(addr >= 0x0c00) return;
  dataRAM[addr] = data;
}

auto HitachiDSP::readIO(uint24 addr, uint8 data) -> uint8 {
  addr &= 0xfff;

  if(addr >= 0xf00) {
    return reg[addr & 0xff];
  }

  return data;
}

auto HitachiDSP::writeIO(uint24 addr, uint8 data) -> void {
  addr &= 0xfff;

  if(addr < 0xf00) {
    //unmapped
    return;
  }

  //command register
  reg[addr & 0xff] = data;

  if(addr == 0xf47) {
    //memory transfer
    transfer_data();
    return;
  }

  if(addr == 0xf4f) {
    //validation
    if(reg[0x49] != 0x00 || reg[0x4a] != 0x80 || reg[0x4b] != 0x02) {
      return;
    }

    //c4 command
    if(reg[0x4d] == 0x0e && !(data & 0xc3)) {
      //c4 test command
      reg[0x80] = data >> 2;
      return;
    }

    switch(reg[0x4e] << 16 | reg[0x4d] << 8 | data) {
    case 0x000000: return op000000();
    case 0x000205: return op000205();
    case 0x00020d: return op00020d();
    case 0x000210: return op000210();
    case 0x000213: return op000213();
    case 0x000215: return op000215();
    case 0x00021f: return op00021f();
    case 0x000222: return op000222();
    case 0x000225: return op000225();
    case 0x00022d: return op00022d();
    case 0x000300: return op000300();
    case 0x000500: return op000500();
    case 0x000604: return op000604();
    case 0x000700: return op000700();
    case 0x000800: return op000800();
    case 0x000801: return op000801();
    case 0x000b00: return op000b00();
    case 0x000c00: return op000c00();
    case 0x000e40: return op000e40();
    case 0x000e54: return op000e54();
    case 0x000e5c: return op000e5c();
    case 0x000e5e: return op000e5e();
    case 0x000e60: return op000e60();
    case 0x000e62: return op000e62();
    case 0x000e64: return op000e64();
    case 0x000e66: return op000e66();
    case 0x000e68: return op000e68();
    case 0x000e6a: return op000e6a();
    case 0x000e6c: return op000e6c();
    case 0x000e6e: return op000e6e();
    case 0x000e70: return op000e70();
    case 0x000e72: return op000e72();
    case 0x000e74: return op000e74();
    case 0x000e76: return op000e76();
    case 0x000e78: return op000e78();
    case 0x000e7a: return op000e7a();
    case 0x000e7c: return op000e7c();
    case 0x000e89: return op000e89();
    }
  }
}

uint8_t HitachiDSP::readb(uint16_t addr) {
  return read(addr);
}

uint16_t HitachiDSP::readw(uint16_t addr) {
  return read(addr) | (read(addr + 1) << 8);
}

uint32_t HitachiDSP::readl(uint16_t addr) {
  return read(addr) | (read(addr + 1) << 8) + (read(addr + 2) << 16);
}

void HitachiDSP::writeb(uint16_t addr, uint8_t data) {
  write(addr,     data);
}

void HitachiDSP::writew(uint16_t addr, uint16_t data) {
  write(addr + 0, data >> 0);
  write(addr + 1, data >> 8);
}

void HitachiDSP::writel(uint16_t addr, uint32_t data) {
  write(addr + 0, data >>  0);
  write(addr + 1, data >>  8);
  write(addr + 2, data >> 16);
}
