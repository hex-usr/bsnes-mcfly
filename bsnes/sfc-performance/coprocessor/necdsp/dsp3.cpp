//DSP-3 emulator code
//Copyright (c) 2003-2006 John Weidman, Kris Bleakley, Lancer, z80 gaiden

auto DSP3::reset() -> void {
  DR = 0x0080;
  SR = 0x0084;
  SetDSP3 = {&DSP3::Command, this};
}

auto DSP3::readSR() -> uint8 {
  return (byte = SR);
}

auto DSP3::writeSR(uint8 data) -> void {
  byte = data;
}

auto DSP3::readDR() -> uint8 {
  if (SR & 0x04)
  {
    byte = (uint8) DR;
    SetDSP3();
  }
  else
  {
    SR ^= 0x10;

    if (SR & 0x10)
      byte = (uint8) (DR);
    else
    {
      byte = (uint8) (DR >> 8);
      SetDSP3();
    }
  }
  return byte;
}

auto DSP3::writeDR(uint8 data) -> void {
  byte = data;
  if (SR & 0x04)
  {
    DR = (DR & 0xff00) + data;
    SetDSP3();
  }
  else
  {
    SR ^= 0x10;

    if (SR & 0x10)
      DR = (DR & 0xff00) + data;
    else
    {
      DR = (DR & 0x00ff) + (data << 8);
      SetDSP3();
    }
  }
}

void DSP3::MemorySize()
{
	DR = 0x0300;
	SetDSP3 = {&DSP3::reset, this};
}

void DSP3::TestMemory()
{
	DR = 0x0000;
	SetDSP3 = {&DSP3::reset, this};
}

void DSP3::DumpDataROM()
{
	DR = necdsp.dataROM[MemoryIndex++];
	if (MemoryIndex == 1024)
		SetDSP3 = {&DSP3::reset, this};
}

void DSP3::MemoryDump()
{
	MemoryIndex = 0;
	SetDSP3 = {&DSP3::DumpDataROM, this};
	DumpDataROM();
}

void DSP3::OP06()
{
	WinLo = (uint8)(DR);
	WinHi = (uint8)(DR >> 8);
	reset();
}

void DSP3::OP03()
{
	int16 Lo = (uint8)(DR);
	int16 Hi = (uint8)(DR >> 8);
	int16 Ofs = (WinLo * Hi << 1) + (Lo << 1);
	DR = Ofs >> 1;
	SetDSP3 = {&DSP3::reset, this};
}

void DSP3::OP07_B()
{
	int16 Ofs = (WinLo * AddHi << 1) + (AddLo << 1);
	DR = Ofs >> 1;
	SetDSP3 = {&DSP3::reset, this};
}

void DSP3::OP07_A()
{
	int16 Lo = (uint8)(DR);
	int16 Hi = (uint8)(DR >> 8);

	if (Lo & 1)	Hi += (AddLo & 1);

	AddLo += Lo;
	AddHi += Hi;

	if (AddLo < 0)
		AddLo += WinLo;
	else
		if (AddLo >= WinLo)
			AddLo -= WinLo;

	if (AddHi < 0)
		AddHi += WinHi;
	else
		if (AddHi >= WinHi)
			AddHi -= WinHi;

	DR = AddLo | (AddHi << 8) | ((AddHi >> 8) & 0xff);
	SetDSP3 = {&DSP3::OP07_B, this};
}

void DSP3::OP07()
{
	uint32 dataOfs = ((DR << 1) + 0x03b2) & 0x03ff;

	AddHi = necdsp.dataROM[dataOfs];
	AddLo = necdsp.dataROM[dataOfs + 1];

	SetDSP3 = {&DSP3::OP07_A, this};
	SR = 0x0080;
}

void DSP3::Coordinate()
{
	Index++;

	switch (Index)
	{
	case 3:
		{
			if (DR == 0xffff)
				reset();
			break;
		}
	case 4:
		{
			X = DR;
			break;
		}
	case 5:
		{
			Y = DR;
			DR = 1;
			break;
		}
	case 6:
		{
			DR = X;
			break;
		}
	case 7:
		{
			DR = Y;
			Index = 0;
			break;
		}
	}
}

void DSP3::Convert_A()
{
	if (BMIndex < 8)
	{
		Bitmap[BMIndex++] = (uint8) (DR);
		Bitmap[BMIndex++] = (uint8) (DR >> 8);

		if (BMIndex == 8)
		{
      short i, j;
			for (i=0; i < 8; i++)
				for (j=0; j < 8; j++)
				{
					Bitplane[j] <<= 1;
					Bitplane[j] |= (Bitmap[i] >> j) & 1;
				}

			BPIndex = 0;
			Count--;
		}
	}

	if (BMIndex == 8)
	{
		if (BPIndex == 8)
		{
			if (!Count) reset();
			BMIndex = 0;
		}
		else
		{
			DR = Bitplane[BPIndex++];
			DR |= Bitplane[BPIndex++] << 8;
		}
	}
}

void DSP3::Convert()
{
	Count = DR;
	BMIndex = 0;
	SetDSP3 = {&DSP3::Convert_A, this};
}

bool DSP3::GetBits(uint8 Count)
{
	if (!BitsLeft)
	{
		BitsLeft = Count;
		ReqBits = 0;
	}

	do {
		if (!BitCount)
		{
			SR = 0xC0;
			return false;
		}

		ReqBits <<= 1;
		if (ReqData & 0x8000) ReqBits++;
		ReqData <<= 1;

		BitCount--;
		BitsLeft--;

	} while (BitsLeft);

	return true;
}

void DSP3::Decode_Data()
{
	if (!BitCount)
	{
		if (SR & 0x40)
		{
			ReqData = DR;
			BitCount += 16;
		}
		else
		{
			SR = 0xC0;
			return;
		}
	}

	if (LZCode == 1)
	{
		if (!GetBits(1))
			return;

		if (ReqBits)
			LZLength = 12;
		else
			LZLength = 8;

		LZCode++;
	}

	if (LZCode == 2)
	{
		if (!GetBits(LZLength))
			return;

		LZCode = 0;
		Outwords--;
		if (!Outwords) SetDSP3 = {&DSP3::reset, this};

		SR = 0x80;
		DR = ReqBits;
		return;
	}

	if (BaseCode == 0xffff)
	{
		if (!GetBits(BaseLength))
			return;

		BaseCode = ReqBits;
	}

	if (!GetBits(CodeLengths[BaseCode]))
		return;

	Symbol = Codes[CodeOffsets[BaseCode] + ReqBits];
	BaseCode = 0xffff;

	if (Symbol & 0xff00)
	{
		Symbol += 0x7f02;
		LZCode++;
	}
	else
	{
		Outwords--;
		if (!Outwords)
			SetDSP3 = {&DSP3::reset, this};
	}

	SR = 0x80;
	DR = Symbol;
}

void DSP3::Decode_Tree()
{
	if (!BitCount)
	{
		ReqData = DR;
		BitCount += 16;
	}

	if (!BaseCodes)
	{
		GetBits(1);
		if (ReqBits)
		{
			BaseLength = 3;
			BaseCodes = 8;
		}
		else
		{
			BaseLength = 2;
			BaseCodes = 4;
		}
	}

	while (BaseCodes)
	{
		if (!GetBits(3))
			return;

		ReqBits++;

		CodeLengths[Index] = (uint8) ReqBits;
		CodeOffsets[Index] = Symbol;
		Index++;

		Symbol += 1 << ReqBits;
		BaseCodes--;
	}

	BaseCode = 0xffff;
	LZCode = 0;

	SetDSP3 = {&DSP3::Decode_Data, this};
	if (BitCount) Decode_Data();
}

void DSP3::Decode_Symbols()
{
	ReqData = DR;
	BitCount += 16;

	do {

		if (BitCommand == 0xffff)
		{
			if (!GetBits(2)) return;
			BitCommand = ReqBits;
		}

		switch (BitCommand)
		{
		case 0:
			{
				if (!GetBits(9)) return;
				Symbol = ReqBits;
				break;
			}
		case 1:
			{
				Symbol++;
				break;
			}
		case 2:
			{
				if (!GetBits(1)) return;
				Symbol += 2 + ReqBits;
				break;
			}
		case 3:
			{
				if (!GetBits(4)) return;
				Symbol += 4 + ReqBits;
				break;
			}
		}

		BitCommand = 0xffff;

		Codes[Index++] = Symbol;
		Codewords--;

	} while (Codewords);

	Index = 0;
	Symbol = 0;
	BaseCodes = 0;

	SetDSP3 = {&DSP3::Decode_Tree, this};
	if (BitCount) Decode_Tree();
}

void DSP3::Decode_A()
{
	Outwords = DR;
	SetDSP3 = {&DSP3::Decode_Symbols, this};
	BitCount = 0;
	BitsLeft = 0;
	Symbol = 0;
	Index = 0;
	BitCommand = 0xffff;
	SR = 0xC0;
}

void DSP3::Decode()
{
	Codewords = DR;
	SetDSP3 = {&DSP3::Decode_A, this};
}


// Opcodes 1E/3E bit-perfect to 'dsp3-intro' log
// src: adapted from SD Gundam X/G-Next

void DSP3::OP3E()
{
	op3e_x = (uint8)(DR & 0x00ff);
	op3e_y = (uint8)((DR & 0xff00)>>8);

	OP03();

	op1e_terrain[ DR ] = 0x00;
	op1e_cost[ DR ] = 0xff;
	op1e_weight[ DR ] = 0;

	op1e_max_search_radius = 0;
	op1e_max_path_radius = 0;
}

void DSP3::OP1E()
{
	int lcv;

	op1e_min_radius = (uint8)(DR & 0x00ff);
	op1e_max_radius = (uint8)((DR & 0xff00)>>8);

	if( op1e_min_radius == 0 )
		op1e_min_radius++;

	if( op1e_max_search_radius >= op1e_min_radius )
		op1e_min_radius = op1e_max_search_radius+1;

	if( op1e_max_radius > op1e_max_search_radius )
		op1e_max_search_radius = op1e_max_radius;

	op1e_lcv_radius = op1e_min_radius;
	op1e_lcv_steps = op1e_min_radius;

	op1e_lcv_turns = 6;
	op1e_turn = 0;

	op1e_x = op3e_x;
	op1e_y = op3e_y;

	for( lcv = 0; lcv < op1e_min_radius; lcv++ )
		OP1E_D( op1e_turn, &op1e_x, &op1e_y );

	OP1E_A();
}

void DSP3::OP1E_A()
{
	int lcv;

	if( op1e_lcv_steps == 0 ) {
		op1e_lcv_radius++;

		op1e_lcv_steps = op1e_lcv_radius;

		op1e_x = op3e_x;
		op1e_y = op3e_y;

		for( lcv = 0; lcv < op1e_lcv_radius; lcv++ )
			OP1E_D( op1e_turn, &op1e_x, &op1e_y );
	}

	if( op1e_lcv_radius > op1e_max_radius ) {
		op1e_turn++;
		op1e_lcv_turns--;

		op1e_lcv_radius = op1e_min_radius;
		op1e_lcv_steps = op1e_min_radius;

		op1e_x = op3e_x;
		op1e_y = op3e_y;

		for( lcv = 0; lcv < op1e_min_radius; lcv++ )
			OP1E_D( op1e_turn, &op1e_x, &op1e_y );
	}

	if( op1e_lcv_turns == 0 ) {
		DR = 0xffff;
		SR = 0x0080;
		SetDSP3 = {&DSP3::OP1E_B, this};
		return;
	}

	DR = (uint8)(op1e_x) | ((uint8)(op1e_y)<<8);
	OP03();

	op1e_cell = DR;

	SR = 0x0080;
	SetDSP3 = {&DSP3::OP1E_A1, this};
}

void DSP3::OP1E_A1()
{
	SR = 0x0084;
	SetDSP3 = {&DSP3::OP1E_A2, this};
}

void DSP3::OP1E_A2()
{
	op1e_terrain[ op1e_cell ] = (uint8)(DR & 0x00ff);

	SR = 0x0084;
	SetDSP3 = {&DSP3::OP1E_A3, this};
}

void DSP3::OP1E_A3()
{
	op1e_cost[ op1e_cell ] = (uint8)(DR & 0x00ff);

	if( op1e_lcv_radius == 1 ) {
		if( op1e_terrain[ op1e_cell ] & 1 ) {
			op1e_weight[ op1e_cell ] = 0xff;
		} else {
			op1e_weight[ op1e_cell ] = op1e_cost[ op1e_cell ];
		}
	}
	else {
		op1e_weight[ op1e_cell ] = 0xff;
	}

	OP1E_D( (int16)(op1e_turn+2), &op1e_x, &op1e_y );
	op1e_lcv_steps--;

	SR = 0x0080;
	OP1E_A();
}


void DSP3::OP1E_B()
{
	op1e_x = op3e_x;
	op1e_y = op3e_y;
	op1e_lcv_radius = 1;

	op1e_search = 0;

	OP1E_B1();

	SetDSP3 = {&DSP3::OP1E_C, this};
}


void DSP3::OP1E_B1()
{
	while( op1e_lcv_radius < op1e_max_radius ) {
		op1e_y--;

		op1e_lcv_turns = 6;
		op1e_turn = 5;

		while( op1e_lcv_turns ) {
			op1e_lcv_steps = op1e_lcv_radius;

			while( op1e_lcv_steps ) {
				OP1E_D1( op1e_turn, &op1e_x, &op1e_y );

				if( 0 <= op1e_y && op1e_y < WinHi &&
						0 <= op1e_x && op1e_x < WinLo ) {
					DR = (uint8)(op1e_x) | ((uint8)(op1e_y)<<8);
					OP03();

					op1e_cell = DR;
					if( op1e_cost[ op1e_cell ] < 0x80 &&
							op1e_terrain[ op1e_cell ] < 0x40 ) {
						OP1E_B2();
					} // end cell perimeter
				}

				op1e_lcv_steps--;
			} // end search line

			op1e_turn--;
			if( op1e_turn == 0 ) op1e_turn = 6;

			op1e_lcv_turns--;
		} // end circle search

		op1e_lcv_radius++;
	} // end radius search
}


void DSP3::OP1E_B2()
{
	int16 cell;
	int16 path;
	int16 x,y;
	int16 lcv_turns;

	path = 0xff;
	lcv_turns = 6;

	while( lcv_turns ) {
		x = op1e_x;
		y = op1e_y;

		OP1E_D1( lcv_turns, &x, &y );

		DR = (uint8)(x) | ((uint8)(y)<<8);
		OP03();

		cell = DR;

		if( 0 <= y && y < WinHi &&
				0 <= x && x < WinLo  ) {

			if( op1e_terrain[ cell ] < 0x80 || op1e_weight[ cell ] == 0 ) {
				if( op1e_weight[ cell ] < path ) {
					path = op1e_weight[ cell ];
				}
			}
		} // end step travel

		lcv_turns--;
	} // end while turns

	if( path != 0xff ) {
		op1e_weight[ op1e_cell ] = path + op1e_cost[ op1e_cell ];
	}
}


void DSP3::OP1E_C()
{
	int lcv;

	op1e_min_radius = (uint8)(DR & 0x00ff);
	op1e_max_radius = (uint8)((DR & 0xff00)>>8);

	if( op1e_min_radius == 0 )
		op1e_min_radius++;

	if( op1e_max_path_radius >= op1e_min_radius )
		op1e_min_radius = op1e_max_path_radius+1;

	if( op1e_max_radius > op1e_max_path_radius )
		op1e_max_path_radius = op1e_max_radius;

	op1e_lcv_radius = op1e_min_radius;
	op1e_lcv_steps = op1e_min_radius;

	op1e_lcv_turns = 6;
	op1e_turn = 0;

	op1e_x = op3e_x;
	op1e_y = op3e_y;

	for( lcv = 0; lcv < op1e_min_radius; lcv++ )
		OP1E_D( op1e_turn, &op1e_x, &op1e_y );

	OP1E_C1();
}


void DSP3::OP1E_C1()
{
	int lcv;

	if( op1e_lcv_steps == 0 ) {
		op1e_lcv_radius++;

		op1e_lcv_steps = op1e_lcv_radius;

		op1e_x = op3e_x;
		op1e_y = op3e_y;

		for( lcv = 0; lcv < op1e_lcv_radius; lcv++ )
			OP1E_D( op1e_turn, &op1e_x, &op1e_y );
	}

	if( op1e_lcv_radius > op1e_max_radius ) {
		op1e_turn++;
		op1e_lcv_turns--;

		op1e_lcv_radius = op1e_min_radius;
		op1e_lcv_steps = op1e_min_radius;

		op1e_x = op3e_x;
		op1e_y = op3e_y;

		for( lcv = 0; lcv < op1e_min_radius; lcv++ )
			OP1E_D( op1e_turn, &op1e_x, &op1e_y );
	}

	if( op1e_lcv_turns == 0 ) {
		DR = 0xffff;
		SR = 0x0080;
		SetDSP3 = {&DSP3::reset, this};
		return;
	}

	DR = (uint8)(op1e_x) | ((uint8)(op1e_y)<<8);
	OP03();

	op1e_cell = DR;

	SR = 0x0080;
	SetDSP3 = {&DSP3::OP1E_C2, this};
}


void DSP3::OP1E_C2()
{
	DR = op1e_weight[ op1e_cell ];

	OP1E_D( (int16)(op1e_turn+2), &op1e_x, &op1e_y );
	op1e_lcv_steps--;

	SR = 0x0084;
	SetDSP3 = {&DSP3::OP1E_C1, this};
}


void DSP3::OP1E_D( int16 move, int16 *lo, int16 *hi )
{
	uint32 dataOfs = ((move << 1) + 0x03b2) & 0x03ff;
	int16 Lo;
	int16 Hi;

	AddHi = necdsp.dataROM[dataOfs];
	AddLo = necdsp.dataROM[dataOfs + 1];

	Lo = (uint8)(*lo);
	Hi = (uint8)(*hi);

	if (Lo & 1)	Hi += (AddLo & 1);

	AddLo += Lo;
	AddHi += Hi;

	if (AddLo < 0)
		AddLo += WinLo;
	else
		if (AddLo >= WinLo)
			AddLo -= WinLo;

	if (AddHi < 0)
		AddHi += WinHi;
	else
		if (AddHi >= WinHi)
			AddHi -= WinHi;

	*lo = AddLo;
	*hi = AddHi;
}


void DSP3::OP1E_D1( int16 move, int16 *lo, int16 *hi )
{
	//uint32 dataOfs = ((move << 1) + 0x03b2) & 0x03ff;
	int16 Lo;
	int16 Hi;

	const unsigned short HiAdd[] = {
		0x00, 0xFF, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00,
		0x00, 0xFF, 0xFF, 0x00, 0x01, 0x00, 0xFF, 0x00
	};
	const unsigned short LoAdd[] = {
		0x00, 0x00, 0x01, 0x01, 0x00, 0xFF, 0xFF, 0x00
	};

	if( (*lo) & 1 )
		AddHi = HiAdd[ move + 8 ];
	else
		AddHi = HiAdd[ move + 0 ];
	AddLo = LoAdd[ move ];

	Lo = (uint8)(*lo);
	Hi = (uint8)(*hi);

	if (Lo & 1)	Hi += (AddLo & 1);

	AddLo += Lo;
	AddHi += Hi;

	*lo = AddLo;
	*hi = AddHi;
}


void DSP3::OP10()
{
	if( DR == 0xffff ) {
		reset();
	} else {
		// absorb 2 bytes
		DR = DR;
	}
}


void DSP3::OP0C_A()
{
	// absorb 2 bytes

	DR = 0;
	SetDSP3 = {&DSP3::reset, this};
}


void DSP3::OP0C()
{
	// absorb 2 bytes

	DR = 0;
	//SetDSP3 = {&DSP3::OP0C_A, this};
	SetDSP3 = {&DSP3::reset, this};
}


void DSP3::OP1C_C()
{
	// return 2 bytes
	DR = 0;
	SetDSP3 = {&DSP3::reset, this};
}


void DSP3::OP1C_B()
{
	// absorb 2 bytes

	// return 2 bytes
	DR = 0;
	SetDSP3 = {&DSP3::OP1C_C, this};
}


void DSP3::OP1C_A()
{
	// absorb 2 bytes

	SetDSP3 = {&DSP3::OP1C_B, this};
}


void DSP3::OP1C()
{
	// absorb 2 bytes

	SetDSP3 = {&DSP3::OP1C_A, this};
}

void DSP3::Command()
{
	if (DR < 0x40)
	{
		switch (DR)
		{
    case 0x02: SetDSP3 = {&DSP3::Coordinate, this}; break;
    case 0x03: SetDSP3 = {&DSP3::OP03, this}; break;
    case 0x06: SetDSP3 = {&DSP3::OP06, this}; break;
    case 0x07: SetDSP3 = {&DSP3::OP07, this}; return;
    case 0x0c: SetDSP3 = {&DSP3::OP0C, this}; break;
    case 0x0f: SetDSP3 = {&DSP3::TestMemory, this}; break;
    case 0x10: SetDSP3 = {&DSP3::OP10, this}; break;
    case 0x18: SetDSP3 = {&DSP3::Convert, this}; break;
    case 0x1c: SetDSP3 = {&DSP3::OP1C, this}; break;
    case 0x1e: SetDSP3 = {&DSP3::OP1E, this}; break;
    case 0x1f: SetDSP3 = {&DSP3::MemoryDump, this}; break;
    case 0x38: SetDSP3 = {&DSP3::Decode, this}; break;
    case 0x3e: SetDSP3 = {&DSP3::OP3E, this}; break;
    default:
     return;
		}
		SR = 0x0080;
		Index = 0;
	}
}
