NECDSP necdsp;
#include "dsp1.cpp"
#include "dsp2.cpp"
#include "dsp3.cpp"
#include "dsp4.cpp"
#include "st010.cpp"
#include "serialization.cpp"

auto NECDSP::power() -> void {
  dsp = nullptr;
  version = 0;
  Hash::CRC32 hash;
  for(uint n : range(revision == Revision::uPD96050 ? 16384 : 2048)) {
    const uint24 word = programROM[n];
    hash.input(word >>  0);
    hash.input(word >>  8);
    hash.input(word >> 16);
  }
  uint32_t value = hash.value();
  if(value == 0xdb9a4c92) dsp = &dsp1,  version = 0;
  if(value == 0x66a73998) dsp = &dsp1,  version = 1;
  if(value == 0x0c1cf838) dsp = &dsp2,  version = 0;
  if(value == 0xf29be51c) dsp = &dsp3,  version = 0;
  if(value == 0x14b77ae3) dsp = &dsp4,  version = 0;
  if(value == 0x691cbb4f) dsp = &st010, version = 0;
//if(value == 0xa741e9fb) dsp = &st011, version = 0;
  if(dsp) dsp->reset();
}

auto NECDSP::read(uint24 addr, uint8 data) -> uint8 {
  if(!dsp) return 0x00;
  uint8 byte;
  if(addr & 1) {
    byte = dsp->readSR();
  } else {
    byte = dsp->readDR();
  }
  return byte;
}

auto NECDSP::write(uint24 addr, uint8 data) -> void {
  if(!dsp) return;
  if(addr & 1) {
    dsp->writeSR(data);
  } else {
    dsp->writeDR(data);
  }
}

auto NECDSP::readRAM(uint24 addr, uint8) -> uint8 {
  if(!dsp) return 0x00;
  uint8 byte = dsp->readDP(addr);
  return byte;
}

auto NECDSP::writeRAM(uint24 addr, uint8 data) -> void {
  if(!dsp) return;
  dsp->writeDP(addr, data);
}
