struct ManagedNECDSP {
  virtual auto reset() -> void;
  virtual auto readSR() -> uint8;
  virtual auto writeSR(uint8) -> void;
  virtual auto readDR() -> uint8;
  virtual auto writeDR(uint8) -> void;
  virtual auto readDP(uint12 addr) -> uint8;
  virtual auto writeDP(uint12 addr, uint8 data) -> void;

  virtual auto serialize(serializer&) -> void;
};

#include "dsp1.hpp"
#include "dsp2.hpp"
#include "dsp3.hpp"
#include "dsp4.hpp"
#include "st010.hpp"

struct NECDSP {
  //necdsp.cpp
  void power();

  auto read(uint24 addr, uint8 data) -> uint8;
  auto write(uint24 addr, uint8 data) -> void;

  auto readRAM(uint24 addr, uint8 data) -> uint8;
  auto writeRAM(uint24 addr, uint8 data) -> void;

  //serialization.cpp
  auto firmware() const -> vector<uint8>;
  auto serialize(serializer&) -> void;

  enum class Revision : uint { uPD7725, uPD96050 } revision;
  uint8_t version;

  uint24 programROM[16384];
  uint16 dataROM[2048];
  uint16 dataRAM[2048];

  uint Frequency = 0;

private:
  DSP1 dsp1;
  DSP2 dsp2;
  DSP3 dsp3;
  DSP4 dsp4;
  ST010 st010;

  ManagedNECDSP* dsp = nullptr;
};

extern NECDSP necdsp;
