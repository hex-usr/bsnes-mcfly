#pragma once

//license: GPLv3
//started: 2004-10-14

#include <emulator/emulator.hpp>
#include <emulator/thread.hpp>
#include <emulator/scheduler.hpp>
#include <emulator/random.hpp>
#include <emulator/cheat.hpp>

#include <component/processor/arm7tdmi/arm7tdmi.hpp>
#include <component/processor/gsu/gsu.hpp>

#if defined(CORE_GB)
  #include <gb/gb.hpp>
#endif

namespace higan::SuperFamicom {
  extern Scheduler scheduler;
  extern Random random;
  extern Cheat cheat;

  struct Thread : higan::Thread {
    auto create(auto (*entrypoint)() -> void, double frequency) -> void {
      higan::Thread::create(entrypoint, frequency);
      scheduler.append(*this);
    }

    inline auto synchronize(Thread& thread) -> void {
      if(clock() >= thread.clock()) scheduler.resume(thread);
    }
  };

  struct Region {
    static inline auto NTSC() -> bool;
    static inline auto PAL() -> bool;
  };

  #include <sfc-performance/priority-queue.hpp>

  #include <sfc/system/system.hpp>
  #include <sfc-performance/memory/memory.hpp>
  #include <sfc/ppu/counter/counter.hpp>

  #include <sfc-performance/cpu/cpu.hpp>
  #include <sfc-performance/apu/apu.hpp>
  #include <sfc-performance/ppu/ppu.hpp>

  #include <sfc-performance/controller/controller.hpp>
  #include <sfc-performance/expansion/expansion.hpp>
  #include <sfc-performance/coprocessor/coprocessor.hpp>
  #include <sfc/slot/slot.hpp>
  #include <sfc/cartridge/cartridge.hpp>

  #include <sfc-performance/memory/memory-inline.hpp>
  #include <sfc/ppu/counter/counter-inline.hpp>
}

#include <sfc-performance/interface/interface.hpp>
