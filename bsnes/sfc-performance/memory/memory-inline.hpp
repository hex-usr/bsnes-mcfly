auto Bus::mirror(uint addr, uint size) -> uint {
  if(size == 0) return 0;
  uint base = 0;
  uint mask = 1 << 23;
  while(addr >= size) {
    while(!(addr & mask)) mask >>= 1;
    addr -= mask;
    if(size > mask) {
      size -= mask;
      base += mask;
    }
    mask >>= 1;
  }
  return base + addr;
}

auto Bus::reduce(uint addr, uint mask) -> uint {
  while(mask) {
    uint bits = (mask & -mask) - 1;
    addr = ((addr >> 1) & ~bits) | (addr & bits);
    mask = (mask & (mask - 1)) >> 1;
  }
  return addr;
}

auto Bus::read(uint24 addr, uint8 data) -> uint8 {
  if((addr & 0x40e000) == 0x000000) {
    data = cpu.wram[addr & 0x1fff];
  } else if((addr & 0x40fc00) == 0x004000) {
    if((addr & 0x40ff80) == 0x004300) {
      data = cpu.readDMA(addr, data);
    } else {
      data = cpu.readCPU(addr, data);
    }
  } else if((addr & 0x40ff00) == 0x002100) {
    if((addr & 0xc0) == 0x00) {
      data = ppu.readIO(addr, data);
    } else if((addr & 0xc0) == 0x40) {
      data = cpu.readAPU(addr & 3, data);
    } else if((addr & 0xfc) == 0x80) {
      data = cpu.readCPU(addr, data);
    }
  } else if((addr & 0xfe0000) == 0x7e0000) {
    data = wramOffset[addr];
  } else {
    uint8 lookupIndex = lookup[addr];
    if(lookupIndex < Page::ReadWrite) {
      data = reader[lookupIndex](target[addr], data);
    } else {
      data = *(page[addr]);
    }
  }

  if(cheat) {
    if(!(addr & 0x40e000)) addr = 0x7e0000 | (addr & 0x1fff);  //de-mirror WRAM
    if(auto result = cheat.find(addr, data)) return result();
  }
  return data;
}

auto Bus::write(uint24 addr, uint8 data) -> void {
  if((addr & 0x40e000) == 0x000000) {
    cpu.wram[addr & 0x1fff] = data;
  } else if((addr & 0x40fc00) == 0x004000) {
    if((addr & 0x40ff80) == 0x004300) {
      cpu.writeDMA(addr, data);
    } else {
      cpu.writeCPU(addr, data);
    }
  } else if((addr & 0x40ff00) == 0x002100) {
    if((addr & 0xc0) == 0x00) {
      ppu.writeIO(addr, data);
    } else if((addr & 0xc0) == 0x40) {
      cpu.writeAPU(addr & 3, data);
    } else if((addr & 0xfc) == 0x80) {
      cpu.writeCPU(addr, data);
    }
  } else if((addr & 0xfe0000) == 0x7e0000) {
    wramOffset[addr] = data;
  } else {
    uint8 lookupIndex = lookup[addr];
    if(lookupIndex < Page::ReadWrite) {
      return writer[lookupIndex](target[addr], data);
    } else if(lookupIndex == Page::ReadWrite) {
      *(page[addr]) = data;
    }
  }
}
