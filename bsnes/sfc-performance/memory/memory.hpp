struct Memory {
  virtual ~Memory() { reset(); }
  inline explicit operator bool() const { return size() > 0; }

  virtual auto reset() -> void {}
  virtual auto allocate(uint, uint8 = 0xff) -> void {}

  virtual auto data() -> uint8* = 0;
  virtual auto size() const -> uint = 0;

  virtual auto read(uint24 address, uint8 data = 0) -> uint8 = 0;
  virtual auto write(uint24 address, uint8 data) -> void = 0;

  uint id = 0;
};

#include <sfc/memory/readable.hpp>
#include <sfc/memory/writable.hpp>
#include <sfc/memory/protectable.hpp>

struct Bus {
  alwaysinline static auto mirror(uint address, uint size) -> uint;
  alwaysinline static auto reduce(uint address, uint mask) -> uint;

  alwaysinline auto read(uint24 address, uint8 data) -> uint8;
  alwaysinline auto write(uint24 address, uint8 data) -> void;

  auto reset() -> void;
  auto map(
    const function<uint8 (uint24, uint8)>& read,
    const function<void (uint24, uint8)>& write,
    const string& address, uint size = 0, uint base = 0, uint mask = 0
  ) -> uint;
  auto map(
    uint8* pointer,
    bool writable,
    const string& address, uint size = 0, uint base = 0, uint mask = 0
  ) -> void;
  auto unmap(const string& address) -> void;

  uint8* wramOffset;

private:
  uint8 lookup[16 * 1024 * 1024];
  union {
    uintptr_t target[16 * 1024 * 1024];
    uint8* page[16 * 1024 * 1024];
  };

  function<uint8 (uint24, uint8)> reader[256];
  function<void (uint24, uint8)> writer[256];
  uint24 counter[256];

  enum Page : uint8_t {
    Unmapped  = 0x00,
    ReadWrite = 0xfe,
    ReadOnly  = 0xff,
  };
};

extern Bus bus;
