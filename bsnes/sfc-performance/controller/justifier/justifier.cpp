Justifier::Justifier(bool chained):
chained(chained),
device(!chained ? ID::Device::Justifier : ID::Device::Justifiers)
{
  create(LightGun::Enter, system.cpuFrequency());
  active = 0;
  prev = 0;

  scale = !ppu.hires() || !ppu.interlace();

  player1.sprite = video.createSprite(32, 32);
  player1.sprite->setPixels(scale ? (
    (image)Resource::Sprite::CrosshairGreenSmall
  ) : (
    (image)Resource::Sprite::CrosshairGreen
  ));
  player1.x = 256 / 2;
  player1.y = 240 / 2;
  player1.trigger = false;
  player2.start = false;

  player2.sprite = video.createSprite(32, 32);
  player2.sprite->setPixels(scale ? (
    (image)Resource::Sprite::CrosshairRedSmall
  ) : (
    (image)Resource::Sprite::CrosshairRed
  ));
  player2.x = 256 / 2;
  player2.y = 240 / 2;
  player2.trigger = false;
  player2.start = false;

  if(chained == false) {
    player2.x = -1;
    player2.y = -1;
  } else {
    player1.x -= 16;
    player2.x += 16;
  }
}

Justifier::~Justifier() {
  video.removeSprite(player1.sprite);
  video.removeSprite(player2.sprite);
  scheduler.remove(*this);
}

auto Justifier::main() -> void {
  uint next = cpu.vcounter() * 1364 + cpu.hcounter();

  int x = (active == 0 ? player1.x : player2.x), y = (active == 0 ? player1.y : player2.y);
  bool offscreen = (x < 0 || y < 0 || x >= 256 || y >= ppu.vdisp());

  if(!offscreen) {
    uint target = y * 1364 + (x + 24) * 4;
    if(next >= target && prev < target) {
      //CRT raster detected, toggle iobit to latch counters
      iobit(0);
      iobit(1);
    }
  }

  int multiplierX = ppu.hires()     ? 2 : 1;
  int multiplierY = ppu.interlace() ? 2 : 1;

  if(next < prev) {
    int nx1 = platform->inputPoll(ID::Port::Controller2, device, 0 + X);
    int ny1 = platform->inputPoll(ID::Port::Controller2, device, 0 + Y);
    nx1 += player1.x;
    ny1 += player1.y;
    player1.x = max(-16, min(256 + 16, nx1));
    player1.y = max(-16, min(240 + 16, ny1));
    player1.sprite->setPosition(player1.x * multiplierX - 16, player1.y * multiplierY - 16);
    player1.sprite->setVisible(true);
  }

  if(next < prev && chained) {
    int nx2 = platform->inputPoll(ID::Port::Controller2, device, 4 + X);
    int ny2 = platform->inputPoll(ID::Port::Controller2, device, 4 + Y);
    nx2 += player2.x;
    ny2 += player2.y;
    player2.x = max(-16, min(256 + 16, nx2));
    player2.y = max(-16, min(240 + 16, ny2));
    player2.sprite->setPosition(player2.x * multiplierX - 16, player2.y * multiplierY - 16);
    player2.sprite->setVisible(true);
  }

  bool scale_ = !ppu.hires() || !ppu.interlace();
  if(scale != scale_) {
    scale = scale_;
    player1.sprite->setPixels(scale ? (
      (image)Resource::Sprite::CrosshairGreenSmall
    ) : (
      (image)Resource::Sprite::CrosshairGreen
    ));
    player2.sprite->setPixels(scale ? (
      (image)Resource::Sprite::CrosshairRedSmall
    ) : (
      (image)Resource::Sprite::CrosshairRed
    ));
  }

  prev = next;
  step(2);
  synchronize(cpu);
}

auto Justifier::data() -> uint2 {
  if(controllerPort2.counter1 >= 32) return 1;

  switch(controllerPort2.counter1++) {
  case  0: return 0;
  case  1: return 0;
  case  2: return 0;
  case  3: return 0;
  case  4: return 0;
  case  5: return 0;
  case  6: return 0;
  case  7: return 0;
  case  8: return 0;
  case  9: return 0;
  case 10: return 0;
  case 11: return 0;

  case 12: return 1;  //signature
  case 13: return 1;  // ||
  case 14: return 1;  // ||
  case 15: return 0;  // ||

  case 16: return 0;
  case 17: return 1;
  case 18: return 0;
  case 19: return 1;
  case 20: return 0;
  case 21: return 1;
  case 22: return 0;
  case 23: return 1;

  case 24: return player1.trigger;
  case 25: return player2.trigger;
  case 26: return player1.start;
  case 27: return player2.start;
  case 28: return active;

  case 29: return 0;
  case 30: return 0;
  case 31: return 0;
  }

  unreachable;
}

auto Justifier::latch(bool data) -> void {
  if(controllerPort2.latched == data) return;
  controllerPort2.latched = data;
  controllerPort2.counter1 = 0;
  if(controllerPort2.latched == 0) active = !active;  //toggle between both controllers, even when unchained

  player1.trigger = platform->inputPoll(ID::Port::Controller2, device, 0 + Trigger);
  player1.start   = platform->inputPoll(ID::Port::Controller2, device, 0 + Start);

  if(chained) {
    player2.trigger = platform->inputPoll(ID::Port::Controller2, device, 4 + Trigger);
    player2.start   = platform->inputPoll(ID::Port::Controller2, device, 4 + Start);
  }
}
