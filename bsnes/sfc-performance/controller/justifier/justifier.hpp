struct Justifier : LightGun {
  enum : uint {
    X, Y, Trigger, Start,
  };

  Justifier(bool chained);
  ~Justifier();

  auto main() -> void;
  auto data() -> uint2;
  auto latch(bool data) -> void;

//private:
  const bool chained;  //true if the second justifier is attached to the first
  const uint device;
  uint prev;

  bool scale;

  bool active;
  struct Player {
    shared_pointer<Sprite> sprite;
    int x;
    int y;
    bool trigger;
    bool start;
  } player1, player2;
};
