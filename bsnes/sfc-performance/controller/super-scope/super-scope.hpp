struct SuperScope : LightGun {
  shared_pointer<Sprite> sprite;

  enum : uint {
    X, Y, Trigger, Cursor, Turbo, Pause,
  };

  SuperScope();
  ~SuperScope();

  auto main() -> void;
  auto data() -> uint2;
  auto latch(bool data) -> void;

private:
  bool scale;

  int x;
  int y;

  bool trigger;
  bool cursor;
  bool turbo;
  bool pause;
  bool offscreen;

  bool oldturbo;
  bool triggerlock;
  bool pauselock;

  uint prev;
};
