auto ControllerPort::gamepadData() -> uint2 {
  if(counter1 >= 16) return 1;
  if(latched == 1) return platform->inputPoll(port, ID::Device::Gamepad, Gamepad::B);

  return joy0 >> (15 - counter1++) & 1;
}

auto ControllerPort::gamepadLatch(bool data) -> void {
  if(latched == data) return;
  latched = data;
  counter1 = 0;

  if(latched == 0) {
    uint16_t data_ = 0x0000;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::B)      << 15;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Y)      << 14;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Select) << 13;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Start)  << 12;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Up)     << 11;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Down)   << 10;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Left)   <<  9;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::Right)  <<  8;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::A)      <<  7;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::X)      <<  6;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::L)      <<  5;
    data_ |= platform->inputPoll(port, ID::Device::Gamepad, Gamepad::R)      <<  4;
    joy0 = data_;
  }
}
