#include <nall/file.hpp>
#include <nall/stdint.hpp>
#include <nall/string.hpp>
using namespace nall;

enum Timing : uint { Cycle, Mixed, Opcode };

static Timing timing;

struct opcode_t {
  string name;
  string_vector args;
  uint opcode;
};

auto generateCycleTiming(file& fp, opcode_t opcode, string_vector& lines, uint sourceStart) -> void {
  fp.print("case 0x", hex(opcode.opcode, 2L), ": {\n");
  fp.print("  switch(opcode_cycle++) {\n");

  for(uint n = sourceStart; n < lines.size(); n++) {
    if(lines[n] == "}") break;

    bool nextLineEndsCycle = false;
    if(lines[n + 1] == "}") nextLineEndsCycle = true;
    if(lines[n + 1].beginsWith("  ") == false) nextLineEndsCycle = true;

    string output;

    if(lines[n].beginsWith("  ")) {
      output = { "  ", lines[n] };
    } else {
      string_vector part = lines[n].split(":", 1L);
      fp.print("  case ", toNatural(part[0]), ":\n");
      output = { "    ", part[1] };
    }

    for(uint n = 1; n <= 8; n++) if(opcode.args.size() > n) output.replace(string{"$", n}, opcode.args[n]);
    output.replace("end;", "{ opcode_cycle = 0; break; }");

    fp.print(output, "\n");
    if(nextLineEndsCycle) {
      if(lines[n + 1].beginsWith("}")) {
        fp.print("    opcode_cycle = 0;\n");
      }
      fp.print("    break;\n");
    }
  }

  fp.print("  }\n");
  fp.print("  break;\n");
  fp.print("}\n\n");
}

auto generateMixedTiming(file& fp, opcode_t opcode, string_vector& lines, uint sourceStart) -> void {
  fp.print("case 0x", hex(opcode.opcode, 2L), ": {\n");
  fp.print("  switch(++opcode_cycle) {\n");
  fp.print("  case 1:\n");

  uint op_io = 0;
  uint opcode_cycle = 1;
  for(uint n = sourceStart; n < lines.size(); n++) {
    if(lines[n] == "}") break;

    bool nextLineEndsCycle = false;
    if(lines[n + 1] == "}") nextLineEndsCycle = true;

    string output;

    if(n > sourceStart && (lines[n].find("op_read") || lines[n].find("op_write"))) {
      fp.print("    break;\n");
      fp.print("  case ", ++opcode_cycle, ":\n");
    }

    if(lines[n].beginsWith("  ")) {
      output = { "  ", lines[n] };
    } else {
      string_vector part = lines[n].split(":", 1L);
      output = { "    ", part[1] };
    }

    for(uint n = 1; n <= 8; n++) if(opcode.args.size() > n) output.replace(string{"$", n}, opcode.args[n]);
    output.replace("end;", "{ opcode_cycle = 0; break; }");

    if(output == "    op_io();") {
      op_io += 1;
    } else {
      if(op_io) fp.print("    op_io(", op_io > 1 ? string{op_io} : string{""}, ");\n");
      op_io = 0;
      fp.print(output, "\n");
    }
    if(nextLineEndsCycle) {
      if(op_io) fp.print("    op_io(", op_io > 1 ? string{op_io} : string{""}, ");\n");
      op_io = 0;
      if(lines[n + 1].beginsWith("}")) {
        fp.print("    opcode_cycle = 0;\n");
      }
      fp.print("    break;\n");
    }
  }

  fp.print("  }\n");
  fp.print("  break;\n");
  fp.print("}\n\n");
}

auto generateOpcodeTiming(file& fp, opcode_t opcode, string_vector& lines, uint sourceStart) -> void {
  fp.print("case 0x", hex(opcode.opcode, 2L), ": {\n");

  uint op_io = 0;
  for(uint n = sourceStart; n < lines.size(); n++) {
    if(lines[n] == "}") break;

    string output;

    if(lines[n].beginsWith("  ")) {
      output = lines[n];
    } else {
      string_vector part = lines[n].split(":", 1L);
      output = { "  ", part[1] };
    }

    for(uint n = 1; n <= 8; n++) if(opcode.args.size() > n) output.replace(string{"$", n}, opcode.args[n]);
    output.replace("end;", "break;");

    if(output == "  op_io();") {
      op_io += 1;
    } else {
      if(op_io) fp.print("  op_io(", op_io > 1 ? string{op_io} : string{""}, ");\n");
      op_io = 0;
      fp.print(output, "\n");
    }
  }
  if(op_io) fp.print("  op_io(", op_io > 1 ? string{op_io} : string{""}, ");\n");
  op_io = 0;

  fp.print("  break;\n");
  fp.print("}\n\n");
}

auto generate(const char* sourceFilename, const char* targetFilename) -> void {
  file fp;
  fp.open(targetFilename, file::mode::write);

  string filedata = file::read(sourceFilename);
  filedata.replace("\r", "");

  string_vector block = filedata.split("\n\n");

  for(string data : block) {
    string_vector lines = data.split("\n");

    vector<opcode_t> array;

    uint sourceStart = 0;
    for(uint currentLine = 0; currentLine < lines.size(); currentLine++) {
      string line = lines[currentLine];
      line.transform("()", "``");
      string_vector part = line.split("`");
      string_vector arguments = part[1].split(", ");

      opcode_t opcode;
      opcode.name = part[0];
      opcode.args = arguments;
      opcode.opcode = toHex(arguments[0]);
      array.append(opcode);

      line.trimRight(",", 1L);
      if(line.endsWith(" {")) {
        line.trimRight("{ ", 1L);
        sourceStart = currentLine + 1;
        break;
      }
    }

    switch(timing) {

    case Timing::Cycle: {
      for(auto opcode : array) {
        generateCycleTiming(fp, opcode, lines, sourceStart);
      }
      break;
    }

    case Timing::Mixed: {
      for(auto opcode : array) {
        uint op = 0;
        uint opReadWrite = 0;
        for(uint n = sourceStart; n < lines.size(); n++) {
          if(lines[n].find("op_")) op += 1;
          if(lines[n].find("op_read") || lines[n].find("op_write")) opReadWrite += 1;
        }
        if(op >= 2 && opReadWrite >= 1) {
          generateMixedTiming(fp, opcode, lines, sourceStart);
        } else {
          generateOpcodeTiming(fp, opcode, lines, sourceStart);
        }
      }
      break;
    }

    case Timing::Opcode: {
      for(auto opcode : array) {
        generateOpcodeTiming(fp, opcode, lines, sourceStart);
      }
      break;
    }

    }
  }

  fp.close();
}

auto main() -> int {
  timing = Timing::Cycle;
  generate("op_misc.b", "cycle_misc.cpp");
  generate("op_mov.b",  "cycle_mov.cpp" );
  generate("op_pc.b",   "cycle_pc.cpp" );
  generate("op_read.b", "cycle_read.cpp");
  generate("op_rmw.b",  "cycle_rmw.cpp" );

  timing = Timing::Mixed;
  generate("op_misc.b", "mixed_misc.cpp");
  generate("op_mov.b",  "mixed_mov.cpp" );
  generate("op_pc.b",   "mixed_pc.cpp" );
  generate("op_read.b", "mixed_read.cpp");
  generate("op_rmw.b",  "mixed_rmw.cpp" );

  timing = Timing::Opcode;
  generate("op_misc.b", "opcode_misc.cpp");
  generate("op_mov.b",  "opcode_mov.cpp" );
  generate("op_pc.b",   "opcode_pc.cpp" );
  generate("op_read.b", "opcode_read.cpp");
  generate("op_rmw.b",  "opcode_rmw.cpp" );

  return 0;
}
