auto SMP::step() -> void {
  timer0.synchronizeStage1();
  timer1.synchronizeStage1();
  timer2.synchronizeStage1();

  clock += cycle_step_cpu;
  spc_dsp.run(1);

  int count = spc_dsp.sample_count() >> 1;
  int16_t* bufferIndex = samplebuffer;
  while(count--) {
    double left, right;
    left  = *bufferIndex++ / 32768.0;
    right = *bufferIndex++ / 32768.0;
    stream->sample(left, right);
  }
  spc_dsp.set_output(samplebuffer, 8192);
}

auto SMP::step(uint clocks) -> void {
  timer0.step(clocks);
  timer1.step(clocks);
  timer2.step(clocks);

  clock += cycle_step_cpu * clocks;
  spc_dsp.run(clocks);

  int count = spc_dsp.sample_count() >> 1;
  int16_t* bufferIndex = samplebuffer;
  while(count--) {
    double left, right;
    left  = *bufferIndex++ / 32768.0;
    right = *bufferIndex++ / 32768.0;
    stream->sample(left, right);
  }
  spc_dsp.set_output(samplebuffer, 8192);
}

template<uint frequency> auto SMP::Timer<frequency>::step(uint clocks) -> void {
  stage1 += clocks;
  if(stage1 < frequency) return;

  stage1 -= frequency;
  if(enable == false) return;

  if(++stage2 != target) return;

  stage2 = 0;
  stage3 = (stage3 + 1) & 15;
}

template<uint frequency> auto SMP::Timer<frequency>::synchronizeStage1() -> void {
  if(++stage1 < frequency) return;

  stage1 = 0;
  if(enable == false) return;

  if(++stage2 != target) return;

  stage2 = 0;
  stage3 = (stage3 + 1) & 15;
}
