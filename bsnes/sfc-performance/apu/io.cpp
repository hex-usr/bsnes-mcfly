auto SMP::portRead(uint8_t addr) -> uint8_t {
  return (apuram + 0xf4)[addr];
}

auto SMP::portWrite(uint8_t addr, uint8 data) -> void {
  io.apu[addr] = data;
}

auto SMP::readIO(uint16 addr) -> uint8 {
  switch(addr) {

  case 0xf2:  //DSPADDR
    return io.dspAddr;

  case 0xf3:  //DSPDATA
    return spc_dsp.read(io.dspAddr & 0x7f);

  case 0xf4:  //CPUIO0
  //synchronize(cpu);
    return io.apu[0];

  case 0xf5:  //CPUIO1
  //synchronize(cpu);
    return io.apu[1];

  case 0xf6:  //CPUIO2
  //synchronize(cpu);
    return io.apu[2];

  case 0xf7:  //CPUIO3
  //synchronize(cpu);
    return io.apu[3];

  case 0xf8:  //AUXIO4
    return io.aux4;

  case 0xf9:  //AUXIO5
    return io.aux5;

  case 0xfd: {//T0OUT (4-bit counter value)
    uint8_t data = timer0.stage3 & 15;
    timer0.stage3 = 0;
    return data;
  }

  case 0xfe: {//T1OUT (4-bit counter value)
    uint8_t data = timer1.stage3 & 15;
    timer1.stage3 = 0;
    return data;
  }

  case 0xff: {//T2OUT (4-bit counter value)
    uint8_t data = timer2.stage3 & 15;
    timer2.stage3 = 0;
    return data;
  }

  }

  return 0x00;
}

auto SMP::writeIO(uint16 addr, uint8 data) -> void {
  switch(addr) {

  case 0xf1:
    io.iplromEnable = data & 0x80;

    if(data & 0x30) {
    //synchronize(cpu);
      if(data & 0x20) {
        io.apu_w[1] = 0x0000;
      }
      if(data & 0x10) {
        io.apu_w[0] = 0x0000;
      }
    }

    if(!timer2.enable && (data & 0x04)) {
      timer2.stage2 = 0;
      timer2.stage3 = 0;
    }
    timer2.enable = data & 0x04;

    if(!timer1.enable && (data & 0x02)) {
      timer1.stage2 = 0;
      timer1.stage3 = 0;
    }
    timer1.enable = data & 0x02;

    if(!timer0.enable && (data & 0x01)) {
      timer0.stage2 = 0;
      timer0.stage3 = 0;
    }
    timer0.enable = data & 0x01;

    break;

  case 0xf2:
    io.dspAddr = data;
    break;

  case 0xf3:
    if(io.dspAddr & 0x80) break;
    spc_dsp.write(io.dspAddr, data);
    break;

//case 0xf4:
//case 0xf5:
//case 0xf6:
//case 0xf7:
  //synchronize(cpu);
  //apuram[addr] = data;
  //break;

  case 0xf8:
    io.aux4 = data;
    break;

  case 0xf9:
    io.aux5 = data;
    break;

  case 0xfa:
    timer0.target = data;
    break;

  case 0xfb:
    timer1.target = data;
    break;

  case 0xfc:
    timer2.target = data;
    break;

  }
}
