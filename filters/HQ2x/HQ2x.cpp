#include <nall/platform.hpp>
#include <nall/traits.hpp>
#include <nall/stdint.hpp>
#include <nall/primitives.hpp>
using namespace nall;
using uint32 = Natural<32>;

extern "C" {
  void filter_size(unsigned&, unsigned&);
  void filter_render(uint32_t*, unsigned, const uint32_t*, unsigned, unsigned, unsigned);
};

enum {
  diff_offset = (0x440 << 21) + (0x207 << 11) + 0x407,
  diff_mask   = (0x380 << 21) + (0x1f0 << 11) + 0x3f0,
};

uint32_t* yuvTable;
uint8_t rotate[256];

const uint8_t hqTable[256] = {
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 15, 12, 5,  3, 17, 13,
  4, 4, 6, 18, 4, 4, 6, 18, 5,  3, 12, 12, 5,  3,  1, 12,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 17, 13, 5,  3, 16, 14,
  4, 4, 6, 18, 4, 4, 6, 18, 5,  3, 16, 12, 5,  3,  1, 14,
  4, 4, 6,  2, 4, 4, 6,  2, 5, 19, 12, 12, 5, 19, 16, 12,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 16, 12, 5,  3, 16, 12,
  4, 4, 6,  2, 4, 4, 6,  2, 5, 19,  1, 12, 5, 19,  1, 14,
  4, 4, 6,  2, 4, 4, 6, 18, 5,  3, 16, 12, 5, 19,  1, 14,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 15, 12, 5,  3, 17, 13,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 16, 12, 5,  3, 16, 12,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 17, 13, 5,  3, 16, 14,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 16, 13, 5,  3,  1, 14,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 16, 12, 5,  3, 16, 13,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 16, 12, 5,  3,  1, 12,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3, 16, 12, 5,  3,  1, 14,
  4, 4, 6,  2, 4, 4, 6,  2, 5,  3,  1, 12, 5,  3,  1, 14,
};

static void initialize() {
  static bool initialized = false;
  if(initialized == true) return;
  initialized = true;

  yuvTable = new uint32_t[256 * 256 * 256];

  #pragma omp parallel for
  for(unsigned i = 0; i < 256 * 256 * 256; i++) {
    double r = (i >> 16) & 255;
    double g = (i >>  8) & 255;
    double b = (i >>  0) & 255;

    //bgr888->yuv
    double y = (r + g + b) * (0.25f * (63.5f / 48.0f));
    double u = ((r - b) * 0.25f + 128.0f) * (7.5f / 7.0f);
    double v = ((g * 2.0f - r - b) * 0.125f + 128.0f) * (7.5f / 6.0f);

    yuvTable[i] = ((unsigned)y << 21) + ((unsigned)u << 11) + ((unsigned)v);
  }

  //counter-clockwise rotation table; one revolution:
  //123    369  12346789
  //4.6 -> 2.8  =
  //789    147  36928147
  for(unsigned n = 0; n < 256; n++) {
    rotate[n] = ((n >> 2) & 0x11) | ((n << 2) & 0x88)
              | ((n & 0x01) << 5) | ((n & 0x08) << 3)
              | ((n & 0x10) >> 3) | ((n & 0x80) >> 5);
  }
}

static void terminate() {
  delete[] yuvTable;
}

static bool same(uint32_t x, uint32_t y) {
  return !((yuvTable[x & 0xffffff] - yuvTable[y & 0xffffff] + diff_offset) & diff_mask);
}

static bool diff(uint32_t x, uint32_t y) {
  return ((x - yuvTable[y & 0xffffff]) & diff_mask);
}

static void grow(uint32_t& n) { n |= n << 16; n &= 0x03e07c1f; }
static uint16_t pack(uint32_t n) { n &= 0x03e07c1f; return n | (n >> 16); }

static uint32_t blend1(uint32 A, uint32 B) {
  for(unsigned n = 0; n < 3; n++) { A.byte(n) = (A.byte(n) * 3 + B.byte(n)) >> 2; }
  return A;
}

static uint32_t blend2(uint32 A, uint32 B, uint32 C) {
  for(unsigned n = 0; n < 3; n++) { A.byte(n) = (A.byte(n) * 2 + B.byte(n) + C.byte(n)) >> 2; }
  return A;
}

static uint32_t blend3(uint32 A, uint32 B, uint32 C) {
  for(unsigned n = 0; n < 3; n++) { A.byte(n) = (A.byte(n) * 5 + B.byte(n) * 2 + C.byte(n)) >> 3; }
  return A;
}

static uint32_t blend4(uint32 A, uint32 B, uint32 C) {
  for(unsigned n = 0; n < 3; n++) { A.byte(n) = (A.byte(n) * 6 + B.byte(n) + C.byte(n)) >> 3; }
  return A;
}

static uint32_t blend5(uint32 A, uint32 B, uint32 C) {
  for(unsigned n = 0; n < 3; n++) { A.byte(n) = (A.byte(n) * 2 + (B.byte(n) + C.byte(n)) * 3) >> 3; }
  return A;
}

static uint32_t blend6(uint32 A, uint32 B, uint32 C) {
  for(unsigned n = 0; n < 3; n++) { A.byte(n) = (A.byte(n) * 14 + B.byte(n) + C.byte(n)) >> 4; }
  return A;
}

static uint32_t blend(unsigned rule, uint32_t E, uint32_t A, uint32_t B, uint32_t D, uint32_t F, uint32_t H) {
  switch(rule) { default:
  case  0: return E;
  case  1: return blend1(E, A);
  case  2: return blend1(E, D);
  case  3: return blend1(E, B);
  case  4: return blend2(E, D, B);
  case  5: return blend2(E, A, B);
  case  6: return blend2(E, A, D);
  case  7: return blend3(E, B, D);
  case  8: return blend3(E, D, B);
  case  9: return blend4(E, D, B);
  case 10: return blend5(E, D, B);
  case 11: return blend6(E, D, B);
  case 12: return same(B, D) ? blend2(E, D, B) : E;
  case 13: return same(B, D) ? blend5(E, D, B) : E;
  case 14: return same(B, D) ? blend6(E, D, B) : E;
  case 15: return same(B, D) ? blend2(E, D, B) : blend1(E, A);
  case 16: return same(B, D) ? blend4(E, D, B) : blend1(E, A);
  case 17: return same(B, D) ? blend5(E, D, B) : blend1(E, A);
  case 18: return same(B, F) ? blend3(E, B, D) : blend1(E, D);
  case 19: return same(D, H) ? blend3(E, D, B) : blend1(E, B);
  }
}

dllexport void filter_size(unsigned& width, unsigned& height) {
  if(width <= 256) width *= 2;
  height *= 2;
}

dllexport void filter_render(
  uint32_t* output, unsigned outputPitch,
  const uint32_t* input, unsigned inputPitch,
  unsigned width, unsigned height
) {
  initialize();
  outputPitch >>= 2, inputPitch >>= 2;

  const bool hires = width > 256;

  #pragma omp parallel for
  for(unsigned y = 0; y < height; y++) {
    const uint32_t* in = input + y * inputPitch;
    uint32_t* out0 = output + y * outputPitch * 2;
    uint32_t* out1 = output + y * outputPitch * 2 + outputPitch;

    int prevline = (y == 0 ? 0 : inputPitch);
    int nextline = (y == height - 1 ? 0 : inputPitch);

    in++;
    if(hires) in++;
    *out0++ = 0; *out0++ = 0;
    *out1++ = 0; *out1++ = 0;

    for(unsigned x = 1; x < (width >> hires) - 1; x++) {
      if(hires && in[0] != in[1]) {
        *out0++ = *out1++ = *in++;
        *out0++ = *out1++ = *in++;
        continue;
      }

      uint32_t A = *(in - prevline - 1);
      uint32_t B = *(in - prevline + 0);
      uint32_t C = *(in - prevline + 1 + hires);
      uint32_t D = *(in - 1);
      uint32_t E = *(in + 0);
      uint32_t F = *(in + 1 + hires);
      uint32_t G = *(in + nextline - 1);
      uint32_t H = *(in + nextline + 0);
      uint32_t I = *(in + nextline + 1 + hires);
      uint32_t e = yuvTable[E & 0xffffff] + diff_offset;

      uint8_t pattern;
      pattern  = diff(e, A) << 0;
      pattern |= diff(e, B) << 1;
      pattern |= diff(e, C) << 2;
      pattern |= diff(e, D) << 3;
      pattern |= diff(e, F) << 4;
      pattern |= diff(e, G) << 5;
      pattern |= diff(e, H) << 6;
      pattern |= diff(e, I) << 7;

      *(out0 + 0) = blend(hqTable[pattern], E, A, B, D, F, H); pattern = rotate[pattern];
      *(out0 + 1) = blend(hqTable[pattern], E, C, F, B, H, D); pattern = rotate[pattern];
      *(out1 + 1) = blend(hqTable[pattern], E, I, H, F, D, B); pattern = rotate[pattern];
      *(out1 + 0) = blend(hqTable[pattern], E, G, D, H, B, F);

      in++; if(hires) in++;
      out0 += 2;
      out1 += 2;
    }

    in++; if(hires) in++;
    *out0++ = 0; *out0++ = 0;
    *out1++ = 0; *out1++ = 0;
  }
}
