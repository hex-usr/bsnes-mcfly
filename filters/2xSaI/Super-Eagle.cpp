#include <nall/platform.hpp>
#include <nall/stdint.hpp>
using namespace nall;

#include "implementation.cpp"

extern "C" {
  void filter_size(unsigned&, unsigned&);
  void filter_render(uint32_t*, unsigned, const uint32_t*, unsigned, unsigned, unsigned);
};

dllexport void filter_size(unsigned& width, unsigned& height) {
  width  *= 2;
  height *= 2;
}

dllexport void filter_render(
  uint32_t* output, unsigned outputPitch,
  const uint32_t* input, unsigned inputPitch,
  unsigned width, unsigned height
) {
  SuperEagle32((unsigned char*)input, inputPitch, nullptr, (unsigned char*)output, outputPitch, width, height);
}
