@echo off

cd bsnes\

del /q out\*.exe
del /q out\*.dll

mingw32-make -j6 name=bsnes-accuracy-compatibility.dll

if exist out\bsnes-accuracy-compatibility.dll (
  mingw32-make -j6 profile=performance name=bsnes-performance.dll

  if exist out\bsnes-performance.dll (
    mingw32-make launcher
  ) else (
    set /A ERRORLEVEL=1
  )
) else (
  set /A ERRORLEVEL=1
)

if /I "%ERRORLEVEL%" NEQ "0" (pause)
@echo on